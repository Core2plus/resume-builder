<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, maximum-scale=1">
<title>MY University</title>
<link rel="icon" href="<?php echo site_url(); ?>assets/img/My University/favicon.png" type="image/png">
<link rel="shortcut icon" href="<?php echo site_url(); ?>assets/img/iqralogosmall.png" type="img/x-icon">
<link href="<?php echo base_url(); ?>assets/css/material-font.min.css" rel="stylesheet" type="text/css">
<link href="<?php echo base_url(); ?>assets/css/main.css" rel="stylesheet" type="text/css">
<link href="<?php echo base_url(); ?>assets/css/mystyle.css" rel="stylesheet" type="text/css">

<style>
body  {
    background-image: url("<?php echo base_url('assets/img/myu_signin_bglogo/university-background-04.jpg') ?>");
    background-color: #cccccc;
}
</style>

</head>
<body style="margin: 0px; padding:0px;" >

<!--main-nav-start-->


<div class="company-logo"><br><img src="<?php echo base_url(); ?>/assets/img/My University/iqralogo.png"  width="350" class="img-responsive my-logo1" style="margin:auto;" alt="">
</div>
<hr style="margin: 0px 185px; border-radius: 115px; border: 0px solid transparent; box-shadow: 6px 7px 3px 0px #7792f3a1;">
    <div class="row" style="margin: 0px;">
        


<div class="login-form" id="login" style="margin-top: 2%; margin-bottom: 0px; ">


    <span>
        <?php
            if(isset($_SESSION['success']))
            {
                ?>
                <div class="alert alert-success">
                    <?php
                        echo $_SESSION['success'];
                    ?>
                </div>
                <?php
            }
        ?>
    </span>



<span>
        <?php
            if(isset($_SESSION['error']))
            {
        ?>
            <div class="alert alert-danger">
                <?php
                    echo $_SESSION['error'];
                ?>
            </div>
        <?php
            }
        ?>
    </span>
    <form action="login" method="POST"  >
        <h2 class="text-center" style="color: white; font-family: Georgia " >Log In</h2>       
        <div class="form-group">
            <input style=" padding: 5px; color: white; " type="email" name="email" id="email" class="form-control" placeholder="Enter your Email" required >
        </div>
        <div class="form-group">
            <input style=" padding: 5px; color: white; "  type="password" name="password" id="password" class="form-control" placeholder="Password" required>
        </div>
        <div class="form-group">
            <input type="submit" class="btn btn-primary btn-block" name="login" value="log in">
        </div>
        <div class="clearfix">
            <label class="pull-left checkbox-inline"><input type="checkbox"> Remember me</label>
            <a href="#" class="pull-right" style="color: white">Forgot Password?</a>
        </div>        
    </form>
    <br>
    <p style="margin-bottom: 0px; padding-bottom: 0px;"  class="text-center"><a href="<?php echo base_url('ORB/register'); ?>" style="color: white">Create an Account</a></p>
    
</div>
<hr>
<!-- Footer -->
<center >
<footer style="margin-top:0px; padding-top: 0px;"  >
  
     
   
       <a href="http://www.core2plus.com/" target="_blank">
        <span class="copyright"  > Copyright © 2018 | Core2Plus </span></a>
	
</footer>
</center>
<script src="<?php echo base_url(); ?>assets/bundles/libscripts.bundle.js"></script> 
<script src="<?php echo base_url(); ?>assets/bundles/vendorscripts.bundle.js"></script> 
<script src="<?php echo base_url(); ?>assets/js/page.js"></script>
</body>
</html>