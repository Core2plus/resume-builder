
  <?php $this->load->view('Admin_Panel/include/header'); ?>

    <div class="container body">
      <div class="main_container">
        
  <?php $this->load->view('Admin_Panel/include/aside'); ?>
        
        <div class="content">
            <div class="container-fluid">
                <div class="row" style="margin: 20px 75px 20px 243px;">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="header">
                                <h4 class="title">All language Show Information <a href="<?php echo base_url('ORB/Languages'); ?>">
      <button type="button" class="btn btn-success" style="float: right;">Back</button></a></h4>
                            </div>
    <div class="row" style="margin: 0px;">
        <div class="col-sm-12 col-md-6">
        <?php
            if(isset($_SESSION['success']))
            {
                ?>
                <div class="alert alert-success">
                    <?php
                        echo $_SESSION['success'];
                    ?>
                </div>
                <?php
            }
        ?>
    </div>
            <div class="col-sm-12 col-md-6">
        <?php
            if(isset($_SESSION['error']))
            {
                ?>
                <div class="alert alert-danger">
                    <?php
                        echo $_SESSION['error'];
                    ?>
                </div>
                <?php
            }
        ?>
    </div>
</div>

                            <div class="content">

<table class="table table-striped table-dark">
  <thead>
    <tr>
      <th scope="col">No</th>
      <th scope="col">language Name</th>
      <th scope="col">language Level</th>
        <th scope="col">Handle</th>

    </tr>
  </thead>
  <tbody>
  <?php
  $no = 1;
    foreach($language as $student)
    {
      
  ?>
    <tr>
      <th scope="row"><?php echo $no++; ?></th>
      <td><?php echo $student->lang_name; ?></td>
      <td><?php echo $student->lang_percentage; ?></td>
      <td>
      
      
   <a href="<?php echo base_url('ORB/lang_deleteId/'.$student->lang_id); ?>">
      <button type="button" class="btn btn-danger"><i class="glyphicon glyphicon-trash "></i></button></a>
      
</td>
    </tr>
    <?php
}
    ?>
  </tbody>
</table>                                
      </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>


      </div>
    </div>
  
    <!-- jQuery -->
  <?php $this->load->view('Admin_Panel/include/footer'); ?> 