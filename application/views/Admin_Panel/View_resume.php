<!DOCTYPE html>

<html>
<head>
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	 <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <link href="<?php echo base_url(); ?>assets/css/resumecss.css" rel="stylesheet" type="text/css">
  
	<title>View Resume</title>
	<style type="text/css">
		@media print {
    #image {
    	width: 100px;
    	height: 100px;
    }
    .icon{
        font-size: xx-large;
        margin-right: 1%;
        color: #001092;
        margin-bottom: 0px; 
    }
    .main{
        max-width: 95%;
        margin-left:5%;
        

    }
    .left{
        width:30%;
        float: left; 
        border-right: solid;
        border-color: #001092;
        padding-left: 2%;
        padding-right: 2%;
    }

    .right{
        width:70%;
        float: left; 
        padding-left: 2%;
        
    }

    .image{
        width: 25%;
        padding: 4%;
        padding-left: 7.5%;
        
    }
    .firstname{
        border: none;
        background: white;
        color: #001092;
        font-size:x-large;
        margin-top:4%;
        min-width: 30%;

        

    }
    .lastname{
        border: none;
        background: white;
        color: #001092;
        font-size:x-large;
        margin-top:4%;
        width: auto;
       
        font-weight: bold;

    }
    .aboutmecontent{
        border: none;
        font-size: 1.5vw;
        background-color: white;
        min-width: 99%;
        min-height: 250px;
        max-height: 300px;
        size: fixed;


    }
    
    .designation{
            border: none;
        background: white;
        color: #001092;
        font-size:2vw;
        margin-top:4%;
        width: 100%;
        padding-top: 0px;
        margin-top: 0px;
        margin-bottom: 3%;
        

    }

    h2 {
   width: 100%; 
   font-size: large;
   text-align: center; 
   border-bottom: 1px solid #000; 
   line-height: 0.1em;
   margin: 10px 0 20px; 
   border-color:#001092; 
   color: #001092;
   } 
   h3{
    width: 100%;
    font-size: large;

   }

    h2 span { 
    background:#fff; 
    padding:0 10px; 
    border-color:#001092; 

    }
        .degree
{

font-weight: bold; 
background-color: white;
border:none;

}
 .content
{

font-weight: 2; 
background-color: white;
border:none;

}

.university
{

font-weight: bold;
background-color: white;
border:none;

}

.date
{
font-weight: bold;
background-color: white;
border:none;
float: right;
padding-left: 20%;
}

hr { 
    display: block;
    margin-top: 0.5em;
    margin-bottom: 0.5em;
    margin-left: auto;
    margin-right: auto;
    border-style: inset;
    border-width: 1px;
    border-color: #001092;
}

.rightheader
    {
        color: #001092; width: 90%; float: right; margin-top: 3px;
    }
}
	</style>
</head>
<body>

<div class="main">
	<div  class="header" >
			<div class="image">
      <?php
      //print_r($personal);
      //die;
      ?>
				<input id="image" type="image" width="180px" height="180px"  alt="Login" style="border-radius: 100%; border-color: white;  border-style: solid; " 
       src="<?php echo base_url().'/assets/img/'.$personal['ms_image']; ?>"">
			</div>
		</div>
	<div class="left">
		<center>
		<div class="name">
			<center>
			<span name="firstname" class="firstname">  <?php echo $personal['ms_fname']; ?> <span name="lastname" class="lastname" >  <?php echo $personal['ms_lname']; ?> </span></span>
			<br>
			<p  class="designation"><?php echo $personal['subtitle_id']; ?> <?php echo $personal['title_id']; ?> <?php echo $personal['level_id']; ?> </p></center>

		</div>

	
		<div class="aboutme">
			
			<h2><span class="leftheader">	ABOUT ME </span></h2>
			<span class="aboutmecontent"   ><?php echo $personal['ms_discription']; ?></span>

			
		</div>
	
		<div class="contactme">
			<h2><span class="leftheader" > CONTACT ME </span></h2>
			
			
			<div class="phonenumber">
				<div class="icon">
					<span class="glyphicon glyphicon-earphone " style="font-size: 20px;" ></span>
				</div>
				<input type="text"  value="<?php echo $personal['ms_contact']; ?>" disabled="true" class="phone">
				
			</div>

			<div class="Email">
					<div class="icon">
				
					<span class="glyphicon glyphicon-envelope " style="font-size: 20px;" ></span>
				</div>
				<span class="email" ><?php echo $personal['ms_email']; ?> </span>
			</div>
			<div class="location">
				<div class="icon">
				
					<span class="glyphicon glyphicon-map-marker" style="font-size: 20px;" ></span>
				</div>
				<span class="email"><?php echo $personal['ms_address']; ?><br>
				<span class="email"><?php echo $personal['ms_city']; ?> <?php echo $personal['ms_postal']; ?> </span>
			</div>
			<BR>
		</div>
	
	
		<BR>

		<div class="languages">
			<h2 ><span class="leftheader"> Languages </span></h2>
		
			<div class="languages1" style="float: left; width:20%;">
				<input type="text"  value="English " disabled="true" class="phone">
			</div>
			
		
			
		</div>
		<br>
			<br>

		<div class="personalSkills">
			<h2><span class="leftheader"> PROFESSIONAL SKILLS </span></h2>
			<?php
			   //foreach($personal as $student)
    //{

    	//if ($personal!=null) {
    		# code...
    	
    	
  ?>

  <input type="text" style="float: left; width:20%;"  value="<?php //echo $student->skill_name; ?>" disabled="true" class="phone">
 
  <div class="progress"  style=" width:50%; margin-top: 2%;    height: 8px; float:left;">
   
   <div class="progress-bar " role="progressbar" aria-valuenow="100" aria-valuemin="20" aria-valuemax="100" style="width:<?php //echo $student->skill_percentage; ?>%; background-color: #001092;">
      
    </div>
  </div>
  
  <div style="width:30%; float:left; margin-left: 5%; margin-top: 0px;">Photoshope</div>
 
  <div class="progress" style="width:50%; margin-top: 2%;    height: 8px; float:left;">
   
   <div class="progress-bar " role="progressbar"  aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width:40%; background-color: #001092;">
      
    </div>
  </div>
  <div style="width:30%; float:left; margin-left: 5%; margin-top: 0px;">Photoshope</div>
 
  <div class="progress" style="width:50%; margin-top: 2%;    height: 8px; float:left;">
   
   <div class="progress-bar " role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width:40%; background-color: #001092;">
      
    </div>
  </div>
  <div style="width:30%; float:left; margin-left: 5%; margin-top: 0px;">Photoshope</div>
 
  <div class="progress" style="width:50%; margin-top: 2%;    height: 8px; float:left;">
   
   <div class="progress-bar " role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width:40%; background-color: #001092;">
      
    </div>
  </div>
  <div style="width:30%; float:left; margin-left: 5%; margin-top: 0px;">Photoshope</div>
 
  <div class="progress" style="width:50%; margin-top: 2%;    height: 8px; float:left;">
   
   <div class="progress-bar " role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width:40%; background-color: #001092;">
      
    </div>
  </div>


		</div>
		</center>

	</div>
	<div class="right" style="">

			<!--  Academic program                    
-->

<span class="glyphicon glyphicon glyphicon-education icon" aria-hidden="true"></span>
<h3 class="rightheader">Academic Qualification<hr></h3>
 <?php

 //$i=1;

 
   // foreach($personal as $student  )
    //{
    	
    		
  ?>
<div class="master-degree">
	<input class="degree" value="<?php //echo $student->edu_name; ?>" name="master" disabled="true">
	<input class="date" value="<?php //echo $student->edu_strdate; ?> to <?php //echo $student->edu_enddate; ?>" name="MasterDate" disabled="true"><br>
	<input class="university" value="<?php //echo $student->edu_inst; ?>" name="MasterUniversity" disabled /><br>
	<div width="100%"><input class="content" value="<?php //echo $student->edu_description; ?>" name="MasterContent" disabled /></div>
	</div>
	<br>


	<?php
	
		//if($i++==3)break;
	
			

 //}

  ?>

	
<!--                                          ___________       -->

 
<!--  Professional Qualification                    
-->
<div class="leftbox">
<span class=" glyphicon glyphicon-folder-close icon" aria-hidden="true"></span>
<h3 class="rightheader">WORK EXPERIENC<hr></hr></h3></b>

<div class="master-degree">

	<input class="degree" value="<?php //echo $student->exp_designation; ?>" name="ProfessionalDegree" disabled="true">
	<input class="date" value="<?php //echo $student->exp_strdate; ?> to <?php echo $student->exp_enddate; ?>" name="ProfessionalDate" disabled="true"><br>
	<input class="content"  value="<?php //echo $student->exp_company; ?>"  disabled>
	
	<div width="100%"><input class="content" value="<?php //echo $student->exp_designation; ?>" name="MasterContent" disabled /></div>

	</div>
	<br>
</div>
<BR>
<BR>
<!-- Work Experience                       
-->
<div class="leftbox">
<span class="glyphicon glyphicon-cog icon" aria-hidden="true"></span>
<h3 class="rightheader">CERTIFICATION<hr></hr></h3></b>
<div class="master-degree">

	<input class="degree" value="<?php //echo $student->certi_name; ?>" name="ProfessionalDegree" disabled="true">
	<input class="date" value="<?php //echo $student->certi_strdate; ?> to <?php//echo $student->certi_enddate; ?>" name="ProfessionalDate" disabled="true"><br>
	<input class="content"  value="<?php //echo $student->certi_insti; ?>"  disabled>
	
	<div width="100%"><input class="content" value="<?php //echo $student->certi_description; ?>" name="MasterContent" disabled /></div>

	</div>
	<br>
<BR>

<!--                       
-->
<!-- References                       
-->
<!--wrench-->
<BR>
<div class="leftbox">
<span class="glyphicon glyphicon-link icon" aria-hidden="true"></span>
<h3 class="rightheader">REFERENCE<hr></hr></h3>

	
<div class="master-degree">

	<input class="degree" value="<?php //echo $student->ref_name; ?>" name="ProfessionalDegree" disabled="true">
	<br>
	<input class="content"  value="<?php //echo $student->ref_phone; ?>"  disabled>
	<br>
	<input class="content"  value="<?php //echo $student->ref_email; ?>"  disabled>
	<br>
	<div width="100%"><input class="content" value="<?php //echo $student->ref_desg; ?>" name="MasterContent" disabled /></div>

	</div>
	<br>
<br>
	</div>

</div>

</body>
</html>