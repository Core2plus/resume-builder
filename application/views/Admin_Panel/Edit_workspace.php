
  <?php
   //echo $all_education['ms_profile'];
   //die;
  $this->load->view('Admin_Panel/include/header'); ?>

    <div class="container body">
      <div class="main_container">
        
  <?php $this->load->view('Admin_Panel/include/aside'); ?>

        <div class="right_col" role="main">
              
              <hr>
                  
                    <div id="wizard" class="form_wizard wizard_horizontal">

                      <div id="step-1" style="margin-left:60px;">
                      <!-- step 1 -->
                      <div class="row">
                        <div class="col-sm-9">
                      
                        <h2 class="StepTitle"  style="font-family: serif;font-size:22px;margin-left:5px;">Edit Workspace Information</h2>
                        <hr><br>        

                        <form class="form-horizontal form-label-left" action="<?php echo base_url('ORB/work_editproces/'); ?>" method="POST" enctype="multipart/form-data">
                          <div class="form-group">
                            <input type="hidden"   name="exp_id" ng-model="lname" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo $all_workspace['exp_id']; ?>">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Designation <span class="required" >*</span>
                            </label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                              <input type="text" id="sec_name"  name="exp_designation" ng-model="lname" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo $all_workspace['exp_designation']; ?>">
                            </div>
                          </div>
                          <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">Company Name <span class="required">*</span>
                            </label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                              <input type="text" id="email" name="exp_company" ng-model="email" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo $all_workspace['exp_company']; ?>">
                            </div>
                          </div>

                           <div class="form-group">
                            <label for="contact" class="control-label col-md-3 col-sm-3 col-xs-12">start date <span class="required" >*</span> </label>
                            <div class="col-md- col-sm-9 col-xs-12">
                              <input class="form-control col-md-7 col-xs-12" text="text"  pattern="(0[1-9]|1[0-9]|2[0-9]|3[01]).(0[1-9]|1[012]).[0-9]{4}" placeholder="dd-mm-yyyy"   name="exp_strdate" value="<?php echo $all_workspace['exp_strdate']; ?>">
                            </div>
                          </div>

                          <div class="form-group">
                             <label class="control-label col-md-3 col-sm-3 col-xs-12" for="address">end date 
                            </label>
                             <div class="col-md-9 col-sm-9 col-xs-12">
                              <input id="input"  class="form-control col-md-7 col-xs-12" text="text"  pattern="(0[1-9]|1[0-9]|2[0-9]|3[01]).(0[1-9]|1[012]).[0-9]{4}" placeholder="dd-mm-yyyy"  name="exp_enddate" value="<?php echo $all_workspace['exp_enddate']; ?>">
                            <input type="checkbox" value="Still Working" name="exp_enddate"   onclick="check()" id="myCheck">&nbspStill Working
                            </div>
                          </div>
                          <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Description <span class="required" >*</span> </label>
                            <div class="col-md-9 col-sm-9 col-xs-12 ">
                              <textarea class="form-control " ng-model="address" required="" name="exp_description" id="address"><?php echo $all_workspace['exp_description']; ?></textarea>
                              <br>
                          <input type="submit"  id="submit_personal" class="btn btn-success btn-md" value="Update">
                        </form>
                        <div id="personal_result"></div>
                  </div>
                  </div> <!-- end row -->
                  </div>

</div>
</div>

          <div class="actionBar"><div class="msgBox"><div class="content"></div><a href="#" class="close">X</a></div><div class="loader">Loading</div><a href="" class="buttonNext btn btn-success">Next</a><a href="Education.php" class="buttonPrevious btn btn-primary">Previous</a></div>
          </div>

<script type="text/javascript">
    function check() {
    if(document.getElementById("myCheck").checked == true)
    {
    document.getElementById("input").disabled = true;
    document.getElementById("input").name= '';
    }
    else{
     document.getElementById("input").disabled = false;
     document.getElementById("input").name= 'exp_enddate';
    }
  }

</script>
  
    <!-- jQuery -->
  <?php $this->load->view('Admin_Panel/include/footer'); ?> 