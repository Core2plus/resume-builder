
  <?php $this->load->view('Admin_Panel/include/header'); ?>

    <div class="container body">
      <div class="main_container">
        
  <?php $this->load->view('Admin_Panel/include/aside'); ?>
        
        <div class="content">
            <div class="container-fluid">
                <div class="row" style="margin: 20px 75px 20px 243px;">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="header">
                                <h4 class="title">All Work Experience Information</h4>
                            </div>
    <div class="row" style="margin: 0px;">
        <div class="col-sm-12 col-md-6">
        <?php
            if(isset($_SESSION['success']))
            {
                ?>
                <div class="alert alert-success">
                    <?php
                        echo $_SESSION['success'];
                    ?>
                </div>
                <?php
            }
        ?>
    </div>

            <div class="col-sm-12 col-md-6">
        <?php
            if(isset($_SESSION['error']))
            {
                ?>
                <div class="alert alert-danger">
                    <?php
                        echo $_SESSION['error'];
                    ?>
                </div>
                <?php
            }
        ?>
    </div>
</div>

                            <div class="content">

<table class="table table-striped table-dark">
  <thead>
    <tr>
      <th scope="col">No</th>
      <th scope="col">designation</th>
      <th scope="col">Company Name</th>
      <th scope="col">start</th>
      <th scope="col">end</th>
      <th scope="col">Description</th>
      <th scope="col">handle</th>
    </tr>
  </thead>
  <tbody>
  <?php
  $no = 1;
    foreach($workspace as $student)
    {
     
  ?>
    <tr>
      <th scope="row"><?php echo $no++; ?></th>
      <td><?php echo $student->exp_designation; ?></td>
      <td><?php echo $student->exp_company; ?></td>
      <td><?php echo $student->exp_strdate; ?></td>
      <td><?php echo $student->exp_enddate; ?></td>
      <td><?php echo $student->exp_description; ?></td>
      <td>
      
      <a href="<?php echo base_url('ORB/work_edit/'.$student->exp_id); ?>">
      <button type="button" class="btn btn-warning"><i class="glyphicon glyphicon-edit"></i></button></a>
      <a href="<?php echo base_url('ORB/work_deleteId/'.$student->exp_id); ?>">
      <button type="button" class="btn btn-danger">Delete</button></a>
      <a href="<?php echo base_url('ORB/workspace'); ?>">
      <button type="button" class="btn btn-success">Done</button></a>
</td>
    </tr>
    <?php
} 
    ?>
  </tbody>
</table>                                
      </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>


      </div>
    </div>
  
    <!-- jQuery -->
  <?php $this->load->view('Admin_Panel/include/footer'); ?> 