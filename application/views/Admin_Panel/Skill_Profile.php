
  <?php $this->load->view('Admin_Panel/include/header'); ?>

    <div class="container body">
      <div class="main_container">
        
  <?php $this->load->view('Admin_Panel/include/aside'); ?>

        <div class="right_col" role="main">
              <div class="row">
                <div class="col-sm-3 col-md-3 col-lg-3" style="display: inline-flex;">
                   
                    
                    
                </div>
                <a href="<?php echo base_url('ORB/skill_show/'.$this->session->userdata('master_id')); ?>" style="float:right;">
                <button class="btn btn-info" style="min-width: 40%;"  name="button">View</button></a>
              </div>
              <hr>
                  
                    <div id="wizard" class="form_wizard wizard_horizontal">

                           <ul class="wizard_steps" style=" margin: 0 -55px 20px; ">
                        <li>
                          <a href="">
                            <span class="step_no" style="background-color: silver;">1</span>
                            <span class="step_descr">
                                              Step 1<br />
                                              <small>Personal Information</small>
                                          </span>
                          </a>
                        </li>
                        <li>
                          <a href="#step-2">
                            <span class="step_no" style="background-color: silver;">2</span>
                            <span class="step_descr">
                                              Step 2<br />
                                              <small>Education</small>
                                          </span>
                          </a>
                        </li>

                        <li>
                          <a href="#step-3">
                            <span class="step_no" style="background-color: silver;">3</span>
                            <span class="step_descr">
                                              Step 3<br />
                                              <small>Work Experience</small>
                                          </span>
                          </a>
                        </li>
                        <li>
                          <a href="#step-4">
                            <span class="step_no" style="background-color: silver;">4</span>
                            <span class="step_descr">
                                              Step 4<br />
                                              <small>Certification</small>
                                          </span>
                          </a>
                        </li>

                         <li>
                          <a href="#step-5">
                            <span class="step_no" >5</span>
                            <span class="step_descr">
                                              Step 5<br />
                                              <small>Skills Profile</small>
                                          </span>
                          </a>
                        </li>


                         <li>
                          <a href="#step-7">
                            <span class="step_no" style="background-color: silver;">6</span>
                            <span class="step_descr">
                                              Step 6<br />
                                              <small>Languages</small>
                                          </span>
                          </a>
                        </li>

                         <li>
                          <a href="#step-8">
                            <span class="step_no" style="background-color: silver;">7</span>
                            <span class="step_descr">
                                              Step 7<br />
                                              <small>Hobbies & Games</small>
                                          </span>
                          </a>
                        </li>

                        <li>
                          <a href="#step-9">
                            <span class="step_no" style="background-color: silver;">8</span>
                            <span class="step_descr">
                                              Step 8<br />
                                              <small>References</small>
                                          </span>
                          </a>
                        </li>
                   </ul>

                      <div id="step-1" style="margin-left:60px;">
                      <!-- step 1 -->
                      <div class="row">
                      
                   <div class="col-sm-9">
                      
                        <h2 class="StepTitle" style="font-family: serif;font-size:22px;margin-left: -24px;font-style: bold;"> <b> Skills Profile </b> </h2>
                     <form action="<?php echo base_url('ORB/skill_insert'); ?>" class="form-horizontal form-label-left ng-invalid ng-invalid-required ng-dirty ng-valid-number ng-valid-min ng-valid-max" method="post" accept-charset="utf-8" style="  border: 1px solid silver; align-content: center; " >
                       <span class="btn btn-danger" style="cursor: pointer; margin: 0px; padding-top: 0.5px;  height: 25px; float: right;" id="removeaddMore" my-attr="1">x</span>
                           <br> <br> <br>

                     

                          <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Skill Name <span class="required">*</span>
                            </label>
                            <div class="col-md-3 col-sm-3 col-xs-12">
                              <input type="text" id="last-name" ng-model="skill1" name="skills_name[]"  class="form-control col-md-7 col-xs-12 ng-pristine ng-untouched ng-empty ng-invalid ng-invalid-required">
                            </div>

                            <label class="control-label col-md-2 col-sm-2 col-xs-12" for="last-name">Skill % <span class="required">*</span>
                            </label>
                            <div class="col-md-2 col-sm-2 col-xs-12">
                              <input type="range" ng-model="per1" id="mySelect" onchange="myFunction()" name="last-name" max="100" min="1" class="form-control col-md-7 col-xs-12 ng-untouched ng-valid ng-not-empty ng-dirty ng-valid-number ng-valid-min ng-valid-max ng-valid-required">
                            </div>
                            <input type="text" id="demo" onchange="myFunction1()" name="skill_percent[]" class="col-md-2 col-sm-2 col-xs-12" value="50" required="" id="textInput" style="margin-top: 5px;">
                          </div>


                             <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Skill Name <span class="required">*</span>
                            </label>
                            <div class="col-md-3 col-sm-3 col-xs-12">
                              <input type="text" ng-model="skill2" id="last-name" name="skills_name[]" class="form-control col-md-7 col-xs-12 ng-pristine ng-untouched ng-empty ng-invalid ng-invalid-required">
                            </div>

                            <label class="control-label col-md-2 col-sm-2 col-xs-12" for="last-name">Skill % <span class="required">*</span>
                            </label>
                            <div class="col-md-2 col-sm-2 col-xs-12">
                              <input type="range" ng-model="per2" id="mySelect1" onchange="myFunction10()" name="last-name" max="100" min="1" class="form-control col-md-7 col-xs-12 ng-untouched ng-valid ng-not-empty ng-dirty ng-valid-number ng-valid-min ng-valid-max ng-valid-required">
                            </div>
                            <input type="text" id="demo1" onchange="myFunction11()" name="skill_percent[]" class="col-md-2 col-sm-2 col-xs-12" value="50" id="textInput1" style="margin-top: 5px;">
                          </div>


                             <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Skill Name <span class="required">*</span>
                            </label>
                            <div class="col-md-3 col-sm-3 col-xs-12">
                              <input type="text" ng-model="skill3" id="last-name" name="skills_name[]" class="form-control col-md-7 col-xs-12 ng-pristine ng-untouched ng-empty ng-invalid ng-invalid-required">
                            </div>

                            <label class="control-label col-md-2 col-sm-2 col-xs-12" for="last-name">Skill % <span class="required">*</span>
                            </label>
                            <div class="col-md-2 col-sm-2 col-xs-12">
                              <input type="range" id="mySelect2" onchange="myFunction20()" max="100" min="1" ng-model="per3"  class="form-control col-md-7 col-xs-12 ng-untouched ng-valid ng-not-empty ng-dirty ng-valid-number ng-valid-min ng-valid-max ng-valid-required">
                            </div>
                            <input type="text" id="demo2" onchange="myFunction21()" name="skill_percent[]" class="col-md-2 col-sm-2 col-xs-12" value="50" id="textInput2" style="margin-top: 5px;">
                          </div>


              <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Skill Name <span class="required">*</span>
                            </label>
                            <div class="col-md-3 col-sm-3 col-xs-12">
                              <input type="text" ng-model="skill4" id="last-name" name="skills_name[]" class="form-control col-md-7 col-xs-12 ng-pristine ng-untouched ng-empty ng-invalid ng-invalid-required">
                            </div>

                            <label class="control-label col-md-2 col-sm-2 col-xs-12" for="last-name">Skill % <span class="required">*</span>
                            </label>
                            <div class="col-md-2 col-sm-2 col-xs-12">
                              <input type="range" ng-model="per4" id="mySelect3" onchange="myFunction30()" max="100" min="1" class="form-control col-md-7 col-xs-12 ng-untouched ng-valid ng-not-empty ng-dirty ng-valid-number ng-valid-min ng-valid-max ng-valid-required">
                            </div>
                            <input type="text" id="demo3" onchange="myFunction31()" name="skill_percent[]" class="col-md-2 col-sm-2 col-xs-12" value="50" id="textInput3" style="margin-top: 5px;">
                          </div>
                    <div id="addmore_entry">
                      <div id="addagain">
                        <div id="removeaddagain" >
                            


                            <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Skill Name <span class="required">*</span>
                            </label>
                            <div class="col-md-3 col-sm-3 col-xs-12">
                              <input type="text" ng-model="skill5" id="last-name" name="skills_name[]" class="form-control col-md-7 col-xs-12 ng-pristine ng-untouched ng-empty ng-invalid ng-invalid-required">
                            </div>

                            <label class="control-label col-md-2 col-sm-2 col-xs-12" for="last-name">Skill % <span class="required">*</span>
                            </label>
                            <div class="col-md-2 col-sm-2 col-xs-12">
                              <input type="range" ng-model="per5" id="mySelect4" onchange="myFunction40()" max="100" min="1" class="form-control col-md-7 col-xs-12 ng-untouched ng-valid ng-not-empty ng-dirty ng-valid-number ng-valid-min ng-valid-max ng-valid-required">
                            </div>
                            <input type="text" id="demo4" onchange="myFunction41()" name="skill_percent[]" class="col-md-2 col-sm-2 col-xs-12" value="50" id="textInput4" style="margin-top: 5px;">
                          </div>
                        </div>

</div>
                      </div>
                     <div class="col-md-12" >
                          <input type="submit" style="float: right;" id="reference_submit" class="btn btn-primary" value="Next">


                      <span class="btn btn-primary addmore" style="cursor: pointer;margin-bottom:10px;float: left;" id="addMore" my-attr="1"><i class="glyphicon glyphicon-plus"></i> Skills </span>

                          
                        </div>

       

                        </form> 
                        <div class="col-md-12" >  <a style="
    float: right;
    /* margin-left: -40px; */
    margin-right: 58px;
    margin-top: -43px;
" href="<?php echo base_url('ORB/certification'); ?>"><button class="btn btn-success " style="min-width: 40%;"  name="button">Previous</button></a>  </div>

                                              
                  
                  </div>
                  </div>

</div>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

<script>
 $(document).ready(function(){
    $("#addMore").click(function(){
      
        $("#addagain").append('<div id="addagain"><div id="removeaddagain" ><div class="form-group"><label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Skill Name <span class="required">*</span></label><div class="col-md-3 col-sm-3 col-xs-12"><input type="text" ng-model="skill5" id="last-name" name="skills_name[]" class="form-control col-md-7 col-xs-12 ng-pristine ng-untouched ng-empty ng-invalid ng-invalid-required"></div><label class="control-label col-md-2 col-sm-2 col-xs-12" for="last-name">Skill % <span class="required">*</span></label><div class="col-md-2 col-sm-2 col-xs-12"><input type="range" ng-model="per5" id="mySelect4" onchange="myFunction40()" max="100" min="1" class="form-control col-md-7 col-xs-12 ng-untouched ng-valid ng-not-empty ng-dirty ng-valid-number ng-valid-min ng-valid-max ng-valid-required"></div><input type="text" id="demo4" onchange="myFunction41()" name="skill_percent[]" class="col-md-2 col-sm-2 col-xs-12" value="50" id="textInput4" style="margin-top: 5px;"></div>  </div>');

        return false;
        
    });

   
});
$( "#removeaddMore" ).click(function() {
  $( "#removeaddagain" ).remove();

});
function myFunction() {
    var x = document.getElementById("mySelect").value;
    document.getElementById("demo").value = x;
    
}


function myFunction1() {
    var x = document.getElementById("demo").value;
    document.getElementById("mySelect").value = x;
    
}
function myFunction10() {
    var x = document.getElementById("mySelect1").value;
    document.getElementById("demo1").value = x;
    
}


function myFunction11() {
    var x = document.getElementById("demo1").value;
    document.getElementById("mySelect1").value = x;
    
}

function myFunction20() {
    var x = document.getElementById("mySelect2").value;
    document.getElementById("demo2").value = x;
    
}


function myFunction21() {
    var x = document.getElementById("demo2").value;
    document.getElementById("mySelect2").value = x;
    
}
function myFunction30() {
    var x = document.getElementById("mySelect3").value;
    document.getElementById("demo3").value = x;
    
}


function myFunction31() {
    var x = document.getElementById("demo3").value;
    document.getElementById("mySelect3").value = x;
    
}
function myFunction40() {
    var x = document.getElementById("mySelect4").value;
    document.getElementById("demo4").value = x;
    
}


function myFunction41() {
    var x = document.getElementById("demo4").value;
    document.getElementById("mySelect4").value = x;
    
}




</script>


  
    <!-- jQuery -->
  <?php $this->load->view('Admin_Panel/include/footer'); ?> 