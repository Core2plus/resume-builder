
  <?php $this->load->view('Admin_Panel/include/header'); ?>

    <div class="container body">
      <div class="main_container">
        
  <?php $this->load->view('Admin_Panel/include/aside'); ?>

        <div class="right_col" role="main">
              <div class="row">
                <div class="col-sm-3 col-md-3 col-lg-3" style="display: inline-flex;">
                    
                   
                   
                </div>
                <a href="<?php echo base_url('ORB/reference_show/'.$this->session->userdata('master_id')); ?>" style="float:right;"><button class="btn btn-info" style="min-width: 40%;"  name="button">View</button></a>
              </div>
              <hr>
                  
                    <div id="wizard" class="form_wizard wizard_horizontal">

                           <ul class="wizard_steps" style=" margin: 0 -50px 20px; ">
                        <li>
                          <a href="">
                            <span class="step_no" style="background-color: silver;">1</span>
                            <span class="step_descr">
                                              Step 1<br />
                                              <small>Personal Information</small>
                                          </span>
                          </a>
                        </li>


                        <li>
                          <a href="#step-2">
                            <span class="step_no" style="background-color: silver;">2</span>
                            <span class="step_descr">
                                              Step 2<br />
                                              <small>Education</small>
                                          </span>
                          </a>
                        </li>

                        <li>
                          <a href="#step-3">
                            <span class="step_no" style="background-color: silver;">3</span>
                            <span class="step_descr">
                                              Step 3<br />
                                              <small>Work Experience</small>
                            </span>
                          </a>
                        </li>
                        <li>
                          <a href="#step-4">
                            <span class="step_no" style="background-color: silver;">4</span>
                            <span class="step_descr">
                                              Step 4<br />
                                              <small>Certification</small>
                                          </span>
                          </a>
                        </li>

                         <li>
                          <a href="#step-5">
                            <span class="step_no" style="background-color: silver;">5</span>
                            <span class="step_descr">
                                              Step 5<br />
                                              <small>Skills Profile</small>
                                          </span>
                          </a>
                        </li>


                         <li>
                          <a href="#step-7">
                            <span class="step_no" style="background-color: silver;">6</span>
                            <span class="step_descr">
                                              Step 6<br />
                                              <small>Languages</small>
                                          </span>
                          </a>
                        </li>

                         <li>
                          <a href="#step-8" >
                            <span class="step_no" style="background-color: silver;">7</span>
                            <span class="step_descr">
                                              Step 7<br />
                                              <small>Hobbies & Games</small>
                                          </span>
                          </a>
                        </li>

                        <li>
                          <a href="#step-9">
                            <span class="step_no">8</span>
                            <span class="step_descr">
                                              Step 8<br />
                                              <small>References</small>
                                          </span>
                          </a>
                        </li>
                   </ul>

                      <div id="step-1" style="margin-left:60px;">
                      <!-- step 1 -->
                      <div class="row">
                      
                 <div class="col-sm-9">
                      
                        <h2 class="StepTitle" style="font-family: serif;font-size:22px;margin-left: -16px;"> <b> References Profile </b></h2>
                     <form action="<?php echo base_url('ORB/referenceinsert/'.$this->session->userdata('master_id')); ?>" class="form-horizontal form-label-left ng-pristine ng-valid ng-valid-email" method="post" accept-charset="utf-8" style="  border: 1px solid silver; align-content: center; ">
                       <span class="btn btn-danger" style="cursor: pointer; margin: 0px; padding-top: 0.5px;  height: 25px; float: right;" id="removeaddMore" my-attr="1">x</span>
                           <br>

                     <div id="addmore_entry">
                      <div id="addagain">
                        
                          <div id="removeaddagain" >
                            <br>
                            <br>

                          <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Name<span class="required">*</span>
                            </label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                              <input type="text" id="name" name="name[]" ng-model="name" class="form-control col-md-7 col-xs-12 ng-pristine ng-untouched ng-valid ng-empty">     
                            </div>
                        </div>


                     <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" pattern="^((\+92)|(0092))-{0,1}\d{3}-{0,1}\d{7}$|^\d{11}$|^\d{4}-\d{7}$" for="phone">Phone<span class="required">*</span>
                            </label>
                            <div class="col-md-3 col-sm-9 col-xs-12">
                              <input type="text" ng-model="phone" id="phone" name="phone[]" class="form-control col-md-7 col-xs-12 ng-pristine ng-untouched ng-valid ng-empty">     
                            </div>
                       
                            <label class="control-label col-md-2 col-sm-3 col-xs-12" for="email">Email<span class="required">*</span>
                            </label>
                            <div class="col-md-4 col-sm-9 col-xs-12">
                              <input type="email" ng-model="email" id="email" name="email[]" class="form-control col-md-7 col-xs-12 ng-pristine ng-untouched ng-valid ng-empty ng-valid-email">     
                            </div>
                        </div>


                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="designation">Designation<span class="required">*</span>
                            </label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                              <input type="text" ng-model="designation" id="designation" name="designation[]" class="form-control col-md-7 col-xs-12 ng-pristine ng-untouched ng-valid ng-empty">     
                            </div>
                        </div>

                         <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="institute">Institution<span class="required">*</span>
                            </label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                              <input type="text" ng-model="institute" id="institute" name="institute[]" class="form-control col-md-7 col-xs-12 ng-pristine ng-untouched ng-valid ng-empty">     
                            </div>
                          </div>
                          <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="institute">Address<span class="required">*</span>
                            </label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                              <input type="text" ng-model="institute" id="institute" name="Address[]" class="form-control col-md-7 col-xs-12 ng-pristine ng-untouched ng-valid ng-empty">     
                            </div>
                        </div> 
                      </div>



                        </div>
                        <br>
                        <div class="col-md-12" >
                          <input type="submit" style="float: right;" id="reference_submit" class="btn btn-primary" value="Finish">

                      <span class="btn btn-primary addmore4" style="cursor: pointer; margin-bottom:10px;  float: left; " id="addMore" my-attr="1"><i class="glyphicon glyphicon-plus"></i> References</span>
                 
                       
                     </div>


              <!-- <a href="create_resume.php?id=77" class="btn btn-info " target="_blank"><i class="fa fa-pencil"></i> See Resume </a> -->

                        </form> 

                        <div class="col-md-12" >
<a href="<?php echo base_url('ORB/Hobbies'); ?>"><button class="btn btn-success " style="min-width: 16%;float: right;/* margin-left: 0px; */margin-right: 72px;margin-top: -43px;"  name="button">Previous</button></a>
                        </div>

                  
                  </div>
                  </div>

</div>
</div>


<script type="text/javascript">
  function title_change()
  {
    var xmlHttp = new XMLHttpRequest();
    xmlHttp.open('GET',"ajax?title="+document.getElementById('title_tb').value,false);
    xmlHttp.send(null);
  document.getElementById('sub_title').innerHTML=xmlHttp.responseText;
  //alert(xmlHttp.responseText);
  }
/*
  function change_level()
  {
        var xmlHttp = new XMLHttpRequest();
    xmlHttp.open('GET',"ajax?subtitle="+document.getElementById('level_tb').value,false);
    xmlHttp.send(null);
  document.getElementById('level').innerHTML=xmlHttp.responseText;

    //alert(xmlHttp.responseText);
    // alert(document.getElementById('level_td').value);
  
  }
*/
</script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script type="text/javascript">
 $(document).ready(function(){
    $("#addMore").click(function(){
      
        $("#addagain").append(' <hr style="font-size: 1vw;"><div id="removeaddagain" ><br><br><div class="form-group"><label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Name<span class="required">*</span></label><div class="col-md-9 col-sm-9 col-xs-12"><input type="text" id="name" name="name[]" ng-model="name" class="form-control col-md-7 col-xs-12 ng-pristine ng-untouched ng-valid ng-empty"></div></div><div class="form-group"><label class="control-label col-md-3 col-sm-3 col-xs-12" for="phone">Phone<span class="required">*</span></label><div class="col-md-3 col-sm-9 col-xs-12"><input type="text" ng-model="phone" id="phone" name="phone[]" pattern="^((\+92)|(0092))-{0,1}\d{3}-{0,1}\d{7}$|^\d{11}$|^\d{4}-\d{7}$" class="form-control col-md-7 col-xs-12 ng-pristine ng-untouched ng-valid ng-empty"></div><label class="control-label col-md-2 col-sm-3 col-xs-12" for="email">Email<span class="required">*</span></label><div class="col-md-4 col-sm-9 col-xs-12"><input type="email" ng-model="email" id="email" name="email[]" class="form-control col-md-7 col-xs-12 ng-pristine ng-untouched ng-valid ng-empty ng-valid-email"></div></div><div class="form-group"><label class="control-label col-md-3 col-sm-3 col-xs-12" for="designation">Designation<span class="required">*</span></label><div class="col-md-9 col-sm-9 col-xs-12">  <input type="text" ng-model="designation" id="designation" name="designation[]" class="form-control col-md-7 col-xs-12 ng-pristine ng-untouched ng-valid ng-empty">     </div></div> <div class="form-group"><label class="control-label col-md-3 col-sm-3 col-xs-12" for="institute">Institution<span class="required">*</span></label><div class="col-md-9 col-sm-9 col-xs-12">  <input type="text" ng-model="institute" id="institute" name="institute[]" class="form-control col-md-7 col-xs-12 ng-pristine ng-untouched ng-valid ng-empty">     </div>  </div><div class="form-group"><label class="control-label col-md-3 col-sm-3 col-xs-12" for="institute">Address<span class="required">*</span></label><div class="col-md-9 col-sm-9 col-xs-12">  <input type="text" ng-model="institute" id="institute" name="Address[]" class="form-control col-md-7 col-xs-12 ng-pristine ng-untouched ng-valid ng-empty"></div></div></div>');

        return false;
        
    });

   
});
$( "#removeaddMore" ).click(function() {
  $( "#removeaddagain" ).remove();

});
</script>
  
    <!-- jQuery -->
  <?php $this->load->view('Admin_Panel/include/footer'); ?> 