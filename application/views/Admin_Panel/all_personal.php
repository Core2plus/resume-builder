
  <?php $this->load->view('Admin_Panel/include/header'); ?>

    <div class="container body">
      <div class="main_container">
        
  <?php $this->load->view('Admin_Panel/include/aside'); ?>
        
        <div class="content">
            <div class="container-fluid">
                <div class="row" style="margin: 20px 75px 20px 243px;">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="header">
                                <h4 class="title">&nbsp; All Personal Information  <a href="<?php echo base_url('ORB/info'); ?>">
      <button type="button" class="btn btn-success" style="float: right; ">Back</button></a></h4>
                            </div>
    <div class="row" style="margin: 0px;">
        <div class="col-sm-12 col-md-6">
        <?php
            if(isset($_SESSION['success']))
            {
                ?>
                <div class="alert alert-success">
                    <?php
                        echo $_SESSION['success'];
                    ?>
                </div>
                <?php
            }
        ?>
    </div>
            <div class="col-sm-12 col-md-6">
        <?php
            if(isset($_SESSION['error']))
            {
                ?>
                <div class="alert alert-danger">
                    <?php
                        echo $_SESSION['error'];
                    ?>
                </div>
                <?php
            }
        ?>
    </div>
</div>

                            <div class="content">

<table class="table table-striped table-dark">
  <thead>
    <tr>
      <th scope="col">No</th>
      <th scope="col">First</th>
      <th scope="col">Last</th>
      <th scope="col">Email</th>
      <th scope="col">Handle</th>
    </tr>
  </thead>
  <tbody>
  <?php
  $no = 1;
    foreach($personal as $student)
    {
  ?>
    <tr>
      <th scope="row"><?php echo $no++; ?></th>
      <td><?php echo $student->ms_fname; ?></td>
      <td><?php echo $student->ms_lname; ?></td>
      <td><?php echo $student->ms_email; ?></td>
      <td>
      <a href="<?php echo base_url('ORB/PI_showId/'.$student->master_id); ?>">
      <button type="button" class="btn btn-default"><i class="glyphicon glyphicon-eye-open"></i></button></a>
      <a href="<?php echo base_url('ORB/PI_deleteId/'.$student->master_id); ?>">
      <button type="button" class="btn btn-danger"><i class="glyphicon glyphicon-trash "></i></button></a>
    
</td>
    </tr>
    <?php
}
    ?>
  </tbody>
</table>                                
      </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>


      </div>
    </div>
  
    <!-- jQuery -->
  <?php $this->load->view('Admin_Panel/include/footer'); ?> 