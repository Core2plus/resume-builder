<!DOCTYPE html>
<html>
<head>

  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
   <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  
  <title>View Resume</title>
</head>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<style>
.checked {
    color: orange;
}
</style>
<style type="text/css">
  .icon{
    font-size: xx-large;
    margin-right: 1%;
    color: #001092;
    margin-bottom: 0px; 
  }
  .main{
    max-width: 90%;
    margin-left:5%;
    margin-right:5%;

  }
  .left{
    width:30%;
    float: left; 
    border-right: solid;
    border-color: #001092;
    padding-left: 2%;
    padding-right: 2%;
  }
  .leftheader{
    color: #001092; width: 93%; float: right; margin-top: 3px;
  }

  .right{
    width:70%;
  
    float: left; 
    
    padding-left: 2%;
    
  }
  .header{
    background-image: url("../assets/img/CVheaderCV.png");
    max-width: 100%;

    height: 210px;
    margin-bottom: 1%
  }
  .image{
    width: 25%;
    padding: 4%;
    padding-left: 7.5%;
    ;
  }
  .firstname{
    border: none;
    background: #faf8ff;
    color: #001092;
    font-size:xx-large;
    margin-top:4%;
    width: auto;
    min-width: 22%;
    max-width: 27%;

  }
  .lastname{
    border: none;
    background: white;
    color: #001092;
    font-size:xx-large;
    margin-top:4%;
    width: auto;
    min-width: 25%;
    max-width: 35%;
    font-weight: bold;

  }

  .aboutmecontent{
    border: none;
    font-size: medium;
    background-color: white;
    min-width: 99%;
    min-height: 150px;
    max-height: 150px;
    size: fixed;


  }
  .glyphicon{
    
  }
  .designation{
      border: none;
    background: white;
    color: #001092;
    font-size:large;
    margin-top:4%;
    width:100%;
    padding-top: 0px;
    margin-top: 0px;
    margin-bottom: 3%;
    

  }
  .phone{
    border: none;
    font-size: medium;
    background-color: white;
    

  }
  h2 {
   width: 100%; 
   font-size: large;
   text-align: center; 
   border-bottom: 1px solid #000; 
   line-height: 0.1em;
   margin: 10px 0 20px; 
   border-color:#001092; 
   color: #001092;
   } 
   h3{
    width: 100%;
    font-size: large;

   }

  h2 span { 
    background:#fff; 
    padding:0 10px; 
    border-color:#001092; 

  }
      .degree
{

font-weight: bold; 
background-color: white;
border:none;

}
 .content
{

font-weight: 2; 
background-color: white;
border:none;

}

.university
{

font-weight: bold;
background-color: white;
border:none;

}

    .date
{

font-weight: bold;
background-color: white;
border:none;
float: right;
padding-left: 30%;
}
hr { 
    display: block;
    margin-top: 0.5em;
    margin-bottom: 0.5em;
    margin-left: auto;
    margin-right: auto;
    border-style: inset;
    
    border-color: #001092;
}
.checked {
    color: #001092;
}


  
</style>
<body>
<div class="main">
  <div class="header" style="background-image:url(<?php echo base_url().'assets/img/CVheaderCV.png'?>);">

      <div class="image">
        <input id="image" type="image" width="100%"  alt="Login" style="border-radius: 100%; border-color: white;  border-style: solid; height: 150px;" 
       src="<?php echo base_url().'assets/img/'.$query->ms_image; ?>">
      </div>
    </div>
  <div class="left">
    <center>
    <div class="name">
      <center>
      <div class="name">
      <center>
      <span name="firstname" class="firstname"><?php echo $query->ms_fname." "; ?><span name="lastname" class="lastname" ><?php echo $query->ms_mname; ?></span></span>
      <br>
      <p  class="designation">
      <?php echo $query->ms_expertise; ?>
      </p>
<!-- <p  class="designation" style="color: #252325c7;"> <?php echo "Roll Number ( '".$query->ms_rollno."' )"; ?> </p> -->
      </center>
    </div>
    <br>

    </center>

    </div>
    <div class="aboutme">
      
      <h2><span>  ABOUT ME </span></h2>
      
      <span class="aboutmecontent"   ><?php echo $query->ms_discription; ?>.</span>
    </div>
  <br>
    <div class="contactme">
      <h2><span> CONTACT ME </span></h2>
      <div class="phonenumber">
        <div class="icon">
        
          <span class="glyphicon glyphicon-earphone " style="font-size: 20px;" ></span>
        </div>
        <spam class="phone"><?php echo $query->ms_contact; ?></spam><br>
        
      </div>
      <div class="Email">
          <div class="icon">
          <span class="glyphicon glyphicon-envelope " style="font-size: 20px;" ></span>
        </div>
        <spam class="phone"><?php echo $query->ms_email; ?></spam><br>
      </div>
      <div class="location">
        <div class="icon">
        
          <span class="glyphicon glyphicon-map-marker" style="font-size: 20px;" ></span>
        </div>
        <spam class="phone"><?php echo $query->ms_address; ?><br><span class="email"><?php echo $query->ms_city; ?>, <?php echo $query->ms_postal; ?> </span></spam>
      </div>
      <BR>
    </div>
    <BR>
    <div class="languages">
      <h2><span> Languages </span></h2>
      <?php 
      //print_r($query5);
      //die;
      foreach ($query5 as $key): ?>
      <br>
      <div class="languages1 " >
        <span class="col-xs-6 col-sm-4"><b><?php echo $key->lang_name; ?></b> </span>
        <?php
        $a=0;
        if($key->lang_percentage>=$a)
        {

          ?>
        <span class="fa fa-star checked"></span>
        <?php
        }
        else{

          ?>
          <span class="fa fa-star "></span>
          <?php
        }
        ?>

        <?php
        $b=21;
        if($key->lang_percentage>=$b)
        {

          ?>
        <span class="fa fa-star checked"></span>
        <?php
        }
        else{

          ?>
          <span class="fa fa-star "></span>
          <?php
        }
        ?>
        <?php
        $c=41;
        if($key->lang_percentage>=$c)
        {

          ?>
        <span class="fa fa-star checked"></span>
        <?php
        }
        else{

          ?>
          <span class="fa fa-star "></span>
          <?php
        }
        ?>
        <?php
        $d=61;
        if($key->lang_percentage>=$d)
        {

          ?>
        <span class="fa fa-star checked"></span>
        <?php
        }
        else{

          ?>
          <span class="fa fa-star "></span>
          <?php
        }
        ?>
        <?php
        $e=81;
        if($key->lang_percentage>=$e)
        {

          ?>
        <span class="fa fa-star checked"></span>
        <?php
        }
        else{

          ?>
          <span class="fa fa-star "></span>
          <?php
        }
        ?>

      </div>

    <?php endforeach; ?>
    <br>
    <br>
    <br>
      
    </div>
  <div class="personalSkills">
      <h2><span> PROFESSIONAL SKILLS </span></h2>
<?php
  foreach ($query7 as $skill1):
?>
 <span style="float: left; width:50%; " ><?php echo $skill1->skill_name; ?> </span>
 
  <div class="progress"  style=" float: right; width:50%; margin-top: 3%;     height: 8px; float:left;">
   
   <div class="progress-bar " role="progressbar" aria-valuenow="100" aria-valuemin="20" aria-valuemax="100" style="width:<?php echo $skill1->skill_percentage; ?>%; background-color: #001092;">
    
      
    </div>

  </div>
  <br>
 <!--
  <div class="progress" style="width:50%; margin-top: 2%;    height: 8px; float:left;">
   
   <div class="progress-bar" role="progressbar" aria-valuenow="100" aria-valuemin="20" aria-valuemax="100" style="width:30%; background-color: #001092;">
      
    </div>
  </div>
  -->
 <?php endforeach; ?>
</div>
</BR>
</div>
  <div class="right" style="">

      <!--  Academic program                    
-->

<span class="glyphicon glyphicon glyphicon-education icon" aria-hidden="true"></span>
<h3 class="leftheader">ACADEMIC QUALIFICATION<hr></hr></h3><BR>

  <?php foreach ($query3 as $key): ?>
  <br>
  <div class="master-degree">
  <span class="degree" ><?php echo $key->edu_name; ?></span>
  <input class="date" value="<?php echo $key->edu_strdate; ?> to <?php echo $key->edu_enddate; ?> " name="MasterDate" disabled="true"><br>
  <span class="university" ><?php echo $key->edu_inst; ?></span><br>
  <div width="100%"><span class="content" ><?php echo $key->edu_description; ?></span><br>
  </div>
  </div>
  <?php endforeach; ?>
  <br>
  <br>
<!--                                          ___________       -->

 
<!--  Professional Qualification                    
-->
<div class="leftbox">
<span class=" glyphicon glyphicon-folder-close icon" aria-hidden="true"></span>
<h3 class="leftheader">WORK EXPERIENCE<hr></hr></h3><BR>
<?php 
//print_r($query2);
//die();
foreach ($query2 as $key): 
?>
<br></b><div class="master-degree">
  <span class="degree" ><?php echo $key->exp_designation; ?></span>
  <input class="date" value="<?php echo $key->exp_strdate; ?> to <?php echo $key->exp_enddate; ?>" name="ProfessionalDate" disabled="true"><br>
  <div width="100%"><span class="content" ><?php echo $key->exp_company; ?></span></div>
  </div>
  <div width="100%"><span class="content" ><?php echo $key->exp_description; ?></span></div>
  </BR>

<?php endforeach; ?>
  <br>
  
</div>
<BR>
<!-- Work Experience                       
-->
<div class="leftbox">
<span class="glyphicon glyphicon-cog icon" aria-hidden="true"></span>
<h3 class="leftheader">CERTIFICATION<hr></hr></h3></b>
<?php foreach ($query4 as $key): ?>
  <br>
<div class="master-degree">
  <input class="degree" value="<?php echo $key->certi_name; ?>" name="WorkExperienceCompany" disabled="true">
  <input class="date" value="<?php echo $key->certi_strdate; ?> to <?php echo $key->certi_enddate; ?>" name="WorkExperienceDate" disabled="true"><br>
 <div width="70%"><span class="content" ><?php echo $key->certi_description; ?></span></div>
  </div>
<?php endforeach; ?>
  <br>
</div>
<BR>

<!--                       
-->
<!-- References                       
-->
<!--wrench--><BR>
<div class="leftbox">
<span class="glyphicon glyphicon-link icon" aria-hidden="true"></span>
<h3 class="leftheader">REFERENCE<hr></hr></h3>

  <?php foreach($query6 as $key): ?>
<div class="References col-xs-8 col-sm-6">

  <span class="degree"><?php echo $key->ref_name; ?></span>
  <br>
  <span class="content"><?php echo $key->ref_phone; ?></span>
  <br>
  <span class="content"><?php echo $key->ref_email; ?></span>
  <br>
  <span class="content"><?php echo $key->ref_desg; ?></span>
  <br>
  <span class="content"><?php echo $key->ref_address; ?></span>
  </div>

<?php endforeach; ?>
  <br>
<br>
  </div>

</body>
</html>