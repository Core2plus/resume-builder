

<?php $this->load->view('Admin_Panel/include/header'); ?>
<style type="text/css">
  .x_content{

    float: left; margin: 0px; padding: 0px; min-width: 40%;
  }
  .input-form-main{
    background:none;
     border: none; 
     font-size: 2.5vw; 
     border-radius: 5px 8px; 
     padding-left:10%;
  }
  .input-form{
    background:none; border: none; font-size: 1.2vw; padding-left:10%;
  }
  .glyphicon{
    color: white;
  }
  .tile-stats{
    margin: 0.5%; 
    padding: 0px;
  }
  .fa{
    color: white;
  }
  .imageover {
  display: block;
  width: 100%;
  height: auto;
}

.overlay {
  position: absolute;
  top: 0;
  bottom: 0;
  left: 0;
  right: 0;
  height: 100%;
  width: 100%;
  opacity: 0;
  transition: .5s ease;
  background-color: gray;
}

</style>

    <div class="container body">
      <div class="main_container">


      <?php $this->load->view('Admin_Panel/include/aside'); ?>
        <div class="right_col" role="main">

          <div class="miditem" >
              <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="tile-stats" style="color: transparent; background-color: transparent; border-color:transparent; min-width: 0px; height: 0px;">
                </div>
              </div>
            </div>

            <div class="row" >
              <div class="col-md-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Recent Summary </h2>
                    <div class="filter" style="">
                      <div id="reportrange" class="pull-right" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc">
                        <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>
                        <span>December 30, 2017 - January 28, 2018</span> <b class="caret"></b>
                      </div>
                    </div>
                    <div class="clearfix"></div>
                  </div>


                  <div class="x_content" style="min-width: 100%;">
                  <form method="POST" action="">
                  <div class="row top_tiles">
              <div class="animated flipInY col-md-4 ">
                <div class="tile-stats"  style="background-color: #e94b3c; color: white; ">
              
                  <?php
                  //print_r($query);
                  //die();
                  //foreach ($query as $key) {
                  ?>    
                  <div class="icon" >
                    <input id="image" type="image" width="65" height="65" alt="Login" style="border-radius: 100%; border-color: white;  border-style: solid; " 
       src="<?php echo base_url()."assets/img/".$query->ms_image; ?>">
                  </div>
                  <input type="text" class="form-group input-form-main imageover" name="name" value="Personal" disabled="disabled"></input>
                  <input type="text" class="form-group input-form" name="name" value="
                  <?php 
                    echo $query->ms_fname;
                  ?>" 
                  disabled="disabled">
                    <br>
                     <input type="text" class="form-group input-form"  name="name" value="<?php  echo $query->ms_contact; ?>"   disabled="disabled">
                    <br>
                     <input type="text" class="form-group input-form"  name="name" value="<?php echo $query->ms_email; ?>"  disabled="disabled">
                    <br>
                    <?php
                  //}
                    ?>
                  </div>
    
              </div>
              <div class="animated flipInY col-md-4" >
                <div class="tile-stats" style="background-color: #004B8D; color: white;">
                
                  <div class="icon"><i class="fa fa-sort-amount-desc"></i></div>
                    <input type="text" class="form-group input-form-main"  name="name" value="Experience"  disabled="disabled">
             <?php
              if(empty($query2)){
                ?>
                <input type="text" class="form-group input-form"  name="name" value="abc"   disabled="disabled">
                    <br>
                     <input type="text" class="form-group input-form" name="name" value=gjhgjh" disabled="disabled">
                    <br>
                     <input type="text" class="form-group input-form"  name="name" value=jsdfhj"   disabled="disabled">
                    <br>
                <?php
              }
              else{
             ?>
                  <input type="text" class="form-group input-form"  name="name" value="<?php echo $query2->exp_designation; ?>"   disabled="disabled">
                    <br>
                     <input type="text" class="form-group input-form" name="name" value="<?php echo $query2->exp_description; ?>" disabled="disabled">
                    <br>
                     <input type="text" class="form-group input-form"  name="name" value="<?php echo $query2->exp_company ?>"   disabled="disabled">
                    <br>
                  </div>
              <?php
            }
              //endforeach; 
              ?>
              
              </div>
              <div class="animated flipInY col-md-4">
                <div class="tile-stats" style="background-color: #4C6A92; color: white;">
               
                  <div class="icon"><i class="glyphicon glyphicon-education"></i></div>
                  <input type="text" class="form-group input-form-main"  name="name" value="Education"  disabled="disabled">
                <?php
                  //print_r($query3);
                  if(empty($query3)){
                    ?>

                  <input type="text" class="form-group input-form"  name="name" value="jksdh"   disabled="disabled">
                   <input type="text" class="form-group input-form"  name="name" value="kjasfh"   disabled="disabled">
                   <input type="text" class="form-group input-form"  name="name" value="hjfa"   disabled="disabled">
                  
                    <?php
                  }
                  else{
               ?>
                  <input type="text" class="form-group input-form"  name="name" value="<?php echo $query3->edu_name; ?>"   disabled="disabled">
                   <input type="text" class="form-group input-form"  name="name" value="<?php echo $query3->edu_inst; ?>"   disabled="disabled">
                   <input type="text" class="form-group input-form"  name="name" value="<?php echo $query3->edu_description; ?>"   disabled="disabled">
                  
                  </div>
                  <?php
                }
                   // endforeach;
                  ?>
              </div>
              <div class="animated flipInY col-md-4">
                <div class="tile-stats" style="background-color:  #838487; color: white;">
                
                  <div class="icon"><i class="glyphicon glyphicon-globe"></i></div>
                   <input type="text" class="form-group input-form-main"  name="name" value="Languages"  disabled="disabled">
                <?php
                  //foreach($query5 as $key){
                ?>
                  <input type="text" class="form-group input-form"  name="name" value="<?php echo $query5->lang_name; ?>"   disabled="disabled">
                   <input type="text" class="form-group input-form"  name="name" value="<?php echo $query5->lang_percentage.'%'; ?>"   disabled="disabled">
                  
                  </div>
                  <?php
                  //}
                  ?>
              </div>
              <div class="animated flipInY col-md-4">
                <div class="tile-stats" style="background-color: #F2552C; color: white;">
                
                  <div class="icon"><i class="glyphicon glyphicon-tasks"></i></div>
                   <input type="text" class="form-group input-form-main"  name="name" value="Skills"  disabled="disabled">
                <?php 
                 // foreach ($query7 as $key):
                ?>
                  <input type="text" class="form-group input-form"  name="name" value="<?php echo $query7->skill_name; ?>"   disabled="disabled">
                   <input type="text" class="form-group input-form"  name="name" value="<?php echo $query7->skill_percentage."%"; ?>"   disabled="disabled">
                  
                  </div>
                  <?php
                  //endforeach;
                  ?>
              </div>
              <div class="animated flipInY col-md-4">
                <div class="tile-stats" style="background-color: #B93A32; color: white;">
                
                  <div class="icon"><i class="glyphicon glyphicon-certificate"></i></div>
                   <input type="text" class="form-group input-form-main"  name="name" value="Certification"  disabled="disabled">
                <?php
                  //foreach($query4 as $key):
                ?>
                  <input type="text" class="form-group input-form"  name="name" value="<?php echo $query4->certi_name; ?>"   disabled="disabled">
                   <input type="text" class="form-group input-form"  name="name" value="<?php echo $query4->certi_insti; ?>"   disabled="disabled">
                  
                  </div>
                  <?php
                  //endforeach;
                   ?>
              </div>

              <div class="animated flipInY col-md-4">
                <div class="tile-stats" style="background-color: #DD4124; color: white;">
                  <div class="icon"><i class="glyphicon glyphicon-tent"></i></div>
                   <input type="text" class="form-group input-form-main"  name="name" value="Games"  disabled="disabled">
                  <input type="text" class="form-group input-form"  name="name" value="checker"   disabled="disabled">
                   <input type="text" class="form-group input-form"  name="name" value="football"   disabled="disabled">
                  
                  </div>
                  
              </div>
              <div class="animated flipInY col-md-4">
                <div class="tile-stats" style="background-color: #898E8C; color: white;">
                
                  <div class="icon"><i class="glyphicon glyphicon-dashboard"></i></div>
                   <input type="text" class="form-group input-form-main"  name="name" value="Hobbies"  disabled="disabled">
                <?php
               // print_r($query8);
                //die();
                  //foreach ($query8 as $key){
                ?>
                  <input type="text" class="form-group input-form"  name="name" value="<?php echo $query8->hobby_names; ?>" disabled="disabled">
                   <input type="text" class="form-group input-form"  name="name" value="Reading noval"   disabled="disabled">
                  
                  </div>
                  <?php
                  //}
                  ?>
              </div>
              <div class="animated flipInY col-md-4">
                <div class="tile-stats" style="background-color: #034F84; color: white;">

                  <div class="icon"><i class="glyphicon glyphicon-link"></i></div>
                   <input type="text" class="form-group input-form-main"  name="name" value="Reference"  disabled="disabled">
                <?php
                 // foreach ($query6 as $key):
                ?>
                  <input type="text" class="form-group input-form"  name="name" value="<?php echo $query6->ref_name; ?>"   disabled="disabled">
                   <input type="text" class="form-group input-form"  name="name" value="<?php echo $query6->ref_desg; ?>"   disabled="disabled">
                  
                  </div>
                  <?php
                 //  endforeach;
                  ?>
              </div>
            </div>
        
            </form>
                  </div>
                </div>
              </div>
            </div>


          </div>
        </div>
        <!-- /page content -->
      </div>
    </div>

    <!-- jQuery -->
    <?php $this->load->view('Admin_Panel/include/footer'); ?>