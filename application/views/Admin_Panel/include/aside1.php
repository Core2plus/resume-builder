 
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"></script>
<div class="top_nav" >
          <div class="nav_menu">
            <nav>
              <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
              </div>

              <ul class="nav navbar-nav navbar-right">
                <li class="">
                  <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    <img src=<?php echo base_url().'assets/register_img/'.$this->session->userdata('reg_image'); ?> alt=""><?Php ?>
                    <span class=" fa fa-angle-down"></span>
                  </a>
                  <ul class="dropdown-menu dropdown-usermenu pull-right">
                    <li><a href="javascript:;"> Profile</a></li>
                    
                    <li><a href="<?php echo base_url('ORB/logout'); ?>">Log Out</a></li>

                  </ul>
                </li>
                <li class="">
                  <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    <?Php ?>
                    <span class=" fa fa-bell"></span>
                  </a>
                  <ul class="dropdown-menu dropdown-usermenu pull-right">
                    <li class="notification"><a href="javascript:;"> </a></li>
                
                    
                  </ul>
                </li>


              </ul>
            </nav>
          </div>
        </div>
<div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="javascript:void(0)" class="site_title"><img  style=" width:30px; border-radius: 50%; "  src="<?php echo base_url('assets/img/My University/favicon.png') ?>" > <span>My University</span></a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <div class="profile clearfix">
              <div class="profile_pic">
                <img src="<?php echo base_url().'assets/register_img/'.$this->session->userdata('reg_image'); ?>" alt="..." class="img-circle profile_img">
              </div>
              <div class="profile_info">
                <span>Welcome,</span>
                <h2><?php echo $this->session->userdata('reg_username'); ?></h2>
              </div>
            </div>
            <!-- /menu profile quick info -->

            <br>

            <!-- sidebar menu -->
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
                
                <ul class="nav side-menu">
                  <li ><a href="<?php echo base_url('ORB/index'); ?>"><i class="fa fa-home"></i>All Resumes</a>
        
                  </li>
                  <li ><a href="<?php echo base_url('ORB/pending'); ?>"><i class="fa fa-home"></i>Pending Resumes</a>
        
                  </li>
                  <li><a href="<?php echo base_url('ORB/approved_resumes'); ?>"><i class="fa fa-edit"></i>Approved Resumes</a>
                      
                  </li>
                  <li><a href="<?php echo base_url('ORB/denied_resumes'); ?>"><i class="fa fa-desktop"></i>Denied Resumes</a>
              
                  </li>
                </ul>
              </div>

            </div>
            <!-- /sidebar menu -->

            
            <!-- /menu footer buttons -->
          </div>
        </div>
<script type="text/javascript">
  $(document).ready(function(){
    function load_unseen_notification(view=''){
      $.ajax({
      url:"fetch.php",
      method:"POST",
      date:{view:view},
      dataType:"json",
      success:function(data)
      {
        $('.notification').html(data.notification);
        if(data.unseen_notidication>0){
          $('.count').html(data.unseen_notidication);

        }
        load_unseen_notification();
        $('#comment_form').on('submit',function(event))

      }
    })
    }
  });
</script>