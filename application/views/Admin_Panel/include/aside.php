
  <div class="top_nav" >
          <div class="nav_menu">
            <nav>
              <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
              </div>
              <ul class="nav navbar-nav navbar-right">
                <li class="">
                  <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    <img src=<?php echo base_url().'assets/register_img/'.$this->session->userdata('reg_image'); ?> alt="">
                    <span class=" fa fa-angle-down"></span>
                  </a>
                  <ul class="dropdown-menu dropdown-usermenu pull-right">
                                        
                    <li><a href="<?php echo base_url('ORB/logout'); ?>">Log Out</a></li>
                  </ul>
                </li>
              </ul>
            </nav>
          </div>
        </div>
<div class="col-md-3 left_col">
  
          <div class="left_col scroll-view" style=" background: #2A3F54; height: 850; ">
            <div class="navbar nav_title" style="border: 0;">

              <a href="javascript:void(0)" class="site_title"><img  style=" width:30px; border-radius: 50%; "  src="<?php echo base_url('assets/img/My University/favicon.png') ?>" > <span>My University</span></a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <div class="profile clearfix">
              <div class="profile_pic">
                <img src="<?php echo base_url().'assets/register_img/'.$this->session->userdata('reg_image'); ?>" alt="..." class="img-circle profile_img">
              </div>
              <div class="profile_info">
                <span>Welcome</span>
                <h2><?php echo $this->session->userdata('reg_username'); ?></h2>
              </div>
            </div>
            <!-- /menu profile quick info -->

            <br>

            <!-- sidebar menu -->
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
                
                <ul class="nav side-menu">
                  <li ><a href="<?php echo base_url('ORB/index'); ?>"><i class="fa fa-home"></i> Auhda Dashboard</a>

                  </li>
                  <li><a href="<?php echo base_url('ORB/info'); ?>"><i class="fa fa-edit"></i>Personal Information</a>        
                  </li>
                  <?php
                  if(empty($this->session->userdata('master_id')))
                    {

                  ?>
                  <li><a href="#" onclick="popup()"><i class="fa fa-desktop"></i> Education </a>
              
                  </li>
                  <li><a href="#" onclick="popup()"><i class="fa fa-table"></i>Work Experience</a>
                    <ul class="nav child_menu">
                      <li></li>
     
                    </ul>
                  </li>
                  <li><a href="#" onclick="popup()"><i class="fa fa-bar-chart-o"></i>Certification</a>
                   
                  </li>
                  <li><a href="#" onclick="popup()"><i class="fa fa-clone"></i>Skills Profile</a>
  
                  </li> 
                  <li><a href="#" onclick="popup()"><i class="fa fa-bug"></i> Languages</a>
            
                  </li>
                  <li><a href="#" onclick="popup()"><i class="fa fa-windows"></i>Hobbies &amp; Games</a>
                    
                  </li>
                  <li><a href="#" onclick="popup()"><i class="fa fa-sitemap"></i>References</a>
                  </li> 
                  <li><a href="#" onclick="popup()"><i class="fa fa-sitemap"></i>CV Resume</a>
                  </li>
                  <?php
                      }
                      else
                      {
                  ?>

                   <li><a href="<?php echo base_url('ORB/education'); ?>"><i class="fa fa-desktop"></i> Education </a>
              
                  </li>
                  <li><a href="<?php echo base_url('ORB/workspace'); ?>"><i class="fa fa-table"></i>Work Experience</a>
                    <ul class="nav child_menu">
                      <li></li>
                    </ul>
                  </li>
                  <li><a href="<?php echo base_url('ORB/Certification'); ?>"><i class="fa fa-bar-chart-o"></i>Certification</a>
                  </li>
                  <li><a href="<?php echo base_url('ORB/skill_Profile'); ?>"><i class="fa fa-clone"></i>Skills Profile</a>
                  </li>

                  <li><a href="<?php echo base_url('ORB/Languages'); ?>"><i class="fa fa-bug"></i> Languages</a>
            
                  </li>
                  <li><a href="<?php echo base_url('ORB/Hobbies'); ?>"><i class="fa fa-windows"></i>Hobbies &amp; Games</a>
                    
                  </li>
                  <li><a href="<?php echo base_url('ORB/Reference'); ?>"><i class="fa fa-sitemap"></i>References</a>
                  </li> 
                  <li><a href="<?php echo base_url('ORB/approve/'.$this->session->userdata('master_id')); ?>"><i class="fa fa-sitemap"></i>CV Resume</a>
                  </li>
                  <?php
                        }
                  ?>                 
                </ul>
              </div>
            </div>
          </div>
        </div>
      
      <script>
            function popup()
            {
              alert('Notice: You Must have to fill First Personal Information Page to access this page...!');
            }
      </script>