
  <?php $this->load->view('Admin_Panel/include/header'); ?>

    <div class="container body">
      <div class="main_container">
        
  <?php $this->load->view('Admin_Panel/include/aside1'); ?>
        
        <div class="content">
            <div class="container-fluid">
                <div class="row" style="margin: 20px 75px 20px 243px;">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="header">
                                <h4 class="title" style="padding: 0px 10px;">All Resumes</h4>
                            </div>
    <div class="row" style="margin: 0px;">
        <div class="col-sm-12 col-md-6">
        <?php
            if(isset($_SESSION['success']))
            {
                ?>
                <div class="alert alert-success">
                    <?php
                        echo $_SESSION['success'];
                    ?>
                </div>
                <?php
            }
        ?>
    </div>
            <div class="col-sm-12 col-md-6">
        <?php
            if(isset($_SESSION['error']))
            {
                ?>
                <div class="alert alert-danger">
                    <?php
                        echo $_SESSION['error'];
                    ?>
                </div>
                <?php
            }
        ?>
    </div>
</div>

                            <div class="content">

<table class="table table-striped table-dark">
  <thead>
    <tr>
      <th scope="col">No</th>
      <th scope="col">Roll No</th>
      <th scope="col">first Name</th>
      <th scope="col">first Name</th>
      <th scope="col">email</th>
      <th scope="col">Handle</th>
    </tr>
  </thead>
  <tbody>
  <?php
  $no = 1;
    foreach($personal as $student)
    {
      
  ?>
    <tr>
      <th scope="row"><?php echo $no++; ?></th>
      <td><?php echo $student->ms_rollno; ?></td>
      <td><?php echo $student->ms_fname; ?></td>
      <td><?php echo $student->ms_lname; ?></td>
      <td><?php echo $student->ms_email; ?></td>
  
      <td>
    
      <a href="<?php echo base_url('ORB/adminapproved/'.$student->master_id); ?>">
      <button type="button" id="btn" onclick="changecolor()" class="btn btn-warning" value="Approve">Approve</button></a>
      <a href="<?php echo base_url('ORB/admindenied/'.$student->master_id); ?>">
      <button type="button" id="btn1" onclick="changecolor1()" class="btn btn-danger">deny</button></a>
      <a href="<?php echo base_url('ORB/adminview_resume/'.$student->master_id); ?>">
      <button type="button" class="btn btn-success">profile</button></a>
</td>
    </tr>
    <?php
}
    ?>
  </tbody>
</table>                                
      </div>
                        </div>
                    </div>      
                </div>
            </div>
    
        </div>
      </div>
    </div>

  <script>
      function changecolor(){
      document.getElementById("btn").value = "Appoved";
      
      document.getElementById("btn").style.backgroundColor = "black";
      }

      function changecolor1(){
      document.getElementById("btn1").value = "Appoved";
      
      document.getElementById("btn1").style.backgroundColor = "brown";
      }
  </script>
    <!-- jQuery -->
  <?php $this->load->view('Admin_Panel/include/footer'); ?> 