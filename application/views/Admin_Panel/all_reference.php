
  <?php $this->load->view('Admin_Panel/include/header'); ?>

    <div class="container body">
      <div class="main_container">
        
  <?php $this->load->view('Admin_Panel/include/aside'); ?>
        
        <div class="content">
            <div class="container-fluid">
                <div class="row" style="margin: 20px 75px 20px 243px;">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="header">
                                <h4 class="title">All reference Information  <a href="<?php echo base_url('ORB/Reference'); ?>">
        
      <button type="button" class="btn btn-success" style="float: right;">back</button></a></h4>
                            </div>
    <div class="row" style="margin: 0px;">
        <div class="col-sm-12 col-md-6">
        <?php
            if(isset($_SESSION['success']))
            {
                ?>
                <div class="alert alert-success">
                    <?php
                        echo $_SESSION['success'];
                    ?>
                </div>
                <?php
            }
        ?>
    </div>
            <div class="col-sm-12 col-md-6">
        <?php
            if(isset($_SESSION['error']))
            {
                ?>
                <div class="alert alert-danger">
                    <?php
                        echo $_SESSION['error'];
                    ?>
                </div>
                <?php
            }
        ?>
    </div>
</div>

                            <div class="content">

<table class="table table-striped table-dark">
  <thead>
    <tr>
      <th scope="col">No</th>
      <th scope="col">Name</th>
      <th scope="col">Phone number</th>
      <th scope="col">email</th>
      <th scope="col">designation</th>
      <th scope="col">Institute</th>
      <th scope="col">Handle</th>
    </tr>
  </thead>
  <tbody>
  <?php
  $no = 1;
    foreach($reference as $student)
    {
      
  ?>
    <tr>
      <th scope="row"><?php echo $no++; ?></th>
      <td><?php echo $student->ref_name; ?></td>
      <td><?php echo $student->ref_phone; ?></td>
      <td><?php echo $student->ref_email; ?></td>
      <td><?php echo $student->ref_desg; ?></td>
      <td><?php echo $student->ref_instit; ?></td>
      <td>
     
      <a href="<?php echo base_url('ORB/reference_edit/'.$student->ref_id); ?>">
      <button type="button" class="btn btn-warning"><i class="glyphicon glyphicon-edit"></i> </button></a>
      <a href="<?php echo base_url('ORB/reference_deleteId/'.$student->ref_id); ?>">
      <button type="button" class="btn btn-danger"><i class="glyphicon glyphicon-trash "></i></button></a>
    
</td>
    </tr>
    <?php
}
    ?>
  </tbody>
</table>                                
      </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>


      </div>
    </div>
  
    <!-- jQuery -->
  <?php $this->load->view('Admin_Panel/include/footer'); ?> 