  


<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>



     <?php $this->load->view('Admin_Panel/include/header');  ?>
  


    <div class="container body">
      <div class="main_container">
        
  <?php $this->load->view('Admin_Panel/include/aside'); ?>

        <div class="right_col" role="main">
              <div class="row">
                <div class="col-sm-3 col-md-3 col-lg-3" style="display: inline-flex;">
      
       <!-- <a href="<?php echo base_url('ORB/index'); ?>"><button class="btn btn-success " style="min-width: 40%;"  name="button">Previous</button></a> -->

        <!-- <a href="<?php echo base_url('ORB/education'); ?>"><button class="btn btn-primary " style="min-width: 40%;"  name="button">Next</button></a> -->
                </div>
                 <a href="<?php echo base_url('ORB/PI_show/'.$this->session->userdata('reg_id')); ?>" style="float:right;">
      <button type="button" class="btn btn-info">View</button></a>
              </div>
              <hr>
                  
                    <div id="wizard" class="form_wizard wizard_horizontal">

                           <ul class="wizard_steps" style=" margin: -12px -46px 29px; ">
                        <li>
                          <a href="">
                            <span class="step_no">1</span>
                            <span class="step_descr">
                                              Step 1<br />
                                              <small>Personal Information</small>
                                          </span>
                          </a>
                        </li>


                        <li>
                          <a href="#step-2">
                            <span class="step_no" style="background-color: silver;">2</span>
                            <span class="step_descr">
                                              Step 2<br />
                                              <small>Education</small>
                                          </span>
                          </a>
                        </li>

                        <li>
                          <a href="#step-3">
                            <span class="step_no" style="background-color: silver;">3</span>
                            <span class="step_descr">
                                              Step 3<br />
                                              <small>Work Experience</small>
                                          </span>
                          </a>
                        </li>
                        <li>
                          <a href="#step-4">
                            <span class="step_no" style="background-color: silver;">4</span>
                            <span class="step_descr">
                                              Step 4<br />
                                              <small>Certification</small>
                                          </span>
                          </a>
                        </li>

                         <li>
                          <a href="#step-5">
                            <span class="step_no" style="background-color: silver;">5</span>
                            <span class="step_descr">
                                              Step 5<br />
                                              <small>Skills Profile</small>
                                          </span>
                          </a>
                        </li>


                         <li>
                          <a href="#step-7">
                            <span class="step_no" style="background-color: silver;">6</span>
                            <span class="step_descr">
                                              Step 6<br />
                                              <small>Languages</small>
                                          </span>
                          </a>
                        </li>

                         <li>
                          <a href="#step-8">
                            <span class="step_no" style="background-color: silver;">7</span>
                            <span class="step_descr">
                                              Step 7<br />
                                              <small>Hobbies & Games</small>
                                          </span>
                          </a>
                        </li>

                        <li>
                          <a href="#step-9">
                            <span class="step_no" style="background-color: silver;">8</span>
                            <span class="step_descr">
                                              Step 8<br />
                                              <small>References</small>
                                          </span>
                          </a>
                        </li>
                   </ul>

                      <div id="step-1" style="margin-left:60px;">
                      <!-- step 1 -->
                      <div class="row">
                        <div class="col-sm-9">
                      
                        <h2 class="StepTitle"  style="font-family: sans-serif;   color: ; font-size:22px;margin-left:5px;">  <b>Personal Information </b> </h2>
                        <?php
                        if($all_personals['ms_fname']!=null)
                        {
                        ?>
                        <form class="form-horizontal form-label-left" action="<?php echo base_url('ORB/PI_editprocess'); ?>" method="POST" enctype="multipart/form-data">
                          <?php
                            }else
                            {
                          ?>
                       
                        <form class="form-horizontal form-label-left" action="<?php echo base_url('ORB/PI_process'); ?>" method="POST" enctype="multipart/form-data">
                          <?php
                        }
                          ?>
         
              <div class="form-group">
                <div class="row">
                  <label for="title" class="control-label col-md-3 col-sm-3 col-xs-12">Expertise </label>
                      
                  <div class="col-md-9 col-sm-9 col-xs-12">
                       
                  <input name="ms_expertise" placeholder="Not Mandatory" value="<?php echo $all_personals['ms_expertise']; ?>" class="form-control ">
                    
                     
                      
                    
                   
                  </div>

                    <!-- </div> -->
       
               
          
                  <!-- </div> -->

                  </div>
                </div>


                           <div class="form-group">
                            <label for="title" class="control-label col-md-3 col-sm-3 col-xs-12">Select Your Image <span class="required" >*</span> </label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                             
                              <label class="fileContainer">
                              Upload Your Picture here!
                              <input type="file" name="client_image"  class="form-control col-md-9 col-xs-12"  id="user_image" accept="image/*"/>
                          </label>
                            </div>
                          </div>

   <!-- <textarea ng-show="myVar" id="textarea" height="200" width="100%" style="display: none;">Welcome to my home.</textarea> -->
        <div class="displayRadioButton"></div>


                          <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name"> Roll No* <span class="required"></span>
                            </label>
                            <div class="col-md-2 col-sm-9 col-xs-12">  
                              <input type="number" id="rollno"  name="rollno"  required="required" class="form-control col-md-7 col-xs-12" value="<?php echo $all_personals['ms_rollno']; ?>" placeholder="Roll Number">
                            </div>
                            <label class="control-label col-md-2 col-sm-3 col-xs-12" for="name">Social Link <span class="required"></span>
                            </label>
                            <div class="col-md-5 col-sm-9 col-xs-12">  
                              <input type="text" id="rollno"  name="social"   class="form-control col-md-7 col-xs-12" value="<?php echo $all_personals['ms_social']; ?>" placeholder="Not Mandatory">
                            </div>
                          </div>

                          <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Full Name <span class="required" >*</span>
                            </label>
                            <div class="col-md-3 col-sm-9 col-xs-12">
                              <input type="text" id="name"  name="name"  placeholder="First Name" ng-model="fname" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo $all_personals['ms_fname']; ?>">
                            </div>
                            
                            
                            <div class="col-md-3 col-sm-9 col-xs-12">
                              <input type="text" id="name"  name="mname" placeholder="Middle Name"  ng-model="fname"  class="form-control col-md-7 col-xs-12" value="<?php echo $all_personals['ms_mname']; ?>">
                            </div>
                       
                           
                            <div class="col-md-3 col-sm-9 col-xs-12">
                              <input type="text" placeholder="Last Name" value="<?php echo $all_personals['ms_lname']; ?>" required="required" id="sec_name"  name="sec_name" ng-model="lname"  class="form-control col-md-7 col-xs-12">
                            </div>
                          </div>

                          <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">Email <span class="required">*</span>
                            </label>
                            <div class="col-md-4 col-sm-9 col-xs-12">
                              <input type="email" value="<?php echo $all_personals['ms_email']; ?>"  id="email" name="email" ng-model="email" required="required" class="form-control col-md-7 col-xs-12">
                            </div>
                          

                          
                            <label for="contact" class="control-label col-md-2 col-sm-3 col-xs-12">Contact <span class="required" >*</span> </label>
                            <div class="col-md-3">
                              <input id="contact" value="<?php echo $all_personals['ms_contact']; ?>" required ng-model="contact"  class="form-control col-md-7 col-xs-12" type="text" name="contact">
                            </div>
                          </div>


                          <div class="form-group">
                             <label class="control-label col-md-3 col-sm-3 col-xs-12" for="address">Address <span class="required">*</span>
                            </label>
                             <div class="col-md-9 col-sm-9 col-xs-12">
                              <input type="type" value="<?php echo $all_personals['ms_address']; ?>"  class="form-control col-md-7 col-xs-12" ng-model="address" required name="address" id="address">
                            </div>
                          </div>
                          
                           <div class="form-group">
                             <label class="control-label col-md-3 col-sm-3 col-xs-12" for="city">City <span class="required">*</span>
                            </label>
                             <div class="col-md-3 col-sm-3 ">
                              <input type="type"  class="form-control col-md-7 col-xs-12" ng-model="city" value="<?php echo $all_personals['ms_city']; ?> " name="city" id="city">
                            </div>
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="postal">Postal Code <span class="required">*</span>
                            </label>
                             <div class="col-md-3 col-sm-3">
                              <input type="type" value="<?php echo $all_personals['ms_postal']; ?>"  class="form-control col-md-7 col-xs-12" ng-model="postal" required="" name="postal" id="postal">
                            </div>
                          </div>
                         
                           <div class="form-group">
                             <label class="control-label col-md-3 col-sm-3 col-xs-12" for="discription">Objective <span class="required">*</span>
                            </label>
                             <div class="col-md-9 col-sm-9 col-xs-12">
                              <textarea type="type"  class="form-control col-md-7 col-xs-12" ng-model="discription" required="" name="discription" id="discription"><?php echo $all_personals['ms_discription']; ?></textarea>
                            </div>
                          </div>

                           <div class="form-group">
                             <label class="control-label col-md-3 col-sm-3 col-xs-12" for="city">Department<span class="required">*</span>
                            </label>
                             <div class="col-md-4 col-sm-3 ">
                              <select type="type" required="required"  class="form-control col-md-7 col-xs-12" id="title_tb" onchange="title_change()" name="ms_department" >

                                <option> Select Department </option>
                               
                                     <?php 
                                                 $connect = mysqli_connect('localhost','root','','myuni_db');


                      $q = "select * from dapartment";



                      $query = mysqli_query($connect,$q);
                    
                        while($title = mysqli_fetch_assoc($query))
                      {
                              ?>

                                <option value="<?php echo $title['dapt_name']; ?>"><?php echo $title['dapt_name']; ?></option>
                              <?php }?>
                              
                              </select>
                              
                            </div>
                            <label class="control-label col-md-1 col-sm-3 col-xs-12" for="postal">Program*
                            </label>
                            <div class="col-md-4 col-sm-3 ">
                              <select class="form-control col-md-7 col-xs-12" name="ms_program" id="sub_title" >
                    <option value="">Select an Option</option>
                </select>
                          </div>
                        </div>




                       


                         
                          
                       
                   




                           <div class="form-group">
                             <label class="control-label col-md-3 col-sm-3 col-xs-12" for="city">Date Of Birth <span class="required">*</span>
                            </label>


                             <div class="col-md-3 col-sm-3 ">

                              <input type="text"  placeholder="DD-MM-YYYY " value="<?php echo $all_personals['ms_dob']; ?>" 
                              pattern="(0[1-9]|1[0-9]|2[0-9]|3[01]).(0[1-9]|1[012]).[0-9]{4}"  ng-model="date"  required="" name="date_of_birth" class="date-picker form-control col-md-12 " required="required">


                            </div>


                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="postal">Gender <span class="required">*</span>
                            </label>
                             <div class="col-md-3 col-sm-3" style=" padding-left: 20px; ">
 
 <div id="gender"  class="btn-group" data-toggle="buttons" 
                              style=" height: 33px;  color: #333;
                                      background-color: #8c8c8c;
                                      border-color: #ccc; " >
                        <label class="btn btn-default" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default" >

                       <input type="radio" ng-model="gender" name="gender" id="gender" value="male" >

                        &nbsp; Male &nbsp;
                                
                        </label>

                                <label  class="btn btn-default" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">

                                <input type="radio" ng-model="gender" name="gender" id="gender" value="female"> Female
                                
                                </label>
                                 
                              </div>
                            
                            



                            </div>
                          </div>




                           <div id="selectme" class="form-group" style="display:none;">
                          
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Specialization Course<span class="required">*</span></label>
                          <div class="col-md-9 col-sm-3 col-xs-12">
                               <select class=" form-control " name="Specialization">
                          <option>none</option>
                            <option>Human Resource Management</option>
                            <option>Supply Chain Management</option>
                            <option>Finance</option>
                            <option>Marketing</option>
                            <option>Management Information Systems (MIS)</option>
                            <option>Technology Management</option>
                          </select>
                        </div>
                      </div>
                    
                     
                      

                <input type="submit" id="submit_personal" value="Next" class="btn btn-success btn-md" style="float: right;" >
                          




                        </form>

                        <a href="<?php echo base_url('ORB/index'); ?>" style="float: right;"  >
                            <button class="btn btn-primary"  style="min-width: 32%;padding-left: 20px;padding-top: 5px;margin-top: -51px;margin-right: 64px;"  name="button">Previous</button>
                           </a>

                         

                        
                         
                         

                           

                        <div id="personal_result"></div>
                      
                  </div>


                  </div> <!-- end row -->
                  </div>

</div>
</div>

          <div class="actionBar"><div class="msgBox"><div class="content"></div><a href="#" class="close">X</a></div><div class="loader">Loading</div><a href="" class="buttonNext btn btn-success">Next</a><a href="Education.php" class="buttonPrevious btn btn-primary">Previous</a></div>
          </div>


<script type="text/javascript">
  function title_change()
  {
    var xmlHttp = new XMLHttpRequest();
    xmlHttp.open('GET',"ajax?title="+document.getElementById('title_tb').value,false);
    xmlHttp.send(null);
  document.getElementById('sub_title').innerHTML=xmlHttp.responseText;
  //alert(xmlHttp.responseText);
  }

</script>
<script>
function displayVals() {
  var singleValues = $( "#sub_title" ).val();

 if((singleValues =="Bachelor of Business Administration (BBA)") || (singleValues =="Master of Business Administration (MBA)")){
  $( "#selectme" ).css("display","block");
}else{
  $( "#selectme" ).css("display","none");
}
}
 
$( "select" ).change( displayVals );
displayVals();
</script>
  
    <!-- jQuery -->
  <?php $this->load->view('Admin_Panel/include/footer'); ?> 