
  <?php $this->load->view('Admin_Panel/include/header'); ?>

    <div class="container body">
      <div class="main_container">
        
  <?php $this->load->view('Admin_Panel/include/aside'); ?>

        <div class="right_col" role="main">
              <div class="row">
                <div class="col-sm-3 col-md-3 col-lg-3" style="display: inline-flex;">
                    
                    
                    
                </div>
                <a href="<?php echo base_url('ORB/hobbies_show/'.$this->session->userdata('master_id')); ?>" style="float:right;"><button class="btn btn-info" style="min-width: 40%;"  name="button">View</button></a>
              </div>
              <hr>
                  
      <div id="wizard" class="form_wizard wizard_horizontal" style="height: 563px;">

                           <ul class="wizard_steps" style="    margin: 0 -56px 20px;">
                        <li>
                          <a href="">
                            <span class="step_no" style="background-color: silver;">1</span>
                            <span class="step_descr">
                                              Step 1<br />
                                              <small>Personal Information</small>
                                          </span>
                          </a>
                        </li>


                        <li>
                          <a href="">
                            <span class="step_no" style="background-color: silver;">2</span>
                            <span class="step_descr">
                                              Step 2<br />
                                              <small>Education</small>
                                          </span>
                          </a>
                        </li>

                        <li>
                          <a href="#step-3">
                            <span class="step_no" style="background-color: silver;">3</span>
                            <span class="step_descr">
                                              Step 3<br />
                                              <small>Work Experience</small>
                                          </span>
                          </a>
                        </li>
                        <li>
                          <a href="#step-4">
                            <span class="step_no" style="background-color: silver;">4</span>
                            <span class="step_descr">
                                              Step 4<br />
                                              <small>Certification</small>
                                          </span>
                          </a>
                        </li>

                         <li>
                          <a href="#step-5">
                            <span class="step_no" style="background-color: silver;">5</span>
                            <span class="step_descr">
                                              Step 5<br />
                                              <small>Skills Profile</small>
                                          </span>
                          </a>
                        </li>


                         <li>
                          <a href="#step-7">
                            <span class="step_no" style="background-color: silver;">6</span>
                            <span class="step_descr">
                                              Step 6<br />
                                              <small>Languages</small>
                                          </span>
                          </a>
                        </li>

                         <li>
                          <a href="#step-8">
                            <span class="step_no" >7</span>
                            <span class="step_descr">
                                              Step 7<br />
                                              <small>Hobbies & Games</small>
                                          </span>
                          </a>
                        </li>

                        <li>
                          <a href="#step-9">
                            <span class="step_no" style="background-color: silver;">8</span>
                            <span class="step_descr">
                                              Step 8<br />
                                              <small>References</small>
                                          </span>
                          </a>
                        </li>
                   </ul>

                      <div id="step-1" style="margin-left:60px;">
                      <!-- step 1 -->


                            <div class="row">
                      
                   <div class="col-sm-9">
                     
                        

                  
           <h2 class="StepTitle" style="font-family: serif;font-size:22px;margin-left: -18px;"><b>Hobies</b></h2>
                          <form action="<?php echo base_url('ORB/hobbies_insert'); ?>" class="form-horizontal form-label-left" method="post" accept-charset="utf-8" style="  border: 1px solid silver; align-content: center; ">
                         <div id="addmore_entry" >
                           <span class="btn btn-danger" style="cursor: pointer; margin: 0px; padding-top: 0.5px;  height: 25px; float: right;" id="removeaddMore" my-attr="1">x</span>
                        
                        <div id="addagain" >
                          <div id="removeaddagain" >
                            <br>
                            <br>
                      
                  
                   <input type="text" id="font_small" name="hobbies[]" class="form-control col-md-12 col-sm-9 col-xs-12 " placeholder="Other Hobbies">
                   <br>

                </div>
              </div>

            </div>
            <br>

            <div class="col-md-12" >
                    
                          <input type="submit" style="float: right;" id="reference_submit" class="btn btn-primary" value="Next">


                      <span class="btn btn-primary addmore" style="cursor: pointer; margin-bottom:10px; float: left;" id="addMore" my-attr="1"><i class="glyphicon glyphicon-plus"></i> Hobby</span>
</div>
                          
                        </div>

                        </form>

                         <div class="col-md-12" >  

            <a href="<?php echo base_url('ORB/Languages'); ?>"><button class="btn btn-success " style="min-width: 11%;margin-right: 32px;margin-left: 428px;margin-top: -70px;"  name="button">Previous</button></a>     

                        </div>      

                      </div>
                      </div>

                                
                  
                  </div>
                  </div>

</div>
</div>

         
          </div>
          <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script>
   $(document).ready(function(){
    $("#addMore").click(function(){
      
        $("#addagain").append('<div id="addagain" ><div id="removeaddagain" > <br><br><input type="text" id="font_small" name="hobbies[]" class="form-control col-md-12 col-sm-9 col-xs-12 " placeholder="Other Hobbies"><br></div></div>');

        return false;
        
    });

   
});
$( "#removeaddMore" ).click(function() {
  $( "#removeaddagain" ).remove();

});</script>

<script type="text/javascript">
  
  
    <!-- jQuery -->
  <?php $this->load->view('Admin_Panel/include/footer'); ?> 