
  <?php $this->load->view('Admin_Panel/include/header'); ?>

    <div class="container body">
      <div class="main_container">
        
  <?php $this->load->view('Admin_Panel/include/aside'); ?>
        
        <div class="content">
            <div class="container-fluid">
                <div class="row" style="margin: 20px 250px;">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="header">
                                <h4 class="title"> Profile</h4>
                            </div>
                            <a href="<?php echo base_url('ORB/PI_show/'.$this->session->userdata('reg_id')); ?>">
                            <button type="button" class="btn btn-primary">Back</button></a>
                            <div class="content">

  <table class="table">
  <?php 
    foreach($personal as $profile){
  ?>
  <tbody>
  <tr>
  <th scope="col">Roll Number</th>    
  </tr>
  <tr>
    <td><?php echo $profile->ms_rollno; ?></td>
  </tr>
  

    <tr>
      <th scope="col">Name</th>
      <th scope="col">Father Name</th>
      <th scope="col">Email</th>
    </tr>  
    <tr>
      <td><?php echo $profile->ms_fname; ?></td>
      <td><?php echo $profile->ms_lname; ?></td>
      <td><?php echo $profile->ms_email; ?></td>
    </tr>
    <tr>
      <th scope="col">Contact</th>
      <th></th>
      <th scope="col">Address</th>
    </tr>
    <tr>
      <td><?php echo $profile->ms_contact; ?></td>
      <td></td>
      <td><?php echo $profile->ms_address; ?></td>
    </tr>
    
    <tr>
      <th scope="col">Gender</th>
      <th scope="col">Date of Birth</th>
      </tr>
    <tr>
      <td><?php echo $profile->ms_gender; ?></td>
      <td><?php echo $profile->ms_dob; ?></td>
      <td></td>
    </tr>
    
    <tr>
      <th scope="col">Image</th>
    </tr>
    <tr>
      <td><img src="<?php echo base_url().'assets/img/'.$profile->ms_image; ?>" style="width:200px; border-radius: 2%; padding:5px; border: 1px solid #e2e2e0;"/></td>
    </tr>
<?php
}
?>
  </tbody>
</table>
                            <div class="header">
                            
                            </div>                                
      </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>


      </div>
    </div>
  
    <!-- jQuery -->
  <?php $this->load->view('Admin_Panel/include/footer'); ?> 