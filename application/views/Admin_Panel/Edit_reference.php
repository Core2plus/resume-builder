
  <?php
   //echo $all_education['ms_profile'];
   //die;
  $this->load->view('Admin_Panel/include/header'); ?>

    <div class="container body">
      <div class="main_container">
       
  <?php $this->load->view('Admin_Panel/include/aside'); ?>

        <div class="right_col" role="main">
              
              <hr>
                    <div id="wizard" class="form_wizard wizard_horizontal">

                      <div id="step-1" style="margin-left:60px;">
                      <!-- step 1 -->
                      <div class="row">
                        <div class="col-sm-9">                    
                        <h2 class="StepTitle"  style="font-family: serif;font-size:22px;margin-left:5px;">Edit Reference Information</h2>
                        <hr><br>        
                        <form class="form-horizontal form-label-left" action="<?php echo base_url('ORB/reference_editproces/'); ?>" method="POST" enctype="multipart/form-data">
                          <div class="form-group">
                            <input type="hidden"   name="ref_id" ng-model="lname" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo $all_reference['ref_id']; ?>">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Name <span class="required" >*</span>
                            </label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                              <input type="text" id="sec_name"  name="ref_name" ng-model="lname" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo $all_reference['ref_name']; ?>">
                            </div>
                          </div>
                          <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">Phone <span class="required">*</span>
                            </label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                              <input type="text" id="email" name="ref_phone" ng-model="email" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo $all_reference['ref_phone']; ?>">
                            </div>
                          </div>

                          <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">email <span class="required">*</span>
                            </label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                              <input type="text" id="email" name="ref_email" ng-model="email" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo $all_reference['ref_email']; ?>">
                            </div>
                          </div>
                          <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">Designation <span class="required">*</span>
                            </label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                              <input type="text" id="email" name="ref_desg" ng-model="email" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo $all_reference['ref_desg']; ?>">
                            </div>
                          </div>
                          
                          <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Institute <span class="required" >*</span> </label>
                            <div class="col-md-9 col-sm-9 col-xs-12 ">
                              <input class="form-control " ng-model="address" required="" name="ref_instit" id="address" value="<?php echo $all_reference['ref_instit']; ?>">
                              <br>
                          <input type="submit"  id="submit_personal" class="btn btn-success btn-md" value="Update">
                        </form>
                        <div id="personal_result"></div>
                  </div>
                  </div> <!-- end row -->
                  <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">Address <span class="required">*</span>
                            </label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                              <input type="text" id="email" name="ref_address" ng-model="email" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo $all_reference['ref_address']; ?>">
                            </div>
                          </div>
                  </div>

</div>
</div>
          <div class="actionBar"><div class="msgBox"><div class="content"></div><a href="#" class="close">X</a></div><div class="loader">Loading</div><a href="" class="buttonNext btn btn-success">Next</a><a href="Education.php" class="buttonPrevious btn btn-primary">Previous</a></div>
          </div>

<script type="text/javascript">
  function title_change()
  {
    var xmlHttp = new XMLHttpRequest();
    xmlHttp.open('GET',"ajax?title="+document.getElementById('title_tb').value,false);
    xmlHttp.send(null);
  document.getElementById('sub_title').innerHTML=xmlHttp.responseText;
  //alert(xmlHttp.responseText);
  }

</script>
  
    <!-- jQuery -->
  <?php $this->load->view('Admin_Panel/include/footer'); ?> 