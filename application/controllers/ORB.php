<?php


defined('BASEPATH') OR exit('No direct script access allowed');
class ORB extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->helper('url');		
		$method = $this->router->fetch_method();
		$methods = array('index');
		if(in_array($method,$methods))
		{
			if(!$this->session->has_userdata('logged'))
			{
				redirect(base_url().'ORB/login_view');
			}
		}
		$this->load->library('session');
	}

	public function approved_resumes()
	{
		$this->load->model('ORB_Model');
		$data['personal'] = $this->ORB_Model->approveshow();
		$this->load->view('Admin_Panel/approveshow',$data);
	}

	public function pending()
	{
		$this->load->model('ORB_Model');
		$data['personal'] = $this->ORB_Model->pendingshow();
		$this->load->view('Admin_Panel/pendingshow',$data);
	}

	public function denied_resumes()
	{
		$this->load->model('ORB_Model');
		$data['personal'] = $this->ORB_Model->deniedshow();
		$this->load->view('Admin_Panel/deniedshow',$data);
	}

	public function index()
	{


		if($this->session->userdata('reg_username')=='admin')
		{
			$this->load->model('ORB_Model');
		    $data['personal'] = $this->ORB_Model->adminshow();
			$this->load->view('Admin_Panel/admin',$data);
		}
		else{
	
		if($this->session->has_userdata('logged'))
		{
			$this->load->model('ORB_Model');
				$data['query'] = $this->ORB_Model->get_infodash();
				$data['query2'] = $this->ORB_Model->get_expdash();
				$data['query3'] = $this->ORB_Model->get_edudash();
				$data['query4'] = $this->ORB_Model->get_certidash();

				$data['query5'] = $this->ORB_Model->get_langdash();

				$data['query6'] = $this->ORB_Model->get_refdash();
				$data['query7'] = $this->ORB_Model->get_skilldash();

				$data['query8'] = $this->ORB_Model->get_hobbydash();
			$this->load->view('Admin_Panel/index',$data);
		}
	
	    }
	}

	public function login_view()
	{
		$this->load->view('sign_in');
	}

	public function login()
	{


		$email = $this->input->post('email');
		$password = $this->input->post('password');			
		$this->load->model('ORB_Model');
		$result = $this->ORB_Model->login_process($email,$password);
		if($result == TRUE)
			{	
		//	print_r($result);
		//	die;
			$this->session->set_userdata('logged');
			$session = array(
				'logged' => TRUE
				);
			$this->session->set_userdata($session);
			redirect(base_url().'ORB/index/');
		}
		else
		{
			$this->session->set_flashdata('error','Invalid email or password');
			redirect(base_url().'ORB/login_view');
		}
	}

	public function logout()
	{
		$this->session->sess_destroy();
		redirect(base_url());
	}

	public function register()
	{
		$this->load->view('sign_up');
	}
	public function register_form()
	{
		$this->load->model('ORB_Model');
		$data = $this->input->post();

		if(empty($data))
		{
 			redirect('ORB/register');
		}	

		$query = $this->ORB_Model->register_process($data);

		if($query){
			$this->session->set_flashdata('success','You Are Registerd Now');
			redirect(base_url().'ORB/login_view');
		}else{

			$this->session->set_flashdata('error','Sorry.! Problum is accured ');
			redirect(base_url().'ORB/register');
	}


	}

	public function info()
	{	
		$this->load->model('ORB_Model');
		
		$data['all_personals'] = $this->ORB_Model->edit();

		
		$this->load->view('Admin_Panel/Personal_information',$data);
	}

	public function PI_process()
	{
		$this->load->model('ORB_Model');
		$data = $this->input->post();
		$this->ORB_Model->PI_insert($data);
	}

	public function reference_show($id)
	{
		$this->load->model('ORB_Model');
		$data['reference'] = $this->ORB_Model->get_inforeference($id);
		$this->load->view('Admin_Panel/all_reference',$data);
	}

	public function reference_showId($id)
	{
		$this->load->model('ORB_Model');
		$data['reference'] = $this->ORB_Model->get_infoIdreference($id);
		$this->load->view('Admin_Panel/workspace',$data);
	}

	public function edu_show($id)
	{
		$this->load->model('ORB_Model');
		$data['education'] = $this->ORB_Model->get_infoedu($id);
		$this->load->view('Admin_Panel/all_education',$data);
	}

	public function edu_showId($id)
	{
		$this->load->model('ORB_Model');
		$data['education'] = $this->ORB_Model->get_infoIdedu($id);
		$this->load->view('Admin_Panel/education',$data);
	}

	public function edu_deleteId($id)
	{
		$this->load->model('ORB_Model');
		$result = $this->ORB_Model->delete_infoIdedu($id);
		if($result == TRUE)
		{
			$this->session->set_flashdata('success','Record is deleted');
		}
		else
		{
			$this->session->set_flashdata('error','Record is not deleted yet!!..');
		}	
		redirect(base_url('ORB/edu_show/'.$this->session->userdata('master_id')));
	}
	
	public function edu_edit($id)
	{
		$this->load->model("ORB_Model");
		$data['all_education'] = $this->ORB_Model->eduedit($id);
		$this->load->view('Admin_Panel/Edit_education',$data);
	}
	
	public function edu_editproces()
	{	 
		$data = array(
			'edu_id' => $this->input->post('edu_id'),
			'edu_name' => $this->input->post('edu_name'),
			'edu_inst' => $this->input->post('edu_inst'),
			'edu_strdate' => $this->input->post('edu_strdate'),
			'edu_enddate' => $this->input->post('edu_enddate'),
			'edu_description' => $this->input->post('edu_description'),	
				);
		$this->load->model("ORB_Model");
		$result = $this->ORB_Model->edu_editprocess($data);
		if($result)
		{
			$this->session->set_flashdata('success','record is updated');
		}
		else
		{
			$this->session->set_flashdata('error','fail to update');		
		}
		redirect(base_url('ORB/edu_show/'.$this->session->userdata('master_id')));
		//$data = $this->input->post();
		//$this->ORB_Model->update_data($data);
	}
		//working lanugage ........
	public function lang_deleteId($id)
	{
		$this->load->model('ORB_Model');
		$result = $this->ORB_Model->delete_infoIdlang($id);
		if($result == TRUE)
		{
			$this->session->set_flashdata('success','Record is deleted');
		}
		else
		{
			$this->session->set_flashdata('error','Record is not deleted yet!!..');
		}	
		redirect(base_url('ORB/language_show/'.$this->session->userdata('master_id')));
	}
	
	public function language_show($id) //lanugage show all update/del/done
	{
		$this->load->model('ORB_Model');
		$data['language'] = $this->ORB_Model->get_infolang($id);
		$this->load->view('Admin_Panel/all_language',$data);
	}

	public function lang_showId($id) //language show all update/del/done with master
	{
		$this->load->model('ORB_Model');
		$data['language'] = $this->ORB_Model->get_infoIdlang($id);
		$this->load->view('Admin_Panel/profile',$data);

	}

	//////end lanugae update del done.......
	public function work_show($id)//workspace
	{
	
		$this->load->model('ORB_Model');
		$data['workspace'] = $this->ORB_Model->get_infowork($id);

		$this->load->view('Admin_Panel/all_workspace',$data);
	}

	public function work_showId($id)
	{
		$this->load->model('ORB_Model');
		$data['workspace'] = $this->ORB_Model->get_infoIdwork($id);
		$this->load->view('Admin_Panel/workspace',$data);
	}
		public function work_deleteId($id)
	{
		$this->load->model('ORB_Model');
		$result = $this->ORB_Model->delete_infoIdwork($id);
		if($result == TRUE)
		{
			$this->session->set_flashdata('success','Record is deleted');
		}
		else
		{
			$this->session->set_flashdata('error','Record is not deleted yet!!..');
		}	
		redirect(base_url('ORB/work_show/'.$this->session->userdata('master_id')));
	}
////here skill

	public function skill_deleteId($id)
	{	
		$this->load->model('ORB_Model');
		$result = $this->ORB_Model->delete_infoIdskill($id);
		if($result == TRUE)
		{
			$this->session->set_flashdata('success','Record is deleted');
		}
		else
		{
			$this->session->set_flashdata('error','Record is not deleted yet!!..');
		}	
		redirect(base_url('ORB/skill_show/'.$this->session->userdata('master_id')));
	}
	
	
	public function skill_show($id)
	{
	
		$this->load->model('ORB_Model');
		$data['skill'] = $this->ORB_Model->get_infoskill($id);

		$this->load->view('Admin_Panel/allskill_show',$data);
	}

	public function skill_showId($id)
	{
		$this->load->model('ORB_Model');
		$data['skill'] = $this->ORB_Model->get_infoIdwork($id);
		$this->load->view('Admin_Panel/skill_Profile',$data);
	}
	//////skill end
	//hobbies
		public function hobbies_show($id)
	{
	

		$this->load->model('ORB_Model');
		$data['hobbies'] = $this->ORB_Model->get_infohobbies($id);

		$this->load->view('Admin_Panel/all_hobbies',$data);
	}
	public function hobbies_showId($id)
	{
		$this->load->model('ORB_Model');
		$data['hobbies'] = $this->ORB_Model->get_infoIdhobbies($id);
		$this->load->view('Admin_Panel/hobbies',$data);

	}
	public function hobbies_deleteId($id)
	{
		$this->load->model('ORB_Model');
		$result = $this->ORB_Model->delete_infoIdhobbies($id);
		if($result == TRUE)
		{
			$this->session->set_flashdata('success','Record is deleted');
		}
		else
		{
			$this->session->set_flashdata('error','Record is not deleted yet!!..');
		}	
		redirect(base_url('ORB/hobbies_show/'.$this->session->userdata('master_id')));
	}


	//certificaiton lanugage ........
	public function certification_deleteId($id)
	{	
		$this->load->model('ORB_Model');
		$result = $this->ORB_Model->delete_infoIdcertification($id);
		if($result == TRUE)
		{
			$this->session->set_flashdata('success','Record is deleted');
		}
		else
		{
			$this->session->set_flashdata('error','Record is not deleted yet!!..');
		}	
		redirect(base_url('ORB/certification_show/'.$this->session->userdata('master_id')));
	}	
	public function certification_show($id) //certification show all update/del/done
	{
	
		$this->load->model('ORB_Model');
		$data['certification'] = $this->ORB_Model->get_infocertification($id);

		$this->load->view('Admin_Panel/all_certification',$data);
	}
public function certification_showId($id) //certificatin show all update/del/done with master
	{
		$this->load->model('ORB_Model');
		$data['certification'] = $this->ORB_Model->get_infoIdcertification($id);
		$this->load->view('Admin_Panel/profile',$data);

	}

	//////end lanugae update del done.......

	public function PI_show($id)
	{
		

		$this->load->model('ORB_Model');
		$data['personal'] = $this->ORB_Model->get_info($id);

		$this->load->view('Admin_Panel/all_personal',$data);
	}

	public function PI_showId($id)
	{
		$this->load->model('ORB_Model');

		$data['personal'] = $this->ORB_Model->get_infoId($id);
		$this->load->view('Admin_Panel/profile',$data);

	}
	public function PI_deleteId($id)
	{
		$this->load->model('ORB_Model');
		$result = $this->ORB_Model->delete_infoId($id);
		if($result == TRUE)
		{
			$this->session->set_flashdata('success','Record is deleted');
		}
		else
		{
			$this->session->set_flashdata('error','Record is not deleted yet!!..');
		}	
		redirect(base_url('ORB/PI_show/'.$this->session->userdata('reg_id')));
	}
	public function PI_doneId($id)
	{
		redirect('ORB/education');
	}

	public function PI_edit($id)
	{
		$this->load->model("ORB_Model");
		$data['all_personals'] = $this->ORB_Model->edit($id);
		$this->load->view('Admin_Panel/Edit_personal_info',$data);
	}

	public function PI_editprocess()
	{
		$this->load->model("ORB_Model");
		
		$data = $this->input->post();
		
		$result = $this->ORB_Model->PI_editprocess($data);

		if($result)
		{
			$this->session->set_flashdata('success','record is updated');
		}
		else
		{
			$this->session->set_flashdata('error','fail to update');		
		}
		redirect(base_url('ORB/education'));
		
		//$data = $this->input->post();
		//$this->ORB_Model->update_data($data);
	}

	public function education()
	{
		$this->load->view('Admin_Panel/Education');
	}
	public function edu_insert()
	{

	$education = $this->input->post('education[]');
	$institute_name = $this->input->post('institute_name[]');	
	$start_date = $this->input->post('start_date[]');	
	$end_date = $this->input->post('end_date[]');
	$description = $this->input->post('description[]');	

	$count = count($education);
	for ($i=0; $i<$count; $i++)
		{ 
			$data = array(
			  'master_id' => $this->session->userdata('master_id'), 
			  'edu_name' => $education[$i],
			  'edu_inst' => $institute_name[$i],
			  'edu_strdate' => $start_date[$i],
			  'edu_enddate' => $end_date[$i],
			  'edu_description' => $description[$i]
			  );
		
			$query = $this->db->insert('edu_tb',$data);
			}
	
			if($query)
			{
			$this->session->set_flashdata("success","Education is inserted..!");
			}
			else
				{
			$this->session->set_flashdata("error","Education is not inserted..!");
				}

				redirect('ORB/workspace');
		}

	public function workspace()
	{
		$this->load->view('Admin_Panel/workspace');
	
	}
	public function work_insert()
	{

	$designation = $this->input->post('designation');
	$company_name = $this->input->post('company_name');
	$start_date = $this->input->post('start_date');	
	$end_date = $this->input->post('end_date');	
	$description = $this->input->post('description');	
	
	$count = count($designation);
	for ($i=0; $i<$count; $i++)
		{ 
			$data = array(

			  'master_id' => $this->session->userdata('master_id'),
			  'exp_designation' => $designation[$i],
			  'exp_company' => $company_name[$i],
			  'exp_strdate' => $start_date[$i],
			  'exp_enddate' => $end_date[$i],
			  'exp_description' => $description[$i]
			  );
		
			$query = $this->db->insert('exp_tb',$data);
			if($query)
			{
			$this->session->set_flashdata("success","workexperience is inserted..!");
			}
			else
				{
			$this->session->set_flashdata("error","workexperience is not inserted..!");
				}
		}

		redirect('ORB/Certification');
	}

	//////end lanugae update del done.......

	////// editing education process end

		//>>>>>>>>>>> edit workexerince start here..............
	public function work_edit($id)
	{
		$this->load->model("ORB_Model");
		$data['all_workspace'] = $this->ORB_Model->workedit($id);
		$this->load->view('Admin_Panel/Edit_workspace',$data);
	}
	public function work_editproces()
	{
		 
		$data = array(
				'exp_id' => $this->input->post('exp_id'),
				'exp_designation' => $this->input->post('exp_designation'),
				'exp_company' => $this->input->post('exp_company'),
				'exp_strdate' => $this->input->post('exp_strdate'),
				'exp_enddate' => $this->input->post('exp_enddate'),
				'exp_description' => $this->input->post('exp_description'),
				
				);
		$this->load->model("ORB_Model");
		$result = $this->ORB_Model->work_editprocess($data);
		if($result)
		{
			$this->session->set_flashdata('success','record is updated');
		}
		else
		{
			$this->session->set_flashdata('error','fail to update');		
		}
		redirect(base_url('ORB/work_show/'.$this->session->userdata('master_id')));
	
	}

	////// Edit reference start here >>>>>>>>>>>>>>>>>>>
	public function reference_deleteId($id)
	{
		$this->load->model('ORB_Model');
		$result = $this->ORB_Model->delete_infoIdreference($id);
		if($result == TRUE)
		{
			$this->session->set_flashdata('success','Record is deleted');
		}
		else
		{
			$this->session->set_flashdata('error','Record is not deleted yet!!..');
		}	
		redirect(base_url('ORB/reference_show/'.$this->session->userdata('master_id')));
	}

	public function reference_edit($id)
	{
		$this->load->model("ORB_Model");
		$data['all_reference'] = $this->ORB_Model->referenceedit($id);
		$this->load->view('Admin_Panel/Edit_reference',$data);
	}
	public function reference_editproces()
	{
		$data = array(
				'ref_id' => $this->input->post('ref_id'),
				'ref_name' => $this->input->post('ref_name'),
				'ref_phone' => $this->input->post('ref_phone'),
				'ref_email' => $this->input->post('ref_email'),
				'ref_desg' => $this->input->post('ref_desg'),
				'ref_instit' => $this->input->post('ref_instit'),		
				);
		$this->load->model("ORB_Model");
		$result = $this->ORB_Model->reference_editprocess($data);
		if($result)
		{
			$this->session->set_flashdata('success','record is updated');
		}
		else
		{
			$this->session->set_flashdata('error','fail to update');		
		}
		redirect(base_url('ORB/reference_show/'.$this->session->userdata('master_id')));
	}

	///////Edit reference end there >>>>>>>>>>>>>>>>>>>>
		//<<<<<<<<<<<<<.. edit workexperice end here...........
		//working lanugage ........
	
//------certification edit Model Start here---------------------------------------------------------------------------

public function certification_edit($id)
	{
		$this->load->model("ORB_Model");
		$data['all_certification'] = $this->ORB_Model->certificationedit($id);
		$this->load->view('Admin_Panel/Edit_certification',$data);
	}
	public function certification_editproces()
	{
		$data = array(
				'certi_id' => $this->input->post('certi_id'),
				'certi_name' => $this->input->post('certi_name'),
				'certi_insti' => $this->input->post('certi_insti'),
				'certi_strdate' => $this->input->post('certi_strdate'),
				'certi_enddate' => $this->input->post('certi_enddate'),
				'certi_description' => $this->input->post('certi_description'),		
				);
		$this->load->model("ORB_Model");
		$result = $this->ORB_Model->certification_editprocess($data);
		if($result)
		{
			$this->session->set_flashdata('success','record is updated');
		}
		else
		{
			$this->session->set_flashdata('error','fail to update');		
		}
		redirect(base_url('ORB/certification_show/'.$this->session->userdata('master_id')));
	}

//------certification edit Model End here-------------------------------------------------

	
	public function Skill_Profile()
	{
		$this->load->view('Admin_Panel/Skill_Profile');
	
	}
	public function skill_insert()
	{
	$skills_name = $this->input->post('skills_name[]');
	$skill_percent = $this->input->post('skill_percent[]');	
	
	$count = count($skills_name);
	for ($i=0; $i<$count; $i++)
		{ 
			$data = array(
				'master_id' => $this->session->userdata('master_id'),
			  
			  
			  'skill_name' => $skills_name[$i],
			  'skill_percentage' => $skill_percent[$i],
			
			  );
		
			$query = $this->db->insert('skills_tb',$data);
			if($query)
			{
			$this->session->set_flashdata("success","reference is inserted..!");
			}
			else
				{
			$this->session->set_flashdata("error","reference is not inserted..!");
				}
		}

		redirect('ORB/Languages');
	}
	public function Hobbies()
	{
		$this->load->view('Admin_Panel/Hobbies');
	}

	public function hobbies_insert()
	{
	$hobbies = $this->input->post('hobbies[]');
		
	$count = count($hobbies);
	for ($i=0; $i<$count; $i++)
		{ 
			$data = array(
				'master_id' => $this->session->userdata('master_id'),
			  'hobby_id' => $this->session->userdata('user_id'), 
			  
			  'hobby_names' => $hobbies[$i]
			
			  );
		
			$query = $this->db->insert('hobby_tb',$data);
			if($query)
			{
			$this->session->set_flashdata("success","Hobbies is inserted..!");
			}
			else
				{
			$this->session->set_flashdata("error","hobbies is not inserted..!");
				}
		}
			redirect('ORB/Reference');
	}

	public function Languages()
	{
		$this->load->view('Admin_Panel/Language');
	
	}
	public function lang_insert()
	{
	$lang_name = $this->input->post('language_name');
	$lang_percentage = $this->input->post('language_percentage');			
	
	$count = count($lang_name);
	for ($i=0; $i<$count; $i++)
		{ 
			$data = array(
			  'master_id' => $this->session->userdata('master_id'),
			  
			  'lang_name' => $lang_name[$i],
			  'lang_percentage' => $lang_percentage[$i],
			
			  );
		
			$query = $this->db->insert('lang_tb',$data);
			if($query)
			{
			$this->session->set_flashdata("success","languages is inserted..!");
			}
			else
				{
			$this->session->set_flashdata("error","languages is not inserted..!");
				}
		}
			redirect('ORB/hobbies');
	}
	public function Reference()
	{
		$this->load->view('Admin_Panel/Reference');
	
	}
	public function ref_insert()
	{
		redirect('ORB/Reference');
	}
	public function certification()
	{
		$this->load->view('Admin_Panel/certification');
	}
	public function certification_insert()
	{
	
	$certificate_name = $this->input->post('certification_name');
	$institute_name = $this->input->post('institute_name');	
	$start_date = $this->input->post('start_date');	
	$end_date = $this->input->post('end_date');
	$description = $this->input->post('description');		
	
	$count = count($certificate_name);
	for ($i=0; $i<$count; $i++)
		{ 
			$data = array(
			  'master_id' => $this->session->userdata('master_id'),
			  'certi_name' => $certificate_name[$i],
			  'certi_insti' => $institute_name[$i],
			  'certi_strdate' => $start_date[$i],
			  'certi_enddate' => $end_date[$i],
			  'certi_description' => $description[$i]
			  );
			$query = $this->db->insert('certi_tb',$data);
			if($query)
			{
			$this->session->set_flashdata("success","certification is inserted..!");
			}
			else
				{
			$this->session->set_flashdata("error","certification is not inserted..!");
				}
		}

		redirect('ORB/skill_Profile');
	}
	//resume
	public function adminapproved($id)
	{
		$approved ='1';
		$data = array(
       
        'aprove'  => $approved
        );
			$this->load->database('orb');
			$this->db->where('master_id',$id);
			$this->db->update('master_tb',$data);
			redirect(base_url().'ORB/index');

	}
		public function admindenied($id)
	{
		$approved ='2';
		$data = array(
       
        'aprove'  => $approved
        );
			$this->load->database('orb');
			$this->db->where('master_id',$id);
			$this->db->update('master_tb',$data);
			redirect(base_url().'ORB/index');

	}

	public function view_resume(){

		if($this->session->has_userdata('logged'))
		{
			$this->load->model('ORB_Model');
				$data['query'] = $this->ORB_Model->get_infodash1();
			
				$data['query2'] = $this->ORB_Model->get_expdash1();
			
				$data['query3'] = $this->ORB_Model->get_edudash1();
			
				$data['query4'] = $this->ORB_Model->get_certidash1();

				$data['query5'] = $this->ORB_Model->get_langdash1();

				$data['query6'] = $this->ORB_Model->get_refdash1();
				$data['query7'] = $this->ORB_Model->get_skilldash1();

				$data['query8'] = $this->ORB_Model->get_hobbydash1();
		
			$this->load->view('Admin_Panel/View_resume_bckp',$data);
		}
	}


	public function adminview_resume(){
	
		if($this->session->userdata('logged'))
		{
			$this->load->model('ORB_Model');
				
				$data['query'] = $this->ORB_Model->get_infodash3();
			
				$data['query2'] = $this->ORB_Model->get_expdash3();
			
				$data['query3'] = $this->ORB_Model->get_edudash3();
			
				$data['query4'] = $this->ORB_Model->get_certidash3();

				$data['query5'] = $this->ORB_Model->get_langdash3();

				$data['query6'] = $this->ORB_Model->get_refdash3();
				
				$data['query7'] = $this->ORB_Model->get_skilldash3();

				$data['query8'] = $this->ORB_Model->get_hobbydash3();
		
			$this->load->view('Admin_Panel/adminView_resume',$data);
		}

	}

	public function referenceinsert($id)
	{
	$ref_name = $this->input->post('name');
	$ref_phone = $this->input->post('phone');	
	$ref_email = $this->input->post('email');	
	$designation = $this->input->post('designation');
	$ref_instit = $this->input->post('institute');	
	if(isset($ref_name)){	
	$count = count($ref_name);
	
	for ($i=0; $i<$count; $i++)
		{ 
			$data = array(
			 'master_id' => $id,
			  'ref_name' => $ref_name[$i],
			  'ref_phone' => $ref_phone[$i],
			  'ref_email' => $ref_email[$i],
			  'ref_desg' => $designation[$i],
			  'ref_instit' => $ref_instit[$i]
			  );
			$query = $this->db->insert('ref_tb',$data);
			if($query)
			{
			$this->session->set_flashdata("success","reference is inserted..!");
			}
			else
				{
			$this->session->set_flashdata("error","reference is not inserted..!");
				}
		}
	}
		
		$this->load->model('ORB_Model');
		$data['adminapproved'] = $this->ORB_Model->checkapproved($id);
		$this->load->view('Admin_Panel/approved',$data);
	
	}

	public function approve($id)
	{
		$this->load->model('ORB_Model');
		$data['adminapproved'] = $this->ORB_Model->checkapproved($id);
	
		$this->load->view('Admin_Panel/approved',$data);
	}
	 
	public function ajax()
	{
		$this->load->view('Admin_Panel/ajax');
	
	}
	public function ajax1()
	{
		$this->load->view('Admin_Panel/ajax1');
	
	}
public function ajax3()
	{
		$this->load->view('Admin_Panel/ajax3');
	
	}
	
}