-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Nov 06, 2018 at 01:19 AM
-- Server version: 5.6.39-cll-lve
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `myuni_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `certi_tb`
--

CREATE TABLE `certi_tb` (
  `certi_id` int(11) NOT NULL,
  `master_id` int(11) DEFAULT NULL,
  `certi_name` varchar(200) DEFAULT NULL,
  `certi_insti` varchar(200) DEFAULT NULL,
  `certi_strdate` varchar(20) DEFAULT NULL,
  `certi_enddate` varchar(50) DEFAULT NULL,
  `certi_description` varchar(500) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `certi_tb`
--

INSERT INTO `certi_tb` (`certi_id`, `master_id`, `certi_name`, `certi_insti`, `certi_strdate`, `certi_enddate`, `certi_description`) VALUES
(8, 68, 'sda', 'x', '0000-00-00', '0000-00-00', 'x'),
(9, 70, 'Android Application Developer', 'Iqra University', '2017-10-31', '01-03-2017', 'Teach Basic knowledge in android'),
(10, 70, 'C++ and Microsoft office', 'Computer Collegiate', '2018-12-31', '01-03-2012', 'asdasda'),
(11, 84, 'Internship', 'Iqra university islamabad campus', '18-05-2015', '05-09-2016', 'on database'),
(12, NULL, 'Fixed Income Trading and Investments', 'Institute of Financial Markets of Pakistan', '15-03-2017', '18-03-2017', 'Certificate of Institute of Financial Markets of Pakistan in the subject of (Fixed Income Trading and Investments)'),
(13, 92, 'rqer', 'reer', '04-06-2009', '04-06-2010', 'ertwete'),
(14, NULL, 'Fixed Income Trading and Investments', 'Institute of Financial Markets of Pakistan', '15-03-2017', '18-03-2017', 'Certificate of Institute of Financial Markets of Pakistan in the subject of (Fixed Income Trading and Investments)'),
(15, 93, 'Fixed Income Trading and Investments', 'Institute of Financial Markets of Pakistan', '15-03-2017', '18-03-2017', 'Certificate of Institute of Financial Markets of Pakistan in the subject of (Fixed Income Trading and Investments)'),
(16, 95, 'Computer Hardware Configuration', 'Kips', '01-02-2013', '01-05-2013', 'BIOS Configuration'),
(17, 95, 'Web Designing', 'Kips', '01-02-2013', '01-05-2013', 'Website Designing'),
(18, NULL, 'Tectiqs ', 'Iqra university Islamabad', '05-10-2015', '05-10-2018', 'i had participated in university event name tectiqs'),
(19, 98, 'Tectiqs ', 'Iqra university Islamabad', '05-10-2015', '05-10-2017', 'i was in team of iqra university yearly event name Tectiqs as a Security');

-- --------------------------------------------------------

--
-- Table structure for table `dapartment`
--

CREATE TABLE `dapartment` (
  `dapt_id` int(100) NOT NULL,
  `dapt_name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dapartment`
--

INSERT INTO `dapartment` (`dapt_id`, `dapt_name`) VALUES
(1, 'COMPUTER & TECHNOLOGY'),
(2, 'FASHION & DESIGN'),
(3, 'MEDIA STUDIES'),
(4, 'BUSINESS ADMINISTRATION'),
(5, 'SOCIAL SCIENCES');

-- --------------------------------------------------------

--
-- Table structure for table `edu_tb`
--

CREATE TABLE `edu_tb` (
  `edu_id` int(11) NOT NULL,
  `master_id` int(11) DEFAULT NULL,
  `edu_name` varchar(200) NOT NULL,
  `edu_inst` varchar(200) NOT NULL,
  `edu_strdate` varchar(50) NOT NULL,
  `edu_enddate` varchar(50) NOT NULL,
  `edu_description` varchar(500) NOT NULL,
  `edu_cgpa` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `edu_tb`
--

INSERT INTO `edu_tb` (`edu_id`, `master_id`, `edu_name`, `edu_inst`, `edu_strdate`, `edu_enddate`, `edu_description`, `edu_cgpa`) VALUES
(30, 68, 'Master in computer Science', 'dasd', '2017-12-31', '2018-12-31', 'good', 0),
(31, 68, 'Bachelor in computer science', 'dasd', '2017-12-30', '2018-01-01', 'good', 0),
(36, 73, 'asda', 'da', '2017-12-31', '2018-07-31', 'asdas', 0),
(50, 70, 'Bachelor in (CS)', 'Iqra University', '01-01-2015', 'In Progress', 'Php Developer ', 134),
(51, 70, 'DAE in (CIT)', 'Aligarh Institute Of Technology', '01-01-2012', '01-01-2014', 'Basic knowledge of computer architecture and programming. ', 0),
(55, 70, 'Intermediate', 'Comprehensive Boys College', '01-01-2010', '01-01-2011', 'Basic information', 65468),
(56, 70, 'a123', 'das', '03-03-2017', '12-03-2013', 'sdas', 165),
(57, 70, 'a1', 'sda', '03-03-2017', '01-03-2017', 'sdas', 134),
(58, 78, 'BSCS', 'Iqra University', '01-09-2012', '01-01-2016', 'Computer Science', 0),
(59, 78, 'FSC', 'IMCB F84 Islamabad', '01-09-2010', '01-09-2012', 'Science', 0),
(60, 79, 'BSCS', 'IUIC', '02-02-2013', '02-01-2017', 'Computer Science', 0),
(62, 81, 'BS(CS)', 'Iqra University, Islamabad.', '16-01-2015', 'In Progress', 'Java development, web development, C++, C, Android development', 0),
(63, 80, 'BSCS', 'Iqra Univeristy Islamabad Campus', '01/04/2014', '01/06/2018', 'Computer Science', 0),
(64, 82, 'BSCS', 'Iqra University Islamabad Campus', '20-01-2014', '01-08-2018', 'Web development, App development', 0),
(65, 83, 'MBA', 'IQRA University', '01-09-2017', 'In Progress', 'Supply chain', 0),
(66, 84, 'BS in computer science', 'Iqra university islamabad campus', '21-01-2013', '16-08-2016', 'Web designing', 0),
(67, 86, 'Bachelors', 'Iqra University Islamabad ', '16-09-2016', 'In Progress', 'Project Management ', 0),
(68, 86, 'Bachelors', 'Iqra University Islamabad ', '16-09-2016', '', 'Project Management ', 0),
(69, NULL, 'Bs(hons) in computer science ', 'Iqra university ', '04-02-2013', '10-12-2017', 'Computer science ', 0),
(70, 88, 'Matric', 'Rise and shine public school Wah Cantt', '10-04-2009', '11-04-2010', 'Science', 0),
(71, 88, 'Fsc', 'FG Science Degree College For Men Wah Cantt', '11-09-2010', '14-06-2012', 'Pre Engineering', 0),
(72, 88, 'BSCS', 'Iqra University', '03-09-2012', '31-05-2016', 'Computer Science', 0),
(73, 88, 'MSCS', 'University of Engineering and Technology, Taxila', '25-10-2017', '', 'Computer Science', 0),
(74, 89, 'Masters in Business Administration', 'Iqra University (Islamabad)', '07-09-2014', '06-08-2018', 'Marketing', 0),
(75, 90, 'MBA Finance', 'Iqra University Islamabad', '28-02-2012', '30-06-2015', 'Finance', 0),
(76, 90, 'MBA Finance', 'Iqra University Islamabad', '28-02-2012', '30-06-2015', 'Finance', 0),
(77, 90, 'MBA Finance', 'Iqra University Islamabad', '28-02-2012', '30-06-2015', 'Finance', 0),
(78, 90, 'MBA Finance', 'Iqra University Islamabad', '28-02-2012', '30-06-2015', 'Finance', 0),
(79, 90, 'MBA Finance', 'Iqra University Islamabad', '28-02-2012', '30-06-2015', 'Finance', 0),
(80, 92, 'erewere', 'rwer', '04-06-2009', '04-06-2010', 'gsdfs', 0),
(81, NULL, 'Matric', 'Alsabah Sakura Academy Sonikot', '01-03-2008', '20-05-2009', 'Science', 0),
(82, NULL, 'MbA', 'Iqra University Islamabad', '21-01-2014', '31-11-2017', 'Finance', 0),
(83, 93, 'MBA', 'Iqra University Islamabad', '21-01-2014', '31-11-2017', 'Finance', 0),
(84, NULL, 'Masters in Business Administration', 'Iqra University Islamabad Campus', '16-01-2017', 'In Progress', 'Marketing', 0),
(85, NULL, 'Bachelors in Commerce', 'University of Punjab', '22-10-2014', '01-11-2016', 'Commerce', 0),
(86, NULL, 'Intermediate in Commerce', 'St. Mary\'s College, Lalazar, Rawalpindi', '01-01-2012', '01-01-2014', 'Commerce', 0),
(87, 95, 'Matric', 'Anglo Arabic Public Secondary School', '01-02-2012', '01-02-2013', 'Biology', 0),
(88, 95, 'Matric', 'Anglo Arabic Public Secondary School', '01-02-2012', '01-02-2013', 'Biology', 0),
(89, 95, 'Intermediate ', 'Punjab Group of College', '01-02-2013', '01-02-2015', 'Computer Science', 0),
(90, 96, 'Bachelors in Computer Science', 'Iqra university', '01-01-2014', '20-05-2018', 'bachelors in computer science', 0),
(91, NULL, 'BS computer science', 'Agriculture University Peshawar', '01-01-2010', '15-02-2014', 'Programming', 0),
(92, 99, 'BSCS', 'IQRA University Islamabad Campus', '01-02-2012', '08-08-2016', 'IT, Social Media Marketing ', 0),
(93, NULL, 'Batchlor of Science of Computer Science', 'Iqra University Islamabad Camous', '10-08-2013', '10-01-2018', 'Graphics and Website Designing, Internet Skills , E commerce , Microsoft office , Data entry , ', 0),
(94, NULL, 'FSC Pre-Engineering', 'Punjab College rawalpindi', '05-06-2011', '05-06-2013', 'FSCPre-Engineering', 0),
(95, 98, 'Bachelor of Science in Computer Science (BSCS)', 'Iqra university Islamabad', '05-08-2013', '05-01-2018', 'Web designing, Graphics designing, project management , Database', 0),
(96, NULL, 'Bachelor of Science in Telecommunication and Networks', 'Iqra University ,Islamabad', '03-09-2012', 'In Progress', 'Computer Science', 0),
(97, 101, 'B,E', 'HITEC UNIVERSITY TAXILA CANTT', '10-09-2009', '10-09-2013', 'MECHANICAL ENGINEERING', 0),
(98, 101, 'MBA', 'IQRA UNIVERSITY ISLAMABAD CAMPUS', '20-01-2016', '', 'MARKETING/ADVERTISEMENT', 0),
(99, 102, 'SSC', 'Federal Government Secondary School ', '06-09-2007', '05-09-2008', 'Science ', 0),
(100, 92, 'fhfh', 'tesrt', '09-08-2011', '09-08-2012', '5654y65', 0);

-- --------------------------------------------------------

--
-- Table structure for table `exp_tb`
--

CREATE TABLE `exp_tb` (
  `exp_id` int(11) NOT NULL,
  `master_id` int(11) DEFAULT NULL,
  `exp_designation` varchar(200) DEFAULT NULL,
  `exp_company` varchar(200) DEFAULT NULL,
  `exp_strdate` varchar(50) DEFAULT NULL,
  `exp_enddate` varchar(50) DEFAULT NULL,
  `exp_description` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `exp_tb`
--

INSERT INTO `exp_tb` (`exp_id`, `master_id`, `exp_designation`, `exp_company`, `exp_strdate`, `exp_enddate`, `exp_description`) VALUES
(11, 73, 'das1', 'sa', '0000-00-00', '0000-00-00', 'd'),
(12, 73, 'das12', 'sa', '0000-00-00', '0000-00-00', 'a'),
(18, 70, 'Software Developer', 'Core2Plus', '25-03-2018', 'Still Working', 'HTML,PHP,Bootstrap,JaveScript,JQuery'),
(19, 70, 'Web Developer', 'Cloud Fish', '01-09-2016', '01-01-2018', 'HTML,BootStrap,CSS'),
(20, 70, 'CSR', 'Cenit', '01-06-2015', '01-01-2017', 'Customer Sale Representative'),
(21, 78, 'Junior Developer', 'Saufik', '01-01-2018', 'Still Working', 'I am working as Full Stack Developer at Saufik. '),
(22, 78, 'E-Commerce Officer', 'IGate Technologies', '10-10-2016', '29-12-2017', 'I was E-Commerce officer at Igate technologies and was responsible for Amazon\'s SEO.'),
(23, 79, 'Web Developer Internie', 'Niku Solution Private Limited', '07-08-2017', '07-03-2018', 'Web Developer'),
(24, 79, 'Full Stack Web Developer', 'Red Star Technologies', '03-03-2018', '', 'PHP MVC Codeigniter HTML5 CSS3 Bootstrap 4 Jquery Ajax JSON'),
(26, 81, 'Php Developer ( internee)', 'Mdeux Solutions pvt. ltd.', '25-05-2018', 'Still Working', 'Doing a project for them that will be submitted as final year project for my degree.'),
(27, 80, 'Social Media Evaluator (Remote)', 'Appen,Australia', '01/07/2017', '30/05/2018', 'Reviewed and rated web content,using a web application to support the measurement of search data relevance.'),
(28, 83, 'Operations Associate', 'SPS', '01-01-2018', 'Still Working', 'Management of Resources.Oversight of inventory, purchasing and supplies.Human resources tasks include determining needs, hiring employees and staff de'),
(29, NULL, 'Resident Engineer IT NOC', 'PTCL', '06-10-2016', '30-04-2018', 'Monitor and manage the IT network infrastructure of ptcl contact centers using multiple tools. CA Spectrum,ehealth, APM'),
(30, NULL, 'Resident Engineer CA Support', 'PTCL', '01-05-2018', 'Still Working', 'Work as developer and administrator on the tools of network health and monitoring, reporting of An USA BASED company troubleshoot and resolve issues.'),
(31, NULL, 'Internee', 'Islamabad Marriot Hotel', '10-08-2017', '09-09-2017', 'just to gain experience '),
(32, 86, 'Promoter', 'Radio Mirchi', '09-07-2014', '20-08-2014', 'Working as a movie promoter in Dubai '),
(33, 88, 'Visiting Lab Engineer', 'Iqra University Islamabad Campus', '09-01-2017', 'Still Working', 'My Job responsibilites as a visiting lab engineer are to conduct different subjects labs and teach students by perfoming practicals.'),
(34, 89, 'Marketing Associate', 'Wahdat Integrated Farms', '03-07-2016', '30-06-2018', 'Planning & Implementing promotional campaigns\r\n,Managing lead generating campaigns and measuring results,Preparing and developing online and print advr'),
(35, 90, 'Accountant', 'AIRCRAFT  MANUFACTURING  FACTORY (Kamra)', '25-04-2016', 'Still Working', 'Skilled Gained:\r\n•Team oriented with high attention to detail\r\n•Ability to deal effectively with month end closing deadlines\r\n•Exceptional organization'),
(36, NULL, 'Accounts officer ', 'GilgitBazar.com', '01-03-2018', '30-08-2018', 'Working as an accounts officer in Gilgitbazar.com'),
(37, 92, 'rewr', 'rwerew', '04-06-2009', '04-06-2010', 'asfeqrqe'),
(38, NULL, 'Accounts officer ', 'GilgitBazar.com', '01-03-2018', 'Still Working', 'Working in Gilgitbazar.com as accounts officer'),
(39, 93, 'Accounts officer ', 'GilgitBazar.com', '15-03-2017', 'Still Working', 'Working in Gilgitbazar.com as accounts officer '),
(40, NULL, 'Corporate Sales Executive', 'Al-Bari Group of Companies', '09-06-2018', 'Still Working', 'On field and office sales and grabbing investors to the company.'),
(41, NULL, 'Paid Internee at Admission Office', 'Iqra University Islamabad Campus', '16-11-2017', '10-06-2018', 'Front desk operations, hosting of university\'s chat portal on website, marketing the features, book keeping, closing balance sheets & forms dealing'),
(42, 97, 'IT teacher', 'Beaconhous school system', '10-03-2016', '08-06-2017', 'Woked as an IT teacher to share the knowledge which i learned.'),
(43, 97, 'IT teacher', 'The Educators', '10-03-2014', '15-06-2016', 'Worked as an IT teacher.'),
(44, 97, 'IT teacher', 'Saint paul education system', '08-03-2013', '10-03-2014', 'Worked as an IT teacher.'),
(45, 98, 'Accountant', 'Private clinic', '05-08-2015', '05-10-2017', 'i worked as a accountant at private clinic. '),
(46, 98, 'Car dealing', 'japan autos', '05-04-2013', '05-06-2015', 'i worked as a dealer in car showroom');

-- --------------------------------------------------------

--
-- Table structure for table `hobby_tb`
--

CREATE TABLE `hobby_tb` (
  `hobby_id` int(11) NOT NULL,
  `master_id` int(11) DEFAULT NULL,
  `hobby_names` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `hobby_tb`
--

INSERT INTO `hobby_tb` (`hobby_id`, `master_id`, `hobby_names`) VALUES
(1, 59, 'Cricket'),
(2, 59, 'Outing'),
(3, 60, 'Cricket'),
(4, 60, 'Outing'),
(19, 70, 'Cricket'),
(20, 70, 'Football'),
(21, 70, 'Cricket'),
(22, 70, 'kacahi'),
(23, 70, ''),
(24, 70, 'cricket'),
(25, 70, 'badminton'),
(26, 78, 'Dota2'),
(27, 78, 'Table Tennis'),
(28, 78, 'Hiking'),
(29, 78, 'Movies'),
(30, 78, 'Cricket'),
(31, 82, 'Video Game'),
(32, 82, 'Football'),
(33, 79, 'Internet Surfing & Online Games'),
(34, 81, 'Football'),
(35, 81, 'Badminton'),
(36, 81, 'Photography'),
(37, 81, 'Travelling or exploring places'),
(38, 81, 'Reading'),
(39, 81, ''),
(40, 83, 'Football'),
(41, 83, 'Traveling'),
(42, 83, 'Football'),
(43, 83, 'Traveling'),
(44, NULL, 'Reading, voluntering'),
(45, 86, 'YouTube '),
(46, NULL, 'Traveling movies gamings'),
(47, NULL, 'Gaming'),
(48, NULL, 'Gaming'),
(49, 88, 'Novel reading, web surfing, listening music.'),
(50, NULL, 'My Hobbies are to watch news and to play cricket and football'),
(51, 92, 'erqere'),
(52, NULL, 'My Hobbies are to watch news and to play cricket and football'),
(53, NULL, 'My Hobbies are to watch news and to play cricket and football'),
(54, 95, 'Cricket'),
(55, 95, 'Hiking'),
(56, 96, ''),
(57, NULL, 'Plants'),
(58, NULL, 'Love to learn new technology'),
(59, NULL, 'Graphics designing'),
(60, NULL, 'Photography'),
(61, 98, 'interest in new technology'),
(62, 98, 'Graphics designing'),
(63, 98, 'Photography'),
(64, 98, '');

-- --------------------------------------------------------

--
-- Table structure for table `lang_tb`
--

CREATE TABLE `lang_tb` (
  `lang_id` int(11) NOT NULL,
  `lang_name` varchar(200) DEFAULT NULL,
  `lang_percentage` varchar(200) DEFAULT NULL,
  `master_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lang_tb`
--

INSERT INTO `lang_tb` (`lang_id`, `lang_name`, `lang_percentage`, `master_id`) VALUES
(112, 'English', '70', 70),
(113, 'Urdu', '60', 70),
(114, 'English', '100', 78),
(115, 'Urdu', '100', 78),
(116, '', '50', 78),
(117, 'C++', '90', 82),
(118, 'Java', '88', 82),
(119, 'Javascript', '92', 82),
(120, 'JQuery', '85', 82),
(121, '', '50', 82),
(122, 'English', '50', 79),
(123, 'Urdu', '50', 79),
(124, 'Pashtu', '50', 79),
(125, 'Punjabi', '50', 79),
(126, 'English', '90', 81),
(127, 'Urdu', '100', 81),
(128, '', '50', 81),
(129, 'English', '100', 80),
(130, 'Urdu', '100', 80),
(131, 'Pashto', '94', 80),
(132, 'Punjabi', '50', 80),
(133, 'English', '84', 83),
(134, 'Urdu', '91', 83),
(135, '', '50', 83),
(136, 'Urdu', '100', NULL),
(137, 'English ', '82', NULL),
(138, '', '50', NULL),
(139, '', '50', 84),
(140, '', '50', 84),
(141, '', '50', 84),
(142, 'Pashto', '100', 86),
(143, 'Urdu', '100', 86),
(144, 'English ', '75', 86),
(145, 'English', '74', NULL),
(146, 'Urdu ', '100', NULL),
(147, 'Panjabi', '39', NULL),
(148, 'Urdu', '100', 88),
(149, 'Punjabi', '100', 88),
(150, 'English', '90', 88),
(151, 'English', '89', NULL),
(152, 'Urdu', '99', NULL),
(153, 'Brushaski and Shina', '100', NULL),
(154, 'ewqrf', '50', 92),
(155, 'dsfds', '50', 92),
(156, 'fsdgds', '50', 92),
(157, 'English', '80', NULL),
(158, 'Urdu', '100', NULL),
(159, 'Shina and Brushaski', '100', NULL),
(160, 'English', '50', NULL),
(161, '', '50', NULL),
(162, '', '50', NULL),
(163, 'English', '50', 95),
(164, 'Pashto', '50', 95),
(165, 'Urdu', '50', 95),
(166, 'Urdu', '100', 96),
(167, 'English', '100', 96),
(168, '', '50', 96),
(169, 'Englist', '90', NULL),
(170, 'Urdu', '100', NULL),
(171, 'Punjabi', '50', NULL),
(172, 'English', '86', 98),
(173, 'Urdu', '100', 98),
(174, 'Punjabi', '50', 98);

-- --------------------------------------------------------

--
-- Table structure for table `level_table`
--

CREATE TABLE `level_table` (
  `level_id` int(11) NOT NULL,
  `level_name` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `level_table`
--

INSERT INTO `level_table` (`level_id`, `level_name`) VALUES
(1, 'Expert'),
(2, 'Entry level'),
(3, 'Trainee');

-- --------------------------------------------------------

--
-- Table structure for table `master_tb`
--

CREATE TABLE `master_tb` (
  `master_id` int(11) NOT NULL,
  `reg_id` int(11) DEFAULT NULL,
  `title_id` int(11) DEFAULT NULL,
  `ms_image` blob,
  `ms_profile` varchar(500) DEFAULT NULL,
  `ms_fname` varchar(100) DEFAULT NULL,
  `ms_lname` varchar(100) DEFAULT NULL,
  `ms_email` varchar(100) DEFAULT NULL,
  `ms_contact` varchar(100) DEFAULT NULL,
  `ms_address` varchar(200) DEFAULT NULL,
  `ms_gender` varchar(50) DEFAULT NULL,
  `ms_dob` varchar(50) DEFAULT NULL,
  `ms_postal` int(10) NOT NULL,
  `ms_city` varchar(100) NOT NULL,
  `ms_discription` varchar(500) NOT NULL,
  `aprove` int(2) NOT NULL,
  `ms_rollno` int(100) NOT NULL,
  `ms_mname` varchar(100) NOT NULL,
  `ms_social` varchar(100) NOT NULL,
  `ms_expertise` varchar(50) NOT NULL,
  `ms_department` varchar(50) NOT NULL,
  `ms_program` varchar(100) NOT NULL,
  `Specialization` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `master_tb`
--

INSERT INTO `master_tb` (`master_id`, `reg_id`, `title_id`, `ms_image`, `ms_profile`, `ms_fname`, `ms_lname`, `ms_email`, `ms_contact`, `ms_address`, `ms_gender`, `ms_dob`, `ms_postal`, `ms_city`, `ms_discription`, `aprove`, `ms_rollno`, `ms_mname`, `ms_social`, `ms_expertise`, `ms_department`, `ms_program`, `Specialization`) VALUES
(57, 13, 1, 0x3135323835333636383930352e2d50554d412d4c6f676f2e6a7067, 'lorem ipsum', 'ali', 'syed', 'syed@gmail.com', '03003053530', 'karachi', 'male', '2018-04-02', 0, '', '', 1, 0, '', '', '', '', '', ''),
(62, 12, 1, 0x31353238363538373637, 'asdas', 'dasd', 'dasda', 'dasad@gmail.com', '03122912921', 'dasd', 'male', '2017-10-30', 0, '', '', 1, 0, '', '', '', '', '', ''),
(70, 15, NULL, 0x3135333337323539333631333731315f3931343333353039383632393530355f393231333735333737353330353531383437305f6e2e6a7067, NULL, 'ali', 'Ali', 'test+1@gmail.com', '03007070103', 'abc123', NULL, '24-02-1995', 0, '                              ', 'Make my own software house and setup institute for those who can not be able to study modern education for the future of Pakistan and make Pakistan proud.\r\n', 1, 1, '', '', '', 'BUSINESS ADMINISTRATION', '', 'none'),
(73, 15, NULL, 0x31353239353735333538, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '', '', 1, 0, '', '', '', '', '', ''),
(75, 15, 0, 0x31353239353736343132, NULL, 'syed', 'ALI1', 'dasad@gmail.com', '03122', 'DDAS', NULL, '2018-12-31', 45, 'KARACHI ', 'das', 2, 0, '', '', '', '', '', ''),
(76, 18, NULL, 0x31353333353537343037, NULL, 'das', 'das', 'dasd@gmail.com', '03122912921', 'sda', NULL, '01-03-2012', 0, 'das', 'dasd', 1, 511, 'das', 'dsa', 'das', 'IU DEPARTMENT', '', ''),
(77, 45, NULL, 0x31353333383832323138372d322d677261706869632d64657369676e2d706963747572652e706e67, NULL, 'Sanaullah', 'Channa', 'sunysagar05@gmail.com', '0300-3128454', 'Channa Muhalla Sobhodero district khairpur', 'male', '06/11/1994', 66110, ' karachi', 'Develop a Developed way of work.', 0, 1361, ' ', 'www.fb.com/pk', 'CodeIgniter', 'COMPUTER & TECHNOLOGY', 'Bachelor of Science in Computer Science BS (CS)', 'none'),
(78, 55, NULL, 0x31353335343432393835494d475f303131392e4a5047, NULL, 'Haseeb ', 'Ahmed', 'haseebahmed676@gmail.com', '03060954947', 'house # 8, street # 19, G81, Islamabad', NULL, '20-09-1994', 44000, 'Islamabad ', 'Seeking a position as a software developer where I can polish my skills and boost my career.', 0, 17170, '', '', 'Asp.Net MVC, Angular 2', 'COMPUTER & TECHNOLOGY', '', 'none'),
(79, 75, NULL, 0x31353335343433343032, NULL, 'Amir', 'Khan', 'ballers999@gmail.com', '03445151822', 'Ho 92, St 35-B I-9/4', 'male', '10-11-1992', 44000, ' Islamabad  ', 'To be a part of a dynamic team, working in a cooperative environment where one can have better\r\nchance of learning and grooming as an individual. Further, to utilize my potential to the maximum\r\nlevel in the interest of organization for achieving its goals and objectives.\r\n', 0, 18103, '', '', '', 'COMPUTER & TECHNOLOGY', 'Bachelor of Science in Computer Science BS (CS)', 'none'),
(80, 79, NULL, 0x31353335343433353139, NULL, 'Muhammad', 'Khan', 'zubairkhanofficial@gmail.com', '03335228227', 'H#ZB-1692 Mehrabad Dhoke Hassu', 'male', '01-08-1993', 46000, 'Rawalpindi  ', 'To obtain the position of developer with reputed firm where I can learn and boost my skills to meet competitive industry requirements and to become an important asset for the company.', 0, 20123, 'Zubair', '', '', 'COMPUTER & TECHNOLOGY', 'Bachelor of Science in Computer Science BS (CS)', 'none'),
(81, 77, NULL, 0x31353335343433353136494d472d32303138303731362d5741303032302e6a7067, NULL, 'Maira', 'Pervaiz', 'pervaizmaira@gmail.com', '03345891094', '14/3C, C-block, Satellite Town, Rawalpindi.', 'female', '01-06-1995', 44000, ' Rawalpindi', 'To enhance my skills by working with oragnization that admire new talent and wish to polish their skills by giving them opportunities, feel proud to give them a chance.', 1, 22165, '', '', '', 'COMPUTER & TECHNOLOGY', 'Bachelor of Science in Computer Science BS (CS)', 'none'),
(82, 66, NULL, 0x3135333534343336303131393636353532305f313430313032383637333332313832305f333239323730343330393032323932313431355f6e2e6a7067, NULL, 'Umar ', 'Zaman', 'umarofficial9@gmail.com', '03426221647', 'h#1312-A, street#84, sector I-10/1', 'male', '09-06-1995', 44000, 'Islamabad', 'Highly motivated graduated bachelor with 3.46 CGPA looking to attain a position of in development field to contribute my knowledge, skills and experience for the advancement of the company while studying and making myself grow with the company', 0, 20007, '', 'https://www.facebook.com/UmarZamann9', '', 'COMPUTER & TECHNOLOGY', 'Bachelor of Science in Computer Science BS (CS)', 'none'),
(83, 85, NULL, 0x31353335343434363235, NULL, 'Umer', 'Mirza', 'umrmirza@gmail.com', '03315174751', 'house#4, Street#2 Fazaia colony Rawalpindi', NULL, '07-12-1989', 46000, 'Rawalpindi ', 'To secure a responsible career opportunity in a challenging environment. Where I can utilize my human resource and management skills to refine my past experience and academic skills, thus making a significant contribution to the success of my employer.', 0, 127723, 'Hayat', '', '', 'BUSINESS ADMINISTRATION', '', 'none'),
(84, 96, NULL, 0x3135333534343731383231393837353536395f313934383330323736353431353838345f343835333631333435323739313632343134315f6e2e6a7067, NULL, 'Areesha', 'Syed', 'areeshasyed696@gmail.com', '03134636006', '18-B Poonch House Staff Colony, Chauburji', NULL, '20-07-1996', 54000, ' Lahore ', 'To serve an organization where I face challenging work environment and present my creativity and innovations. I look forward to an organization which makes my way towards lucid career, and rewards me for the skills that I possess. I would not only prefer an organization where I can earn a good amount of money, but also such an organization which helps me explore more about my own self, about team work and the practical life.   ', 0, 1, '', '', '', 'COMPUTER & TECHNOLOGY', '', 'none'),
(85, 103, NULL, 0x31353335343437313933, NULL, 'Akash', 'Keyani', 'akashkeyani@gmail.com', '03465081857', 'House# 8-E, St# 57, Sector # G-6/4 Isbd', 'male', '06-10-1994', 44000, ' Capital', 'Want to be a part of University where i want to express the blunders of current systems and improve the rules and make University a better place for students rather than just a cash machine...\r\n', 0, 16025, 'Ur Rehman', '', '', 'COMPUTER & TECHNOLOGY', 'Bachelor of Science in Computer Science BS (CS)', 'none'),
(86, 113, NULL, 0x31353335343438333331, NULL, 'Nabeel Khurshid ', 'Khurshid Ali', 'nabeelkhurshid95@gmail.com', '03363975597', 'D-12/1', 'male', '20-07-1995', 45710, ' Islamabad   ', 'To gain a confidence with experience ', 0, 25200, '', '', '', 'BUSINESS ADMINISTRATION', 'Bachelor of Business Administration (BBA)', 'none'),
(87, 131, NULL, 0x313533353435323633364e657720446f6320323031382d30362d3037202831295f312e6a7067, NULL, 'Danial ', 'Khalid', 'danialkhalid692@gmail.com', '03455218149', 'H#30, Street 05, Lane 01 Gulrez Phase 2 Rawalpindi', NULL, '06/03/1992', 46000, ' Rawalpindi ', 'To', 0, 21426, '', '', 'Finance and Marketing', 'BUSINESS ADMINISTRATION', '', 'none'),
(88, 136, NULL, 0x313533353435323834314d20414244554c4c41482e6a7067, NULL, 'Muhammad', 'Abdullah', 'ashar621@gmail.com', '03135639555', 'House # CB-100 Darbar e karimi near new city wah cantt.', 'male', '06-02-1995', 47040, ' Wah Cantt', 'I believe in hard work and very confident about myself to become a part of well Reputed Organization and show my abilities there and play my role in its progress.', 0, 17627, '', '', 'Web designing and Development, Graphic Designing, ', 'COMPUTER & TECHNOLOGY', 'Bachelor of Science in Computer Science BS (CS)', 'none'),
(89, 122, NULL, 0x31353335343534383736494d472d32303135303531322d5741303030322e6a7067, NULL, 'Syed ', 'Zain-ul-abdeen', 'zain21034@gmail.com', '0333-7917679', '304, A-3 Rabbi Apartments , Gulshan-e-iqbal, Karachi', NULL, '15-08-1990', 75600, 'karachi ', 'To seek career in a reputable organization which can help me to grow as a professional and help me contribute my efforts in the progression of the organization.', 0, 21034, '', '', '', 'BUSINESS ADMINISTRATION', '', 'none'),
(90, 165, NULL, 0x31353335343734303231, NULL, 'Muhammad', 'Atif', 'muhammadatif366@gmail.com', '03065008025', 'Directorate of Projects AMF PAC Kamra Attock Pakistan, Five Star Bag Super Bilal Market Gate No 2 Kamra Attock Pakistan', 'male', '06-01-1989', 43600, 'Attock  ', 'To work gregarious and attain hierarchy management level.', 0, 2147483647, '', 'https://www.facebook.com/muhammad.atif.50552', 'Financial analyst', 'BUSINESS ADMINISTRATION', 'Master of Business Administration (MBA)', 'Finance'),
(91, 188, NULL, 0x313533353439353335355f445343303339336e2e6a7067, NULL, 'Usman', 'Ahmed', 'ussmanmani@gmail.com', '03222362222', 'Flat No 507C, Almustafa Tower F10, Markaz Islamabad', 'male', '26-06-1988', 43000, ' Islamabad', 'Project Management', 0, 21607, '', '', '', 'BUSINESS ADMINISTRATION', 'Master of Business Administration (MBA)', 'Technology Management'),
(92, 44, NULL, 0x31353338323938353337, NULL, 'nida', 'tariq', 'nidatariq64@gmail.com', '03315055118', 'street # 24  rawalpindi', NULL, '05-07-1993', 46000, 'rwp  ', 'dsfsdf', 1, 1234, '', '', '', 'COMPUTER & TECHNOLOGY', '', 'none'),
(93, 140, NULL, 0x31353335353239383135, NULL, 'Shahid', 'Karim', 'shahidkarim784@gmail.com', '03420551738', 'Muhallah Sonikot Gilgit', NULL, '19-08-1993', 15100, ' Gilgit ', 'To become a Specialized Financial Analyst ', 0, 20411, '', 'Facebook Instagram', 'Finance', 'BUSINESS ADMINISTRATION', '', 'none'),
(94, 201, NULL, 0x3135333535343634353031353339313034345f313235303335393539383333363037305f333835333837353336323535333530383030385f6e2e6a7067, NULL, 'muhammad', 'rahat', 'raheelrahat@hotmail.com', '03355136251', 'h.no sn 1878 street no 5 farooq e azam colony shamsabad rawalpindi', 'male', '10-01-1992', 46000, ' rawalpindi', 'entrepreneur', 0, 15036, 'raheel', 'https://web.facebook.com/raheel.rahat.786', 'digital marketing', 'COMPUTER & TECHNOLOGY', 'Bachelor of Science in Computer Science BS (CS)', 'none'),
(95, 207, NULL, 0x31353335353535383437, NULL, 'Faris', 'Fahim', 'farisfahim1@yahoo.com', '03025225498', 'House # 177, Street # 8 , Majistrate Colony , Sadiqabad Rawalpindi', NULL, '26-03-1996', 46300, 'Rawalpindi ', 'Experienced Software Engineer ,searching for the opportunity to bring 3+ years experience with extensive practice in Data Mining and Web Development, seeking work to utilize design and customer service abilities.', 0, 25248, '', 'https://www.facebook.com/faris.fahim2', '', 'COMPUTER & TECHNOLOGY', '', 'none'),
(96, 212, NULL, 0x313533353536343039333030312e6a7067, NULL, 'Ali ', 'Sohail', 'ali112008@live.com', '03366631352', 'house no 372 river view road phase 4 bahria town islamabad', NULL, '30-08-1995', 92, ' islamabad ', 'internship required', 0, 20259, 'Asghar', '', '', 'COMPUTER & TECHNOLOGY', '', 'none'),
(97, 240, NULL, 0x31353335373838353634323031362d30392d31332d31372d34352d30322d3738382e6a7067, NULL, 'Nagza ', 'Naveed', 'nagza91@gmail.com', '03435551692', 'Street 80,I 10 islamabad', NULL, '06-10-1991', 44000, ' Islamabad ', 'To work with a professional group that could offer opportunity for career advancement, good professional growth and the utilization of my skills in a dynamic work environment to achieve new levels of competence', 0, 11220, '', '', '', 'COMPUTER & TECHNOLOGY', '', 'none'),
(98, 156, NULL, 0x31353335383736363030, NULL, 'Muhammad Usman Khan', 'Khan', 'itsusmankhan853@gmail.com', '03325951979', 'HOUSE # 962/1 ,STREET # 12 NASEERABAD OPPOSITE TO KOHINOOR MILLS RAWALPINDI', 'male', '03-06-1995', 46000, ' rawalpindi  ', 'Motivated, selfless, goal oriented and committed professional seeking a position in a result-oriented Organization.', 0, 19340, 'Usman', '', 'Basic C++,JAVA,HTML ,CSS, Word,Excel', 'COMPUTER & TECHNOLOGY', '', 'none'),
(99, 244, NULL, 0x31353335383334323438, NULL, 'Ahmad', 'Qazi', 'hasanqazi36@gmail.com', '03455893338', 'House # 570, Block C, Street # 40, PAK PWD Islamabad', 'male', '16-12-1991', 44000, ' Islamabad', 'To work in a challenging environment in an Organization dealing with Information Technologies, Computer technologies and learning perspective etc. To achieve result oriented targets.', 0, 16202, 'Hasan Alam ', '', '', 'COMPUTER & TECHNOLOGY', 'Bachelor of Science in Computer Science BS (CS)', 'none'),
(100, 253, NULL, 0x3135333539313031333832303138303832375f3230313834372e6a7067, NULL, 'Muhammad Abdullah', 'Abdullah', 'abdullahmalik3840@gmail.com', '03318623840', 'G-10/1 house number 131', 'male', '25-10-1993', 44000, ' Islamabad', 'Software developer', 0, 16130, 'Abdullah', '', '', 'IU DEPARTMENT', '', 'none'),
(101, 263, NULL, 0x31353336333139313638352e6a7067, NULL, 'PARIS', 'SAFDAR', 'parissafdar@gmail.com', '03435380628', 'HOUSE NO.3820 GAWALMANDI RAWALPINDI', 'male', '11-11-1991', 4600, ' RAWALPINDI', 'My objective is to obtain a position in  a professional office environment where my skills are valued and \r\ncan benefit the organization.  Ideally,  I wish to have a focus in marketing/Advertisement  for a  growing \r\norganization.  I am an enthusiastic, reliable, ambitious and a good communicator, which  enforces me to \r\nwork on my own initiative individually and as part of a team', 0, 26290, '', '', '', 'BUSINESS ADMINISTRATION', 'Master of Business Administration (MBA)', 'Marketing'),
(102, 271, NULL, 0x31353336383632333039, NULL, 'Zeeshan ', 'Farooq', 'zeeshanfarooq46@gmail.com', '03315263072', 'H # 15 S # 10, Muzamil Town Kurri Road Rawalpindi ', NULL, '01-11-1990', 46000, ' Rawalpindi ', 'Seeking a challenging opportunity to work in a dynamic environment, where I could prove my abilities towards the growth of the organization and my career.', 0, -20222, 'Shani', '', 'Customer Services, Banking, Warehousing & Distribu', 'BUSINESS ADMINISTRATION', '', 'none'),
(103, 275, NULL, 0x31353339333430313139696d616765732e6a666966, NULL, 'ashdhj', 'hasjdhasj', 'asjdhjas@gmail.com', '03333333333', 'lsakjdlsakj', 'male', '30-06-1993', 0, 'jaslkdjaslkj', 'hamza', 0, 38, 'ahsjdhasjh', '', 'asdasd', 'IU DEPARTMENT', '', 'none');

-- --------------------------------------------------------

--
-- Table structure for table `program`
--

CREATE TABLE `program` (
  `program_id` int(10) NOT NULL,
  `program_name` varchar(50) NOT NULL,
  `dept_id` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `program`
--

INSERT INTO `program` (`program_id`, `program_name`, `dept_id`) VALUES
(1, 'Bachelor of Science in Computer Science BS (CS)', 'COMPUTER & TECHNOLOGY'),
(2, 'Bachelor of Science in Computer Science BS (CS)', 'COMPUTER & TECHNOLOGY'),
(3, 'Bachelor of Science in Computer Science BS (CS)', 'COMPUTER & TECHNOLOGY'),
(4, 'Master of Science in Telecommunication and Network', 'COMPUTER & TECHNOLOGY'),
(5, 'Doctor of Philosophy in Computer Sciences PhD (CS)', 'COMPUTER & TECHNOLOGY'),
(6, 'Doctor of Philosophy in Telecommunication and Netw', 'COMPUTER & TECHNOLOGY'),
(7, 'Bachelor of Fashion Design (BFD)', 'FASHION & DESIGN'),
(8, 'Bachelor of Textile Design (BTD)', 'FASHION & DESIGN'),
(9, 'BMS Advertising', 'MEDIA STUDIES'),
(10, 'BMS Animation', 'MEDIA STUDIES'),
(11, 'Film & TV ', 'MEDIA STUDIES'),
(12, 'Bachelor of Business Administration (BBA)', 'BUSINESS ADMINISTRATION'),
(13, 'Master of Business Administration (MBA)', 'BUSINESS ADMINISTRATION'),
(14, 'Bachelor of Science in Development BS (DS)', 'SOCIAL SCIENCES'),
(15, 'Bachelor of Science in Social Sciences BS (SS)', 'SOCIAL SCIENCES'),
(16, 'Bachelor of Science in International Relations BS ', 'SOCIAL SCIENCES'),
(17, 'Bachelor of Science in Economics', 'SOCIAL SCIENCES'),
(18, 'Master of Science in Development Studies, MSc (DS)', 'SOCIAL SCIENCES'),
(19, 'Master of Science in International Relations, MSc ', 'SOCIAL SCIENCES'),
(20, 'MPhil IDS ', 'SOCIAL SCIENCES');

-- --------------------------------------------------------

--
-- Table structure for table `ref_tb`
--

CREATE TABLE `ref_tb` (
  `ref_id` int(11) NOT NULL,
  `master_id` int(11) DEFAULT NULL,
  `ref_name` varchar(200) DEFAULT NULL,
  `ref_phone` varchar(200) DEFAULT NULL,
  `ref_email` varchar(200) DEFAULT NULL,
  `ref_desg` varchar(500) DEFAULT NULL,
  `ref_instit` varchar(200) DEFAULT NULL,
  `ref_address` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ref_tb`
--

INSERT INTO `ref_tb` (`ref_id`, `master_id`, `ref_name`, `ref_phone`, `ref_email`, `ref_desg`, `ref_instit`, `ref_address`) VALUES
(10, 70, 'saad', '03122912921', 'S@GMAIL.COM', 'dasdas', 'das', ''),
(11, 70, 'SAAD', '03122', 'S@GMAIL.COM', 'SAAD', 'DASDA', 'dasda'),
(12, 81, '', '', '', '', '', ''),
(13, 80, '', '', '', '', '', ''),
(14, 83, 'Air Commodore Sikander Hayat Mirza', '', '', 'HOD', 'RCMS,Nust', ''),
(15, 83, 'Air Commodore Sikander Hayat Mirza', '', '', 'HOD', 'RCMS,Nust', ''),
(16, 84, 'Areesha Syed', '03134636006', 'areeshasyed696@gmail.com', 'Student', 'iqra university', ''),
(17, 88, 'Mr Muhammad Sabir Qamar', '0345-5323231', 'sabir.qamar@iqraisb.edu.pk', 'Deputy Controller of Examination', 'Iqra University Islamabad Campus', ''),
(18, 88, 'Mr Umaid Ajmal', '0333-5393452', 'umaid.ajmal@iqraisb.edu.pk', 'Lab Engineer', 'Iqra University Islamabad Campus', ''),
(19, 92, 'ewqeqw', '3432423', 'nida@gmail.com', '43dfdfvd', 'vdsfd', ''),
(20, 95, 'Fahad Fahim', '03005163965', 'fahadfahim12@gmail.com', 'Mechanical Engineer', 'OGDCL', ''),
(21, 96, '', '', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `registeration`
--

CREATE TABLE `registeration` (
  `reg_id` int(11) NOT NULL,
  `role_id` int(11) DEFAULT NULL,
  `reg_username` varchar(200) DEFAULT NULL,
  `reg_contact` varchar(200) DEFAULT NULL,
  `reg_email` varchar(200) DEFAULT NULL,
  `reg_password` varchar(200) DEFAULT NULL,
  `reg_image` blob
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `registeration`
--

INSERT INTO `registeration` (`reg_id`, `role_id`, `reg_username`, `reg_contact`, `reg_email`, `reg_password`, `reg_image`) VALUES
(12, NULL, 'Sanaullah', '03003128454', 'admn@gmail.com', '@123', 0x3135323835333435353933355f3338335f333232385f3039315f463130202831292e6a7067),
(13, NULL, 'Syed Ali', '03122123123', 'syed@gmail.com', 'syed', 0x3135323835333436323264306633653533303365663862623933366665636238663065383766323564642d2d6b69642d636c6f7468696e672d62616e6e65722d64657369676e2e6a7067),
(14, NULL, 'Core2plus', '021-750383', 'core2plus@gmail.com', 'core2', 0x313532383533373130326332702d6c6f676f2e706e67),
(15, NULL, 'syed', 'ali', 'syedalisaad488@gmail.com', '123', 0x31353238353739373532627573696e6573732e6a7067),
(16, NULL, 'syed', 'ali', 'abc@gmail.com', '123', 0x3135323836313237313932323038393936315f323032323130313134343733333031385f313231383132373631343332393437363735385f6e2e6a7067),
(17, NULL, 'admin', '03122912921', 'syedalisaad488@gmail.com', '123', 0x3135323934383038333133353339343230335f323433333832313731333330323438315f333238363731323239303036313035383034385f6e2e6a7067),
(18, NULL, 'admin', '03122912921', 'admin@gmail.com', '123', 0x3135323934383039373633353439363438335f323433333832313331333330323532315f313532363035323130313332313339323132385f6e2e6a7067),
(19, NULL, 'baby', '03122912921', 'baby@gmail.com', '123', 0x3135323939393934303133353439363438335f323433333832313331333330323532315f313532363035323130313332313339323132385f6e2e6a7067),
(20, NULL, 'Admin', '03122912921', 'a@gmail.com', '123', 0x3135333030313039323433353437373133325f3438333836353534353337393139385f343030393332363238363931313536393932305f6e2e6a7067),
(21, NULL, 'Admin', '03122912921', 'admin@gmail.com', 'admin123', 0x31353330333538323731547769672d4761735f53656e736f722e626d70),
(22, NULL, 'Admin', '03122912921', 'syedalisaad488@gmail.com', '123', 0x3135333133303038373441756864612064617368626f6172642e6a7067),
(23, NULL, 'Admin', '03122912921', 'admin123@gmail.com', '123', 0x31353331333030393432736d616c6c2d6c6f676f2e706e67),
(24, NULL, 'Syed Ali', '03122912921', 'syedalisaad488@gmail.com', '123', 0x3135333335333939303832323038393936315f323032323130313134343733333031385f313231383132373631343332393437363735385f6e2e6a7067),
(25, NULL, 'admin', '03122912921', 'syedalisaad488@gmail.com', '123', 0x31353333353430323336697172616c6f676f736d616c6c2e706e67),
(26, NULL, 'admin', '03122912921', 'syedalisaad488@gmail.com', '123', 0x31353333353430373439547769672d4761735f53656e736f722e626d70),
(27, NULL, 'admin', '03122912921', 'syedalisaad488@gmail.com', '1234', 0x31353333353430393036547769672d4761735f53656e736f722e626d70),
(28, NULL, 'Admin', '03122912921', 'syedalisaad488@gmail.com', '123', 0x3135333335343039333332323038393936315f323032323130313134343733333031385f313231383132373631343332393437363735385f6e2e6a7067),
(29, NULL, 'Syed Ali', '03122912921', 'syedalisaad488@gmail.com', '123', 0x31353333353431333932547769672d4761735f53656e736f722e626d70),
(30, NULL, 'Admin', '03122912921', 'syedalisaad488@gmail.com', '123', 0x3135333335343134323532323038393936315f323032323130313134343733333031385f313231383132373631343332393437363735385f6e2e6a7067),
(31, NULL, 'Admin', '03122912921', 'syedalisaad488@gmail.com', '123', 0x31353333353431343838547769672d4761735f53656e736f722e626d70),
(32, NULL, 'Admin', '03122912921', 'admin123@gmail.com', '123', 0x3135333335343136313161636664386339312d623836362d343234302d386237632d6336656435343264383565352e706e67),
(33, NULL, 'Admin', '03122912921', 'admin123@gmail.com', '123', 0x3135333335343136333861636664386339312d623836362d343234302d386237632d6336656435343264383565352e706e67),
(34, NULL, 'Syed Ali', '03122912921', 'admin123@gmail.com', '123', 0x31353333353431373135627573696e6573732e6a7067),
(35, 1, '17896', '03122912921', 'admin@iqra.com', 'iu@101', 0x31353333353431373835697172616c6f676f736d616c6c2e706e67),
(36, NULL, 'Admin', '03122912921', 'admin123@gmail.com', '123', 0x31353333353431383839697172616c6f676f736d616c6c2e706e67),
(37, NULL, 'shoaib', '03472911682', 'shoaib@gmail.com', '123', 0x3135333337313535383273686f6269206e206e69617a2e6a7067),
(38, NULL, 'shoaib', '03472911682', 'shoaib@gmail.com', '123', 0x3135333337313536333173686f6269206e206e69617a2e6a7067),
(39, NULL, 'shoaib', '03472911682', 'shoaib@gmail.com', '123', 0x3135333337313536343873686f6269206e206e69617a2e6a7067),
(40, NULL, 'shoaib', '03472911682', 'shoaib123@gmail.com', '123', 0x3135333337313536393573686f6269206e206e69617a2e6a7067),
(41, NULL, 'shoaib', '03472911682', 'shoaib123@gmail.com', '123', 0x3135333337313537323473686f6269206e206e69617a2e6a7067),
(42, NULL, 'ali', '03472911682', 'test@gmail.com', '123', 0x3135333337313834303231333731315f3931343333353039383632393530355f393231333735333737353330353531383437305f6e2e6a7067),
(43, NULL, 'ali', '03472911682', 'test@gmail.com', '123', 0x3135333337313834303431333731315f3931343333353039383632393530355f393231333735333737353330353531383437305f6e2e6a7067),
(44, NULL, 'nida', '03315055118', 'nidatariq64@gmail.com', '123', 0x3135333338313634393546616365626f6f6b2d4769726c732d46616b652d446973706c61792d50696374757265732d46422d66616b652d6769726c732d6c65616b65642d70726f66696c652d70696374757265732d3235312e6a7067),
(45, NULL, 'Sanaullah', '03003128454', 'sunii@ymail.com', '12345', 0x31353333383831393738332e6a7067),
(46, NULL, 'Sanaullah', '03003128454', 'sunii@ymail.com', '12345', 0x31353333383832303033332e6a7067),
(47, NULL, 'shoaib', '03472911682', 'shoaib@gmail.com', '123', 0x31353334353930343339612e6a7067),
(48, NULL, 'shoaib', '03472911682', 'shoaib123@gmail.com', '123', 0x31353334353930343735612e6a7067),
(49, NULL, 'owais', '03472911682', 'ows@gmail.com', '123', 0x31353334353933363031612e6a7067),
(50, NULL, 'M.Ahmad Jan ', '03317077711', 'ajaan085@gmail.com', 'dubai1001', 0x31353335343432343938317a7a78782e6a7067),
(51, NULL, 'M.Ahmad Jan ', '03317077711', 'ajaan085@gmail.com', 'dubai1001', 0x31353335343432353231317a7a78782e6a7067),
(52, NULL, 'Muhammad Maroof', '03325316660', 'Muhammadmaroof44@gmail.com', 'iqrauniversity', 0x3135333534343235323346425f494d475f313533333937393834353136332e6a7067),
(53, NULL, 'Sajjad Ali', '03450486720', 'sajjadali223223@gmail.com', 'S@jjadali005566', 0x3135333534343235373846425f494d475f313533353330353235363238322e6a7067),
(54, NULL, 'Sajjad Ali', '03450486720', 'sajjadali223223@gmail.com', 'S@jjadali005566', 0x3135333534343236323646425f494d475f313533353330353235363238322e6a7067),
(55, NULL, 'Haseeb Ahmed', '03060954947', 'haseebahmed676@gmail.com', 'Kashifashakur6', 0x31353335343432363332494d475f303131392e4a5047),
(56, NULL, 'Haseeb Ahmed', '03060954947', 'haseebahmed676@gmail.com', 'Kashifashakur6', 0x31353335343432363631494d475f303131392e4a5047),
(57, NULL, 'Sajjad Ali', '03450486720', 'sajjadali223223@gmail.com', 'S@jjadali005566', 0x3135333534343236373546425f494d475f313533353330353235363238322e6a7067),
(58, NULL, 'Haseeb Ahmed', '03060954947', 'haseebahmed676@gmail.com', 'Kashifashakur6', 0x31353335343432363831494d475f303131392e4a5047),
(59, NULL, 'Junaid Ali', '03065019519', 'junaidali9559@gmail.com', 'malikjunaidali1', 0x3135333534343237373232303138303231305f3139323230372e6a7067),
(60, NULL, 'Abbas Haider', '0347-5126462', 'syedabbashaiderbukhari@yahoo.com', 'Abbasshah55', 0x3135333534343237383941464631353842412d303841322d343143392d383144322d3030303533363736393343362e6a706567),
(61, NULL, 'Mehboob Nazim Shehzad', '03320593372', 'mehboobfraz85@gmail.com', '0125301253', 0x3135333534343237393143414d30303532352e6a7067),
(62, NULL, 'Faysal Shahzad', '03005290195', 'fysal.shahzad@gmail.com', 'AAkber**2018', 0x3135333534343238353646616973616c20536861687a61642e6a7067),
(63, NULL, 'Shahzad Khan', '03315739986', 'blueshahzad@gmail.com', '756283', 0x3135333534343238383131393837353336385f3939313331353535373633383338325f383333373731343738303832363535333137345f6e2e6a7067),
(64, NULL, 'Faysal Shahzad', '03005290195', 'fysal.shahzad@gmail.com', 'AAkber**2018', 0x3135333534343238393246616973616c20536861687a61642e6a7067),
(65, NULL, 'Faysal Shahzad', '03005290195', 'fysal.shahzad@gmail.com', 'AAkber**2018', 0x3135333534343238393746616973616c20536861687a61642e6a7067),
(66, NULL, 'Umar Zaman', '03426221647', 'umarofficial9@gmail.com', '16129200a', 0x3135333534343239303137423036353337432d393744462d344632352d383642412d3243363636383638433745352e6a706567),
(67, NULL, 'Umar Zaman', '03426221647', 'umarofficial9@gmail.com', '16129200a', 0x3135333534343239303837423036353337432d393744462d344632352d383642412d3243363636383638433745352e6a706567),
(68, NULL, 'Shahzad Khan', '03315739986', 'blueshahzad@gmail.com', '756283', 0x3135333534343239313131393837353336385f3939313331353535373633383338325f383333373731343738303832363535333137345f6e2e6a7067),
(69, NULL, 'Umar Zaman', '03426221647', 'umarofficial9@gmail.com', '16129200a', 0x3135333534343239323137423036353337432d393744462d344632352d383642412d3243363636383638433745352e6a706567),
(70, NULL, 'Shahzad Khan', '03315739986', 'blueshahzad@gmail.com', '756283', 0x3135333534343239323231393837353336385f3939313331353535373633383338325f383333373731343738303832363535333137345f6e2e6a7067),
(71, NULL, 'Mehboob Nazim Shehzad', '03320593372', 'mehboobfraz85@gmail.com', '0125301253', 0x3135333534343239343743414d30303532352e6a7067),
(72, NULL, 'Muhammad Faisal', '03343808080', 'faisal.techmetis@gmail.com', 'Aalrehman16', 0x31353335343432393534494d475f3135303120312e4a5047),
(73, NULL, 'Junaid Ali', '03065019519', 'junaidali9559@gmail.com', 'malikjunaidali', 0x31353335343432393534494d472d32303137303332302d5741303030342e6a7067),
(74, NULL, 'Ahmad Waleed Khan', '03005078076', 'ahmad.waleed221@gmail.com', 'abug6082346', 0x31353335343432393933313533353434323732353732372d3132393136393132332e6a7067),
(75, NULL, 'Amir Khan', '03445151822', 'ballers999@gmail.com', 'computer0000', 0x31353335343433303535416d697220696d6167652e6a7067),
(76, NULL, 'Amir Khan', '03445151822', 'ballers999@gmail.com', 'computer0000', 0x31353335343433303636416d697220696d6167652e6a7067),
(77, NULL, 'Maira Pervaiz', '03345891094', 'pervaizmaira@gmail.com', 'maira12345', 0x31353335343433303830494d472d32303138303731362d5741303030312e6a7067),
(78, NULL, 'Maira Pervaiz', '03345891094', 'pervaizmaira@gmail.com', 'maira12345', 0x31353335343433313131494d472d32303138303731362d5741303030312e6a7067),
(79, NULL, 'Muhammad Zubair Khan', '03335228227', 'zubairkhanofficial@gmail.com', 'PataNahe', 0x313533353434333132334453435f393532382e4a5047),
(80, NULL, 'Muhammad Zubair Khan', '03335228227', 'zubairkhanofficial@gmail.com', 'PataNahe', 0x313533353434333135304453435f393532382e4a5047),
(81, NULL, 'Muhammad Zubair Khan', '03335228227', 'zubairkhanofficial@gmail.com', 'PataNahe', 0x313533353434333138314453435f393532382e4a5047),
(82, NULL, 'Mehboob Nazim Shehzad', '03320593372', 'mehboobfraz85@gmail.com', '0125301253', 0x3135333534343334353643414d30303532352e6a7067),
(83, NULL, 'Muhammad Usama Bin Raza', '03328892212', 'usamaraza88@gmail.com', 'ibanustlums', 0x31353335343433343833696d673430352e6a7067),
(84, NULL, 'Muhammad Junaid', '03325999413', 'Junaidbhatti888@gmail.com', 'gwnzxk', 0x313533353434333731325f445343303331322e4a5047),
(85, NULL, 'Umer Hayat Mirza', '03315174751', 'umrmirza@gmail.com', '123456', 0x31353335343433383638494d472d32303138303230322d574130303238206e65772e6a7067),
(86, NULL, 'Umer Hayat Mirza', '03315174751', 'umrmirza@gmail.com', '123456', 0x31353335343434313934494d472d32303138303230322d574130303238206e65772e6a7067),
(87, NULL, 'Ahmad Raza', '03125367998', 'ahmadraza815@gmail.com', 'impcch84b.com', 0x31353335343434373838302e6a706567),
(88, NULL, 'Ahmad Raza', '03125367998', 'ahmadraza815@gmail.com', 'impcch84b.com', 0x31353335343434383134302e6a706567),
(89, NULL, 'Laila Batool', '03402074884', 'laila.alam95@gmail.com', '1995laila', 0x3135333534343439313632303137303830325f3138303133382e6a7067),
(90, NULL, 'Laila Batool', '03402074884', 'laila.alam95@gmail.com', '1995laila', 0x3135333534343439373432303137303830325f3138303133382e6a7067),
(91, NULL, 'Muhammad Saffi Khan', '03425299239', 'saffimsk@gmail.com', 'asdfghjkl', 0x31353335343434393835494d472d32303138303733302d5741303033342e6a7067),
(92, NULL, 'Laila Batool', '03402074884', 'laila.alam95@gmail.com', '1995laila', 0x3135333534343530343832303137303830325f3138303133382e6a7067),
(93, NULL, 'Ali haider', '03078367133', 'haiderbellek7@gmail.com', 'nikeisbest8', 0x31353335343435303732313533353230373338393632332e706e67),
(94, NULL, 'Farrukh Abbasi ', '03345355007', 'farrukha905@gmail.com', 'abbasi786', 0x3135333534343531333832363536323235382d393931332d344142302d413544312d3833414344383030373935432e6a706567),
(95, NULL, 'Israr Nemat', '03315044237', 'israr.nemat@gmail.com', 'israr123456@', 0x31353335343435353634313438343339303336303935392d63396161343866322d303839362d343265332d396238372d3162323531616561613761655f2d6d696e2e6a7067),
(96, NULL, 'Areesha Syed', '03134636006', 'areeshasyed696@gmail.com', 'areeshasyed22', 0x31353335343435373137697172612e6a7067),
(97, NULL, 'Areesha Syed', '03134636006', 'areeshasyed696@gmail.com', 'areeshasyed22', 0x31353335343435373334697172612e6a7067),
(98, NULL, 'Aqeel Mustafa', '03315065643', 'aqeel.sundhu@gmail.com', 'w7y6bm', 0x31353335343435373933556e7469746c65642e706e67),
(99, NULL, 'Israr Nemat', '03315044237', 'israr.nemat@gmail.com', 'israr123456@', 0x31353335343435383539313438343339303336303935392d63396161343866322d303839362d343265332d396238372d3162323531616561613761655f2d6d696e2e6a7067),
(100, NULL, 'Aqeel Mustafa', '03315065643', 'aqeel.sundhu@gmail.com', 'w7y6bm', 0x31353335343435393237556e7469746c65642e706e67),
(101, NULL, 'Israr Nemat', '03315044237', 'israr.nemat@gmail.com', 'israr123456@', 0x31353335343435393835313438343339303336303935392d63396161343866322d303839362d343265332d396238372d3162323531616561613761655f2d6d696e2e6a7067),
(102, NULL, 'Mubashir Shah', '03365354845', 'Mubashirshah002@gmail.com', 'Shah@Uni', 0x31353335343436303434494d472d32303138303731312d5741303030332e6a7067),
(103, NULL, 'Akssh ur rehman keyani', '03465081857', 'akashkeyani@gmail.com', 'aniqa007', 0x31353335343436313235494d472d32303138303630352d5741303031372e6a7067),
(104, NULL, 'Akssh ur rehman keyani', '03465081857', 'akashkeyani@gmail.com', 'aniqa007', 0x31353335343436313535494d472d32303138303630352d5741303031372e6a7067),
(105, NULL, 'Muhammad Junaid', '03325999413', 'Junaidbhatti888@gmail.com', 'gwnzxk', 0x313533353434363431335f445343303331322e4a5047),
(106, NULL, 'lizmia ', '+923313833315', 'alilizmia@gmail.com', 'ilovemyself', 0x313533353434363835356d6167617a696e652d756e6c6f636b2d30312d322e332e313038322d5f33363632414234413836423634374431433832363641433530334238314231382e6a7067),
(107, NULL, 'lizmia ', '+923313833315', 'alilizmia@gmail.com', 'ilovemyself', 0x313533353434363837386d6167617a696e652d756e6c6f636b2d30312d322e332e313038322d5f33363632414234413836423634374431433832363641433530334238314231382e6a7067),
(108, NULL, 'lizmia ali', '+923313833315', 'alilizmia@gmail.com', 'ilovemyself', 0x3135333534343639393846425f494d475f313533353434363935323937362e6a7067),
(109, NULL, 'lizmia ali', '+923313833315', 'alilizmia@gmail.com', 'ilovemyself', 0x3135333534343730313546425f494d475f313533353434363935323937362e6a7067),
(110, NULL, 'lizmia ali', '+923313833315', 'alilizmia@gmail.com', 'ilovemyself', 0x3135333534343730323746425f494d475f313533353434363935323937362e6a7067),
(111, NULL, 'lizmia ali', '+923313833315', 'alilizmia@gmail.com', 'ilovemyself', 0x3135333534343731323446425f494d475f313533353434363935323937362e6a7067),
(112, NULL, 'lizmia ali', '+923313833315', 'alilizmia@gmail.com', 'ilovemyself', 0x3135333534343731333746425f494d475f313533353434363935323937362e6a7067),
(113, NULL, 'Nabeel Khurshid ', '03363975597', 'nabeelkhurshid95@gmail.com', 'naks1995', 0x3135333534343738363846425f494d475f313532373936393433383930372e6a7067),
(114, NULL, 'Umar Farooq', '03345348747', 'umerfarooq88@gmail.com', 'celerond0321', 0x3135333534343837343331322e6a7067),
(115, NULL, 'Umar Farooq', '03345348747', 'umerfarooq88@gmail.com', 'celerond0321', 0x3135333534343837363031322e6a7067),
(116, NULL, 'AhsanSaleem', '03435009416', 'chahsan18098@gmail.com', 'chahsanrix6016', 0x3135333534343839373770617373706f72742073697a652e6a7067),
(117, NULL, 'AhsanSaleem', '03435009416', 'chahsan18098@gmail.com', 'chahsanrix6016', 0x3135333534343839383370617373706f72742073697a652e6a7067),
(118, NULL, 'Akhmal M. Chaudhry', '03022533806', 'akhmalluqman050@gmail.com', 'Mother!234', 0x3135333534343933333243463831424544372d443339432d344144392d383233392d4646394239333135333534422e6a706567),
(119, NULL, 'Akhmal M. Chaudhry', '03022533806', 'akhmalluqman050@gmail.com', 'Mother!234', 0x3135333534343933353043463831424544372d443339432d344144392d383233392d4646394239333135333534422e6a706567),
(120, NULL, 'Akhmal M. Chaudhry', '03022533806', 'akhmalluqman050@gmail.com', 'Mother!234', 0x3135333534343933363243463831424544372d443339432d344144392d383233392d4646394239333135333534422e6a706567),
(121, NULL, 'Sadam Tariq Mughal', '03455080259', 'sadam.mughal2@gmail.com', 'goodboy.1', 0x3135333534343935303432303138303832385f3134323831332e6a7067),
(122, NULL, 'Syed Zain-ul-abdeen', '03337917679', 'zain21034@gmail.com', 'nokilat90', 0x31353335343439373730494d472d32303135303531322d5741303030322e6a7067),
(123, NULL, 'Hassan Ahmed Khan', '03318853336', 'Hassanahmedkhan92@gmail.com', 'manila12345', 0x313533353434393830366431372d333236302e6a7067),
(124, NULL, 'Syed Zain-ul-abdeen', '03337917679', 'zain21034@gmail.com', 'nokilat90', 0x31353335343439383230494d472d32303135303531322d5741303030322e6a7067),
(125, NULL, 'Hassan Ahmed Khan', '03318853336', 'Hassanahmedkhan92@gmail.com', 'manila12345', 0x313533353434393833366431372d333236302e6a7067),
(126, NULL, 'Syed Zain-ul-abdeen', '03337917679', 'zain21034@gmail.com', 'nokilat90', 0x31353335343439383431494d472d32303135303531322d5741303030322e6a7067),
(127, NULL, 'Syed Zain-ul-abdeen', '03337917679', 'zain21034@gmail.com', 'nokilat90', 0x31353335343439383632494d472d32303135303531322d5741303030322e6a7067),
(128, NULL, 'Syed Zain-ul-abdeen', '03337917679', 'zain21034@gmail.com', 'nokilat90', 0x31353335343530353134494d472d32303135303531322d5741303030322e6a7067),
(129, NULL, 'Hamza moeen', '03335152501', 'hamzasheikh101@gmail.com', '12345class', 0x31353335343530363833494d472d32303137303931302d5741303030302e6a7067),
(130, NULL, 'Hamza moeen', '03335152501', 'hamzasheikh101@gmail.com', '12345class', 0x31353335343530373931494d472d32303137303931302d5741303030302e6a7067),
(131, NULL, 'Danial Khalid', '03455218149', 'danialkhalid692@gmail.com', 'juji12345', 0x313533353435313837314e657720446f6320323031382d30362d3037202831295f312e6a7067),
(132, NULL, 'Danial Khalid', '03455218149', 'danialkhalid692@gmail.com', 'juji12345', 0x313533353435323031304e657720446f6320323031382d30362d3037202831295f312e6a7067),
(133, NULL, 'Danial Khalid', '03455218149', 'danialkhalid692@gmail.com', 'juji12345', 0x313533353435323036354e657720446f6320323031382d30362d3037202831295f312e6a7067),
(134, NULL, 'Danial Khalid', '03455218149', 'danialkhalid692@gmail.com', 'juji12345', 0x313533353435323135364e657720446f6320323031382d30362d3037202831295f312e6a7067),
(135, NULL, 'Danial Khalid', '03455218149', 'danialkhalid692@gmail.com', 'juji12345', 0x313533353435323136324e657720446f6320323031382d30362d3037202831295f312e6a7067),
(136, NULL, 'Muhammad Abdullah', '03135639555', 'ashar621@gmail.com', 'sparta62!', 0x313533353435323535334d20414244554c4c41482e6a7067),
(137, NULL, 'Syed Faizan', '03320547693', 'syed5564576@gmail.com', 'F5564576f%', 0x31353335343532353630494d475f32303138303832355f3135333930322e6a7067),
(138, NULL, 'Syed Faizan', '03320547693', 'syed5564576@gmail.com', 'F5564576f%', 0x31353335343532393035494d475f32303138303832355f3135333930322e6a7067),
(139, NULL, 'Syed Faizan', '03320547693', 'syed5564576@gmail.com', 'F5564576f%', 0x31353335343532393638494d475f32303138303832355f3135333930322e6a7067),
(140, NULL, 'Shahid Karim', '03420551738', 'shahidkarim784@gmail.com', 'skarim1452', 0x31353335343535313431494d475f313832302e4a5047),
(141, NULL, 'Shahid Karim', '03420551738', 'shahidkarim784@gmail.com', 'skarim1452', 0x31353335343535353031494d475f313832302e4a5047),
(142, NULL, 'Shahid Karim', '03420551738', 'shahidkarim784@gmail.com', 'skarim1452', 0x31353335343535383234494d475f313832302e4a5047),
(143, NULL, 'Syed Wajahat Ansar', '03315404672', 'syedwajahat395@gmail.com', '120991', 0x3135333534353731373833306b622e6a7067),
(144, NULL, 'Syed Wajahat Ansar', '03315404672', 'syedwajahat395@gmail.com', '120991', 0x3135333534353731383533306b622e6a7067),
(145, NULL, 'Saad Iftikhar Khawaja ', '0334-5324534', 'saad-khawaja@hotmail.com', 'crackh3adx', 0x31353335343537353930706963747572652e6a7067),
(146, NULL, 'Saad Iftikhar Khawaja ', '0334-5324534', 'saad-khawaja@hotmail.com', 'crackh3adx', 0x31353335343537363332706963747572652e6a7067),
(147, NULL, 'Kashif IQBAL', '03361046411', 'rajakashif18099@outlook.com', 'Pakistan123', 0x313533353435373638346b61736869662e6a7067),
(148, NULL, 'Saad Iftikhar Khawaja ', '0334-5324534', 'saad-khawaja@hotmail.com', 'crackh3adx', 0x31353335343537383737706963747572652e6a7067),
(149, NULL, 'Saad Iftikhar Khawaja ', '0334-5324534', 'saad-khawaja@hotmail.com', 'crackh3adx', 0x31353335343538303339706963747572652e6a7067),
(150, NULL, 'Saad Iftikhar Khawaja ', '0334-5324534', 'saad-khawaja@hotmail.com', 'crackh3adx', 0x31353335343538303831706963747572652e6a7067),
(151, NULL, 'Saad Iftikhar Khawaja ', '0334-5324534', 'saad-khawaja@hotmail.com', 'crackh3adx', 0x31353335343538313531706963747572652e6a7067),
(152, NULL, 'Saad Iftikhar Khawaja ', '0334-5324534', 'saad-khawaja@hotmail.com', 'crackh3adx', 0x31353335343538333032706963747572652e6a7067),
(153, NULL, 'Mariam', '03340296321', 'mariam.kh61@gmail.com', '12345kh', 0x3135333534363232313031353335343632313235353832383539363431323737373639393032383935322e6a7067),
(154, NULL, 'Saaim khan ', '03340588888', 'saaimkhan21@gmail.com', 'dogaan123', 0x3135333534363436323741303039333237392d423646382d343739322d414139352d4535343941384234423843462e6a706567),
(155, NULL, 'Saaim khan ', '03340588888', 'saaimkhan21@gmail.com', 'dogaan123', 0x3135333534363436363941303039333237392d423646382d343739322d414139352d4535343941384234423843462e6a706567),
(156, NULL, 'Muhammad Usman Khan', '03325951979', 'itsusmankhan853@gmail.com', 'Rizwan711', 0x3135333534363437363746333031384446462d434241412d344238302d393636452d3744364646414134393042312e6a706567),
(157, NULL, 'Haziq owais', '03346519959', 'haziq.awais@gmail.com', 'kharalbkr@', 0x3135333534363934343632303138303832385f3230313530352e6a7067),
(158, NULL, 'Haziq owais', '03346519959', 'haziq.awais@gmail.com', 'kharalbkr@', 0x3135333534363936343232303138303832385f3230313530352e6a7067),
(159, NULL, 'Haziq owais', '03346519959', 'haziq.awais@gmail.com', 'kharalbkr@', 0x3135333534363936353732303138303832385f3230313530352e6a7067),
(160, NULL, 'Haziq owais', '03346519959', 'haziq.awais@gmail.com', 'kharalbkr@', 0x3135333534363936373432303138303832385f3230313530352e6a7067),
(161, NULL, 'Haziq owais', '03346519959', 'haziq.awais@gmail.com', 'kharalbkr@', 0x3135333534363937303532303138303832385f3230313530352e6a7067),
(162, NULL, 'yasir javed', '03361556805', 'yasirjaved2007@gmail.com', 'yasir4500', 0x3135333534373039383379617369722e6a706567),
(163, NULL, 'yasir javed', '03361556805', 'yasirjaved2007@gmail.com', 'yasir4500', 0x3135333534373130313079617369722e6a706567),
(164, NULL, 'yasir javed', '03361556805', 'yasirjaved2007@gmail.com', 'yasir4500', 0x3135333534373130373079617369722e6a706567),
(165, NULL, 'Atif Bangash', '03065008025', 'muhammadatif366@gmail.com', 'ATif@12349$', 0x3135333534373232343946425f494d475f313533303538363132393737362e6a7067),
(166, NULL, 'Atif Bangash', '03065008025', 'muhammadatif366@gmail.com', 'ATif@12349$', 0x3135333534373233303546425f494d475f313533303538363132393737362e6a7067),
(167, NULL, 'Atif Bangash', '03065008025', 'muhammadatif366@gmail.com', 'ATif@12349$', 0x3135333534373233333446425f494d475f313533303538363132393737362e6a7067),
(168, NULL, 'Atif Bangash', '03065008025', 'muhammadatif366@gmail.com', 'ATif@12349$', 0x3135333534373233333546425f494d475f313533303538363132393737362e6a7067),
(169, NULL, 'Junaid ur Rehman', '03335980090', 'junaid.rehman@ufone.com', '32zifda11', 0x3135333534373530323831333538332d4a756e616964207572205265686d616e2e6a7067),
(170, NULL, 'Junaid ur Rehman', '03335980090', 'junaid.rehman@ufone.com', '32zifda11', 0x3135333534373530333731333538332d4a756e616964207572205265686d616e2e6a7067),
(171, NULL, 'Junaid ur Rehman', '03335980090', 'jd.reh11@gmail.com', '32zifda11', 0x3135333534373531303131333538332d4a756e616964207572205265686d616e2e6a7067),
(172, NULL, 'Asif Ahmad', '03339886220', 'asifahmadbacha@gmail.com', 'iqraisb1', 0x313533353437353534384173696620526573756d652e646f63),
(173, NULL, 'Asif Ahmad', '03339886220', 'asifahmadbacha@gmail.com', '', 0x313533353437353837364173696620706963747572652e6a7067),
(174, NULL, 'Asif Ahmad', '03339886220', 'asifahmadbacha@gmail.com', '', 0x313533353437353939304173696620706963747572652e6a7067),
(175, NULL, 'Asif Ahmad', '03339886220', 'asifahmadbacha@gmail.com', '', 0x313533353437363031314173696620706963747572652e6a7067),
(176, NULL, 'Asif Ahmad', '03339886220', 'asifahmadbacha@gmail.com', '', 0x313533353437363033314173696620706963747572652e6a7067),
(177, NULL, 'Muhammad Ayan', '03329623297', 'ayankhn67@gmail.com', 'afridi12345', 0x313533353438313236344d7568616d6d6164204179616e2d325f3332392e646f6378),
(178, NULL, 'Muhammad Ayan', '03329623297', 'ayankhn67@gmail.com', 'afridi12345', 0x313533353438313239314d7568616d6d6164204179616e2d325f3332392e646f6378),
(179, NULL, 'Muhammad Ayan', '03329623297', 'ayankhn67@gmail.com', 'afridi12345', 0x313533353438313332364d7568616d6d6164204179616e2d325f3332392e646f6378),
(180, NULL, 'Muhammad Ayan', '03329623297', 'ayankhn67@gmail.com', 'afridi12345', 0x313533353438313332374d7568616d6d6164204179616e2d325f3332392e646f6378),
(181, NULL, 'Muhammad Ayan', '03329623297', 'ayankhn67@gmail.com', 'afridi12345', 0x313533353438313431354d7568616d6d6164204179616e2d325f3332392e646f6378),
(182, NULL, 'Muhammad Ayan', '03329623297', 'ayankhn67@gmail.com', 'afridi12345', 0x313533353438313431374d7568616d6d6164204179616e2d325f3332392e646f6378),
(183, NULL, 'Muhammad Ayan', '03329623297', 'ayankhn67@gmail.com', 'afridi12345', 0x313533353438313431384d7568616d6d6164204179616e2d325f3332392e646f6378),
(184, NULL, 'Muhammad Ayan', '03329623297', 'ayankhn67@gmail.com', 'afridi12345', 0x313533353438313432314d7568616d6d6164204179616e2d325f3332392e646f6378),
(185, NULL, 'Muhammad Ayan', '03329623297', 'ayankhn67@gmail.com', 'afridi12345', 0x313533353438313432314d7568616d6d6164204179616e2d325f3332392e646f6378),
(186, NULL, 'Muhammad Ayan', '03329623297', 'ayankhn67@gmail.com', 'afridi12345', 0x313533353438313432394d7568616d6d6164204179616e2d325f3332392e646f6378),
(187, NULL, 'Moteeb Asad', '03026144056', 'moteeb83@gmail.com', 'moteeb15810', 0x31353335343837313633626c61636b2e706e67),
(188, NULL, 'Usman Ahmed', '03222362222', 'ussmanmani@gmail.com', 'Mani03222362222', 0x313533353439343938315f445343303339336e2e6a7067),
(189, NULL, 'Usman Ahmed', '03222362222', 'ussmanmani@gmail.com', 'Mani03222362222', 0x313533353439353032335f445343303339336e2e6a7067),
(190, NULL, 'Muhammad Mounas Samim', '03244849424', 'munasusman@gmail.com', 'Mounas4849424', 0x3135333535313435343431323433353839375f3934393939353436383432383531385f313133353337343836315f6e2e6a7067),
(191, NULL, 'Shahid Karim', '03420551738', 'shahidkarim784@gmail.com', 'skarim1452', 0x31353335353139393536494d475f313832302e4a5047),
(192, NULL, 'Muhammad Daaniyal', '03336546454', 'muhammad.daaniyal7@gmail.com', 'koja1808', 0x313533353532303433324d7568616d6d6164204461616e6979616c2e6a7067),
(193, NULL, 'Muhammad Daaniyal', '03336546454', 'muhammad.daaniyal7@gmail.com', 'marlboro150', 0x313533353532303436324d7568616d6d6164204461616e6979616c2e6a7067),
(194, NULL, 'faizan hafeez malik', '03315096097', 'faizan.hafeez.malik@gmail.com', 'st.lucia*', 0x3135333535333035353963762e4a5047),
(195, NULL, 'Ubaid ur Rehman Khan', '0322-5296426', 'murkniazi@gmail.com', 'ubaidniazi', 0x3135333535333333373053637265656e73686f745f323031382d30382d32392d31342d30312d34392e706e67),
(196, NULL, 'Haziq Owais', '03346519959', 'haziq.awais@gmail.com', 'kharalbkr@', 0x3135333535333532383346425f494d475f313533353437323130303639302e6a7067),
(197, NULL, 'Haziq Owais', '03346519959', 'haziq.awais@gmail.com', 'kharalbkr@', 0x3135333535333533313446425f494d475f313533353437323130303639302e6a7067),
(198, NULL, 'Haziq owais', '03346519959', 'haziq.awais@gmail.com', 'kharalbkr@', 0x3135333535333538313046425f494d475f313533353437323130303639302e6a7067),
(199, NULL, 'Hina', '03331348719', 'hinamba8@gmail.com', 'mypassion123', 0x31353335353431393932696d616765732e6a7067),
(200, NULL, 'Hina', '03331348719', 'hinamba8@gmail.com', 'mypassion123', 0x31353335353432303835696d616765732e6a7067),
(201, NULL, 'M raheel rahat', '03355136251', 'raheelrahat@hotmail.com', 'raheel987', 0x3135333535343538343531353339313034345f313235303335393539383333363037305f333835333837353336323535333530383030385f6e2e6a7067),
(202, NULL, 'M raheel rahat', '03355136251', 'raheelrahat@hotmail.com', 'raheel987', 0x3135333535343539323331353339313034345f313235303335393539383333363037305f333835333837353336323535333530383030385f6e2e6a7067),
(203, NULL, 'hani', '03155311858', 'hanifaisal294@gmail.com', 'inocentheart5294', 0x3135333535353332373531363437333034365f313434383731313630313831393835325f393034323837333232363836363339373539325f6e2e6a7067),
(204, NULL, 'hani', '03155311858', 'hanifaisal294@gmail.com', 'inocentheart5294', 0x3135333535353333383831363437333034365f313434383731313630313831393835325f393034323837333232363836363339373539325f6e2e6a7067),
(205, NULL, 'Husnain Abid', '03005373434', 'husnain_702@hotmail.com', '123456', 0x313533353535353432364875736e61696e2041626964203035312d31322d3131373633302020526573756d652e646f6378),
(206, NULL, 'Husnain Abid', '03005373434', 'husnain_702@hotmail.com', '123456', 0x313533353535353433364875736e61696e2041626964203035312d31322d3131373633302020526573756d652e646f6378),
(207, NULL, 'faris fahim', '03025225498', 'farisfahim1@yahoo.com', 'Farisfahim1', 0x3135333535353536313733323733373037345f313834363634303532323035323638355f333435373836393735313837333639393834305f6e2e6a7067),
(208, NULL, 'faris fahim', '03025225498', 'farisfahim1@yahoo.com', 'Farisfahim1', 0x3135333535353536333033323733373037345f313834363634303532323035323638355f333435373836393735313837333639393834305f6e2e6a7067),
(209, NULL, 'faris fahim', '03025225498', 'farisfahim1@yahoo.com', 'Farisfahim1', 0x3135333535353536383133323733373037345f313834363634303532323035323638355f333435373836393735313837333639393834305f6e2e6a7067),
(210, NULL, 'Sabeen Ashfaq', '03335538171', 'sabeen_ashfaq@ymail.com', 'iqra2012', 0x31353335353631333937313533353332323130373134372e6a7067),
(211, NULL, 'Sabeen Ashfaq', '03335538171', 'sabeen_ashfaq@ymail.com', 'iqra2012', 0x31353335353631353335313533353332323130373134372e6a7067),
(212, NULL, 'Ali Asghar Sohail', '03366631352', 'ali112008@live.com', 'ali112008', 0x313533353536333838313030312e6a7067),
(213, NULL, 'Ali Asghar Sohail', '03366631352', 'ali112008@live.com', 'ali112008', 0x313533353536333930313030312e6a7067),
(214, NULL, 'Ali Asghar Sohail', '03366631352', 'ali112008@live.com', 'ali112008', 0x313533353536333933343030312e6a7067),
(215, NULL, 'Ali Asghar Sohail', '03366631352', 'ali112008@live.com', 'ali112008', 0x313533353536333935333030312e6a7067),
(216, NULL, 'Ali Asghar Sohail', '03366631352', 'ali112008@live.com', 'ali112008', 0x313533353536333935373030312e6a7067),
(217, NULL, 'Iftikhar ali', '03469317137', 'iftikhar_ali32@yahoo.com', 'greatali555', 0x3135333535363730373246425f494d475f313533353536373133343635352e6a7067),
(218, NULL, 'Iftikhar ali', '03469317137', 'iftikhar_ali32@yahoo.com', 'greatali555', 0x3135333535363730393546425f494d475f313533353536373133343635352e6a7067),
(219, NULL, 'Mobina Malik ', '03315551610', 'mobinam10@gmail.com', 'CHANGEPASSWORD789!', 0x3135333535383237343030313141373141342d413932412d344545362d424442302d4441354236303837373736362e6a706567),
(220, NULL, 'Mobina Malik ', '03315551610', 'mobinam10@gmail.com', 'CHANGEPASSWORD789!', 0x3135333535383237353030313141373141342d413932412d344545362d424442302d4441354236303837373736362e6a706567),
(221, NULL, 'Mobina Malik ', '03315551610', 'mobinam10@gmail.com', 'CHANGEPASSWORD789!', 0x3135333535383237373730313141373141342d413932412d344545362d424442302d4441354236303837373736362e6a706567),
(222, NULL, 'Muhammad Kashif Hayat Khan', '03458561945', 'kashifhayat1n1@hotmail.com', 'kl5ru793', 0x3135333536303336313954617377656572202e6a7067),
(223, NULL, 'Sheeraz waheed', '03007500041', 'sherazawan001@gmail.com', 'malikawan001', 0x3135333536313531373073656c66696563616d6572615f323031382d30342d32322d31342d32362d33342d3330372e6a7067),
(224, NULL, 'Sheeraz waheed', '03007500041', 'sherazawan001@gmail.com', 'malikawan001', 0x3135333536313532343273656c66696563616d6572615f323031382d30342d32322d31342d32362d33342d3330372e6a7067),
(225, NULL, 'Moiz Ur Rehman', '0331-2312529', 'moizurrehman23322@gmail.com', 'escobar 12', 0x3135333536313532383631353434333135335f31303231313330343933323332393130305f353032393733343133353637313132323932395f6f2e6a7067),
(226, NULL, 'Sheeraz waheed', '03007500041', 'sherazawan001@gmail.com', 'malikawan001', 0x3135333536313532393173656c66696563616d6572615f323031382d30342d32322d31342d32362d33342d3330372e6a7067),
(227, NULL, 'Moiz Ur Rehman', '0331-2312529', 'moizurrehman23322@gmail.com', 'escobar 12', 0x3135333536313533373031353434333135335f31303231313330343933323332393130305f353032393733343133353637313132323932395f6f2e6a7067),
(228, NULL, 'Sheeraz waheed', '03007500041', 'sherazawan001@gmail.com', 'malikawan001', 0x3135333536313533383373656c66696563616d6572615f323031382d30342d32322d31342d32362d33342d3330372e6a7067),
(229, NULL, 'Moiz Ur Rehman', '0331-2312529', 'moizurrehman23322@gmail.com', 'escobar 12', 0x3135333536313535343531353434333135335f31303231313330343933323332393130305f353032393733343133353637313132323932395f6f2e6a7067),
(230, NULL, 'Moiz Ur Rehman', '0331-2312529', 'moizurrehman23322@gmail.com', 'escobar 12', 0x3135333536313535393331353434333135335f31303231313330343933323332393130305f353032393733343133353637313132323932395f6f2e6a7067),
(231, NULL, 'Sheeraz waheed', '03007500041', 'sherazawan001@gmail.com', 'malikawan001', 0x3135333536313536313873656c66696563616d6572615f323031382d30342d32322d31342d32362d33342d3330372e6a7067),
(232, NULL, 'Moiz Ur Rehman', '0331-2312529', 'moizurrehman23322@gmail.com', 'escobar 12', 0x3135333536313536323431353434333135335f31303231313330343933323332393130305f353032393733343133353637313132323932395f6f2e6a7067),
(233, NULL, 'Moiz Ur Rehman', '0331-2312529', 'moizurrehman23322@gmail.com', 'escobar 12', 0x3135333536313536353731353434333135335f31303231313330343933323332393130305f353032393733343133353637313132323932395f6f2e6a7067),
(234, NULL, 'Moiz Ur Rehman', '0331-2312529', 'moizurrehman23322@gmail.com', 'escobar 12', 0x3135333536313536393731353434333135335f31303231313330343933323332393130305f353032393733343133353637313132323932395f6f2e6a7067),
(235, NULL, 'Moiz Ur Rehman', '0331-2312529', 'moizurrehman23322@gmail.com', 'escobar 12', 0x3135333536313633333431353434333135335f31303231313330343933323332393130305f353032393733343133353637313132323932395f6f2e6a7067),
(236, NULL, 'Moiz Ur Rehman', '0331-2312529', 'moizurrehman23322@gmail.com', 'escobar 12', 0x3135333536313634333031353434333135335f31303231313330343933323332393130305f353032393733343133353637313132323932395f6f2e6a7067),
(237, NULL, 'Moiz Ur Rehman', '0331-2312529', 'moizurrehman23322@gmail.com', 'escobar 12', 0x3135333536313634373831353434333135335f31303231313330343933323332393130305f353032393733343133353637313132323932395f6f2e6a7067),
(238, NULL, 'Mobina Malik ', '03315551610', 'mobinam10@gmail.com', 'CHANGEPASSWORD789!', 0x3135333536353536373430313141373141342d413932412d344545362d424442302d4441354236303837373736362e6a706567),
(239, NULL, 'Zaiban', '03335025638', 'zaibankhan001@gmail.com', 'iqrauni.com', 0x31353335373437353636494d475f32303138303532305f3233343133335f3633362e6a7067),
(240, NULL, 'Nagza naveed', '03435551692', 'nagza91@gmail.com', 'wailay@gr8', 0x31353335373837393830323031362d30392d31332d31372d34352d30322d3738382e6a7067),
(241, NULL, 'Nagza naveed', '03435551692', 'nagza91@gmail.com', 'wailay@gr8', 0x31353335373838303039323031362d30392d31332d31372d34352d30322d3738382e6a7067),
(242, NULL, 'Muhammad Ayan', '03329623297', 'ayankhn67@gmail.com', 'iqra560601', 0x313533353830333234354d7568616d6d6164204179616e2d325f3332392e646f6378),
(243, NULL, 'Muhammad Ayan', '03329623297', 'ayankhn67@gmail.com', 'iqra560601', 0x313533353830333237394d7568616d6d6164204179616e2d325f3332392e646f6378),
(244, NULL, 'Hasan Qazi', '03455893338', 'hasanqazi36@gmail.com', '03455302104', 0x3135333538333338313231383935333033335f31303135353433353332343337343436385f373533363835373831323136373132353838355f6e2e6a7067),
(245, NULL, 'Hasan Qazi', '03455893338', 'hasanqazi36@gmail.com', '03455302104', 0x3135333538333338333031383935333033335f31303135353433353332343337343436385f373533363835373831323136373132353838355f6e2e6a7067),
(246, NULL, 'Muhammad Adnan Khan', '+923155717774', 'Adnan_Laghari@live.com', 'adnan22273', 0x3135333538373731343346696c65303034302e4a5047),
(247, NULL, 'Muhammad Adnan Khan', '+923155717774', 'Adnan_Laghari@live.com', 'adnan22273', 0x3135333538373732363246696c65303034302e4a5047),
(248, NULL, 'Muhammad Adnan Khan', '+923155717774', 'Adnan_Laghari@live.com', 'adnan22273', 0x3135333538373735333146696c65303034302e4a5047),
(249, NULL, 'Muhammad Aaqib Javed Kayani', '0343-1211290', 'aaqibjaved864@gmail.com', 'australia66', 0x31353335383930373236415149422053554e4e592e6a7067),
(250, NULL, 'Muhammad Aaqib Javed Kayani', '0343-1211290', 'aaqibjaved864@gmail.com', 'australia66', 0x31353335383930373531415149422053554e4e592e6a7067),
(251, NULL, 'Muhammad Aaqib Javed Kayani', '0343-1211290', 'aaqibjaved864@gmail.com', 'australia66', 0x31353335383930373731415149422053554e4e592e6a7067),
(252, NULL, 'Muhammad Aaqib Javed Kayani', '0343-1211290', 'aaqibjaved864@gmail.com', 'australia66', 0x31353335383930383138415149422053554e4e592e6a7067),
(253, NULL, 'Muhammad Abdullah', '03318623840', 'abdullahmalik3840@gmail.com', 'iqraisb0808', 0x3135333539303937303032303138303832375f3230313834372e6a7067),
(254, NULL, 'Muhammad Abdullah', '03318623840', 'abdullahmalik3840@gmail.com', 'iqraisb0808', 0x3135333539303937303732303138303832375f3230313834372e6a7067),
(255, NULL, 'Muhammad Abdullah', '03318623840', 'abdullahmalik3840@gmail.com', 'iqraisb0808', 0x3135333539303937363132303138303832375f3230313834372e6a7067),
(256, NULL, 'Nafees Kamal', '03135336214', 'nafi.kamalpk@gmail.com', 'k@mal123456', 0x31353335393738313434494d472d32303138303832342d574130303234),
(257, NULL, 'Nafees Kamal', '03135336214', 'nafi.kamalpk@gmail.com', 'k@mal123456', 0x31353335393738323039494d472d32303138303832342d574130303234),
(258, NULL, 'Nafees Kamal', '03135336214', 'nafi.kamalpk@gmail.com', 'k@mal123456', 0x31353335393738323236494d472d32303138303832342d574130303234),
(259, NULL, 'Komal maryam ', '03101515176', 'komalmaryam4@gmail.com', 'komalmaryam4', 0x31353336313537363333494d472d32303138303930352d5741303030312e6a7067),
(260, NULL, 'Muhammad Mounas Samim', '03244849424', 'munasusman@gmail.com', 'Mounas@4849424', 0x313533363230373534306d652e6a7067),
(261, NULL, 'Muhammad Mounas Samim', '03244849424', 'munasusman@gmail.com', 'Mounas@4849424', 0x313533363230383734366d652e6a7067),
(262, NULL, 'Muhammad Mounas Samim', '03244849424', 'munasusman@gmail.com', 'Mounas@4849424', 0x313533363231333934346d652e6a7067),
(263, NULL, 'PARIS SAFDAR', '03435380628', 'parissafdar@gmail.com', '5456810', 0x31353336333138383239352e6a7067),
(264, NULL, 'Shahrayar khalid', '03335811446', 'sherryjerry449@gmail.com', 'rtgh27', 0x31353336333832303434536e6170636861742d313539363232343738392e6a7067),
(265, NULL, 'Shahrayar khalid', '03335811446', 'sherryjerry449@gmail.com', 'rtgh27', 0x31353336333832303634536e6170636861742d313539363232343738392e6a7067),
(266, NULL, 'Muhammad Hussain', '03453783492', 'hussain.changazi91@gmail.com', 'vbwjxj13', 0x31353336353133373337494d472d32303137303332342d5741303030302e6a7067),
(267, NULL, 'Muhammad Shafi', '03156444528', 'shafiihanif@gmail.com', '24203shafi', 0x3135333636303930303331303638363539355f313532333039383135343539393237395f343933313832313334303231353532383534335f6e2e6a7067),
(268, NULL, 'Muhammad Shafi', '03156444528', 'shafiihanif@gmail.com', '24203shafi', 0x3135333636303931303831303638363539355f313532333039383135343539393237395f343933313832313334303231353532383534335f6e2e6a7067),
(269, NULL, 'Admin', '03173820550', 'mysite@gmail.com', 'name.13SW', 0x3135333636353230343732363835303632315f313630313738373632333233383233335f343436373434333934343030313232373137385f6f2e6a7067),
(270, NULL, 'nosshen', '03330111232', 'nosheen@gmail.com', 'nosheen123', 0x3135333636353236363131612e6a7067),
(271, NULL, 'Zeeshan Farooq', '03315263072', 'zeeshanfarooq46@gmail.com', 'guru007', 0x313533363836313438365a65657368616e2043562e646f6378),
(272, NULL, 'Waleed Ali Khan', '03219004107', 'waleedalikahn@gmail.com', 'qwerty123', 0x3135333731303733373050686f746f2e6a7067),
(273, NULL, 'fatima Naz', '03337917127', 'fatima_naz1994@yahoo.com', 'Myiqra1994', 0x3135333835323738353536363331362e6a7067),
(274, NULL, 'fatima Naz', '03337917127', 'fatima_naz1994@yahoo.com', 'Myiqra1994', 0x3135333835323739363736363331362e6a7067),
(275, NULL, 'Hamza ', '03333333333', 'm.hamzanajeeb@gmail.com', '123', 0x31353339333339383536696d616765732e6a666966),
(276, NULL, 'Saleha Saud', '03339560560', 'salehasaud92@gmail.com', 'kawasaki91', 0x313534303031383132315053502e4a5047),
(277, NULL, 'Saleha Saud', '03339560560', 'salehasaud92@gmail.com', 'kawasaki91', 0x313534303031383134385053502e4a5047),
(278, NULL, 'Saleha Saud', '03339560560', 'salehasaud92@gmail.com', 'kawasaki91', 0x313534303031383137365053502e4a5047),
(279, NULL, 'Saleha Saud', '03339560560', 'salehasaud92@gmail.com', 'kawasaki91', 0x313534303032303532395053502e4a5047),
(280, NULL, 'Ali', '03173820550', 'kamran@gmail.com', '123', 0x313534313438393834353161692e6a7067);

-- --------------------------------------------------------

--
-- Table structure for table `role`
--

CREATE TABLE `role` (
  `role_id` int(11) NOT NULL,
  `role_name` varchar(200) DEFAULT NULL,
  `role_status` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `skills_tb`
--

CREATE TABLE `skills_tb` (
  `skill_id` int(11) NOT NULL,
  `master_id` int(11) DEFAULT NULL,
  `skill_name` varchar(200) DEFAULT NULL,
  `skill_percentage` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `skills_tb`
--

INSERT INTO `skills_tb` (`skill_id`, `master_id`, `skill_name`, `skill_percentage`) VALUES
(4, 70, 'BootStrap', '60'),
(5, 70, 'HTML', '70'),
(6, 70, 'CSS', '80'),
(7, 78, 'ASP.NET MVC', '100'),
(8, 78, 'JQUERY/JavaScript', '93'),
(9, 78, 'Angular 2', '81'),
(10, 78, 'SEO', '88'),
(11, 78, 'Photoshop', '100'),
(12, 82, 'HTML', '90'),
(13, 82, '', '50'),
(14, 82, '', '50'),
(15, 82, '', '50'),
(16, 82, '', '50'),
(17, 79, 'PHP MVC Codeigniter', '50'),
(18, 79, 'HTML5', '50'),
(19, 79, 'CSS3', '50'),
(20, 79, 'Bootstrap 4', '50'),
(21, 79, 'Jquery', '50'),
(22, 79, 'AJAX', '50'),
(23, 79, 'JSON', '50'),
(24, 79, 'API', '50'),
(25, 81, 'Photography', '85'),
(26, 81, 'Content Writing', '81'),
(27, 81, 'Team Organization', '93'),
(28, 81, 'Hard work', '100'),
(29, 81, '', '50'),
(30, 80, 'ASP.NET MVC', '50'),
(31, 80, 'HTML', '50'),
(32, 80, 'CSS', '50'),
(33, 80, 'Javascript', '33'),
(34, 80, 'PHP', '41'),
(35, 80, 'MSSQL', '50'),
(36, 80, 'MYSQL', '50'),
(37, 83, 'MS office', '82'),
(38, 83, '', '50'),
(39, 83, '', '50'),
(40, 83, '', '50'),
(41, 83, '', '50'),
(42, NULL, 'Programming C#', '91'),
(43, NULL, 'Programming android ', '90'),
(44, NULL, 'Administrative work', '91'),
(45, NULL, 'Managging team', '80'),
(46, NULL, 'Problem solving/strategic view', '70'),
(47, NULL, 'MS offive', '50'),
(48, NULL, 'Able to multitask', '55'),
(49, NULL, '', '50'),
(50, NULL, '', '50'),
(51, NULL, '', '50'),
(52, 84, '', '50'),
(53, 84, '', '50'),
(54, 84, '', '50'),
(55, 84, '', '50'),
(56, 84, 'English', '80'),
(57, 84, 'Urdu', '90'),
(58, 84, 'C++', '50'),
(59, 84, '', '50'),
(60, 84, '', '50'),
(61, 86, 'Computer ', '70'),
(62, 86, 'Communication ', '60'),
(63, 86, '', '50'),
(64, 86, '', '50'),
(65, 86, '', '50'),
(66, NULL, 'Web Development ', '88'),
(67, NULL, 'Firebase database storage authentication ', '73'),
(68, NULL, 'Html css', '71'),
(69, NULL, 'C# ', '60'),
(70, NULL, 'C++', '18'),
(71, NULL, 'Unity 3d', '50'),
(72, NULL, '', '50'),
(73, 88, 'Web Designing', '90'),
(74, 88, 'Web Development', '90'),
(75, 88, 'Graphic Designing', '75'),
(76, 88, 'Programming', '75'),
(77, 88, '', '50'),
(78, NULL, 'keyboard skills', '84'),
(79, NULL, 'MS Office.', '80'),
(80, NULL, 'SPSS use', '60'),
(81, NULL, 'Work individually', '80'),
(82, NULL, 'Communication Skills', '90'),
(83, 92, 'rer', '50'),
(84, 92, 'etrsf', '50'),
(85, 92, 'gdrg', '50'),
(86, 92, 'fdgdfg', '50'),
(87, 92, 'dfg', '50'),
(88, NULL, 'keyboard skills', '80'),
(89, NULL, 'Ms Office', '80'),
(90, NULL, 'Interpersonal ', '85'),
(91, NULL, 'Working in Group', '90'),
(92, NULL, 'Communication ', '90'),
(93, 95, 'C#', '43'),
(94, 95, 'html', '100'),
(95, 95, 'Css', '50'),
(96, 95, 'Java', '50'),
(97, 95, 'JavaScript', '50'),
(98, 95, 'Wordpress', '50'),
(99, 95, 'C', '50'),
(100, 96, '', '50'),
(101, 96, '', '50'),
(102, 96, '', '50'),
(103, 96, '', '50'),
(104, NULL, 'Communication skills', '86'),
(105, NULL, 'Learning', '90'),
(106, NULL, 'Perfection', '84'),
(107, NULL, 'Honesty', '100'),
(108, NULL, 'Personality', '92'),
(109, 98, '', '50'),
(110, 98, '', '50'),
(111, 98, '', '50'),
(112, 98, '', '50'),
(113, 98, '', '50'),
(114, 98, 'Communication skills', '86'),
(115, 98, 'Learning', '100'),
(116, 98, 'Honesty', '100'),
(117, 98, 'Confidense', '93'),
(118, 98, 'Group head working', '100');

-- --------------------------------------------------------

--
-- Table structure for table `subtitle_table`
--

CREATE TABLE `subtitle_table` (
  `subtitle_id` int(11) NOT NULL,
  `title_id` int(11) DEFAULT NULL,
  `subtitle_name` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `subtitle_table`
--

INSERT INTO `subtitle_table` (`subtitle_id`, `title_id`, `subtitle_name`) VALUES
(1, 1, 'PHP'),
(2, 1, 'Database'),
(3, 2, 'Software'),
(4, 1, 'Java'),
(5, 2, 'SEO Enginner');

-- --------------------------------------------------------

--
-- Table structure for table `title_table`
--

CREATE TABLE `title_table` (
  `title_id` int(11) NOT NULL,
  `title_name` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `title_table`
--

INSERT INTO `title_table` (`title_id`, `title_name`) VALUES
(0, NULL),
(1, 'Developer'),
(2, 'PHP Expert Developer');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `certi_tb`
--
ALTER TABLE `certi_tb`
  ADD PRIMARY KEY (`certi_id`);

--
-- Indexes for table `dapartment`
--
ALTER TABLE `dapartment`
  ADD PRIMARY KEY (`dapt_id`);

--
-- Indexes for table `edu_tb`
--
ALTER TABLE `edu_tb`
  ADD PRIMARY KEY (`edu_id`),
  ADD KEY `edu_id` (`edu_id`);

--
-- Indexes for table `exp_tb`
--
ALTER TABLE `exp_tb`
  ADD PRIMARY KEY (`exp_id`);

--
-- Indexes for table `hobby_tb`
--
ALTER TABLE `hobby_tb`
  ADD PRIMARY KEY (`hobby_id`);

--
-- Indexes for table `lang_tb`
--
ALTER TABLE `lang_tb`
  ADD PRIMARY KEY (`lang_id`);

--
-- Indexes for table `level_table`
--
ALTER TABLE `level_table`
  ADD PRIMARY KEY (`level_id`);

--
-- Indexes for table `master_tb`
--
ALTER TABLE `master_tb`
  ADD PRIMARY KEY (`master_id`),
  ADD UNIQUE KEY `ms_expertise` (`master_id`),
  ADD UNIQUE KEY `master_id` (`master_id`,`reg_id`);

--
-- Indexes for table `program`
--
ALTER TABLE `program`
  ADD PRIMARY KEY (`program_id`);

--
-- Indexes for table `ref_tb`
--
ALTER TABLE `ref_tb`
  ADD PRIMARY KEY (`ref_id`);

--
-- Indexes for table `registeration`
--
ALTER TABLE `registeration`
  ADD PRIMARY KEY (`reg_id`);

--
-- Indexes for table `role`
--
ALTER TABLE `role`
  ADD PRIMARY KEY (`role_id`);

--
-- Indexes for table `skills_tb`
--
ALTER TABLE `skills_tb`
  ADD PRIMARY KEY (`skill_id`);

--
-- Indexes for table `subtitle_table`
--
ALTER TABLE `subtitle_table`
  ADD PRIMARY KEY (`subtitle_id`);

--
-- Indexes for table `title_table`
--
ALTER TABLE `title_table`
  ADD PRIMARY KEY (`title_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `certi_tb`
--
ALTER TABLE `certi_tb`
  MODIFY `certi_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `dapartment`
--
ALTER TABLE `dapartment`
  MODIFY `dapt_id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `edu_tb`
--
ALTER TABLE `edu_tb`
  MODIFY `edu_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=101;

--
-- AUTO_INCREMENT for table `exp_tb`
--
ALTER TABLE `exp_tb`
  MODIFY `exp_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;

--
-- AUTO_INCREMENT for table `hobby_tb`
--
ALTER TABLE `hobby_tb`
  MODIFY `hobby_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=65;

--
-- AUTO_INCREMENT for table `lang_tb`
--
ALTER TABLE `lang_tb`
  MODIFY `lang_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=175;

--
-- AUTO_INCREMENT for table `level_table`
--
ALTER TABLE `level_table`
  MODIFY `level_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `master_tb`
--
ALTER TABLE `master_tb`
  MODIFY `master_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=104;

--
-- AUTO_INCREMENT for table `program`
--
ALTER TABLE `program`
  MODIFY `program_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `ref_tb`
--
ALTER TABLE `ref_tb`
  MODIFY `ref_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `registeration`
--
ALTER TABLE `registeration`
  MODIFY `reg_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=281;

--
-- AUTO_INCREMENT for table `role`
--
ALTER TABLE `role`
  MODIFY `role_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `skills_tb`
--
ALTER TABLE `skills_tb`
  MODIFY `skill_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=119;

--
-- AUTO_INCREMENT for table `subtitle_table`
--
ALTER TABLE `subtitle_table`
  MODIFY `subtitle_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `title_table`
--
ALTER TABLE `title_table`
  MODIFY `title_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
