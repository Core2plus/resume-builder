/*
SQLyog Ultimate v12.2.6 (32 bit)
MySQL - 10.1.36-MariaDB : Database - myu
*********************************************************************
*/


/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`myu` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `myu`;

/*Table structure for table `certi_tb` */

DROP TABLE IF EXISTS `certi_tb`;

CREATE TABLE `certi_tb` (
  `certi_id` int(11) DEFAULT NULL,
  `master_id` int(11) DEFAULT NULL,
  `certi_name` varchar(600) DEFAULT NULL,
  `certi_insti` varchar(600) DEFAULT NULL,
  `certi_strdate` varchar(60) DEFAULT NULL,
  `certi_enddate` varchar(150) DEFAULT NULL,
  `certi_description` varchar(1500) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `certi_tb` */

/*Table structure for table `dapartment` */

DROP TABLE IF EXISTS `dapartment`;

CREATE TABLE `dapartment` (
  `dapt_id` int(100) NOT NULL AUTO_INCREMENT,
  `dapt_name` varchar(50) NOT NULL,
  PRIMARY KEY (`dapt_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

/*Data for the table `dapartment` */

insert  into `dapartment`(`dapt_id`,`dapt_name`) values 

(1,'COMPUTER & TECHNOLOGY'),

(2,'FASHION & DESIGN'),

(3,'MEDIA STUDIES'),

(4,'BUSINESS ADMINISTRATION'),

(5,'SOCIAL SCIENCES');

/*Table structure for table `edu_tb` */

DROP TABLE IF EXISTS `edu_tb`;

CREATE TABLE `edu_tb` (
  `edu_id` int(11) DEFAULT NULL,
  `master_id` int(11) DEFAULT NULL,
  `edu_name` varchar(600) DEFAULT NULL,
  `edu_inst` varchar(600) DEFAULT NULL,
  `edu_strdate` varchar(150) DEFAULT NULL,
  `edu_enddate` varchar(150) DEFAULT NULL,
  `edu_description` varchar(1500) DEFAULT NULL,
  `edu_cgpa` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `edu_tb` */

/*Table structure for table `exp_tb` */

DROP TABLE IF EXISTS `exp_tb`;

CREATE TABLE `exp_tb` (
  `exp_id` int(11) NOT NULL AUTO_INCREMENT,
  `master_id` int(11) DEFAULT NULL,
  `exp_designation` varchar(200) DEFAULT NULL,
  `exp_company` varchar(200) DEFAULT NULL,
  `exp_strdate` varchar(50) DEFAULT NULL,
  `exp_enddate` varchar(50) DEFAULT NULL,
  `exp_description` longtext NOT NULL,
  PRIMARY KEY (`exp_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `exp_tb` */

/*Table structure for table `hobby_tb` */

DROP TABLE IF EXISTS `hobby_tb`;

CREATE TABLE `hobby_tb` (
  `hobby_id` int(11) NOT NULL AUTO_INCREMENT,
  `master_id` int(11) DEFAULT NULL,
  `hobby_names` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`hobby_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `hobby_tb` */

/*Table structure for table `lang_tb` */

DROP TABLE IF EXISTS `lang_tb`;

CREATE TABLE `lang_tb` (
  `lang_id` int(11) NOT NULL AUTO_INCREMENT,
  `lang_name` varchar(200) DEFAULT NULL,
  `lang_percentage` varchar(200) DEFAULT NULL,
  `master_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`lang_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `lang_tb` */

/*Table structure for table `level_table` */

DROP TABLE IF EXISTS `level_table`;

CREATE TABLE `level_table` (
  `level_id` int(11) NOT NULL AUTO_INCREMENT,
  `level_name` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`level_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `level_table` */

insert  into `level_table`(`level_id`,`level_name`) values 

(1,'Expert'),

(2,'Entry level'),

(3,'Trainee');

/*Table structure for table `master_tb` */

DROP TABLE IF EXISTS `master_tb`;

CREATE TABLE `master_tb` (
  `master_id` int(11) NOT NULL AUTO_INCREMENT,
  `reg_id` int(11) DEFAULT NULL,
  `title_id` int(11) DEFAULT NULL,
  `ms_image` blob,
  `ms_profile` varchar(500) DEFAULT NULL,
  `ms_fname` varchar(100) DEFAULT NULL,
  `ms_lname` varchar(100) DEFAULT NULL,
  `ms_email` varchar(100) DEFAULT NULL,
  `ms_contact` varchar(100) DEFAULT NULL,
  `ms_address` varchar(200) DEFAULT NULL,
  `ms_gender` varchar(50) DEFAULT NULL,
  `ms_dob` varchar(50) DEFAULT NULL,
  `ms_postal` int(10) NOT NULL,
  `ms_city` varchar(100) NOT NULL,
  `ms_discription` varchar(500) NOT NULL,
  `aprove` int(2) NOT NULL,
  `ms_rollno` int(100) NOT NULL,
  `ms_mname` varchar(100) NOT NULL,
  `ms_social` varchar(100) NOT NULL,
  `ms_expertise` varchar(50) NOT NULL,
  `ms_department` varchar(50) NOT NULL,
  `ms_program` varchar(100) NOT NULL,
  `Specialization` varchar(255) NOT NULL,
  PRIMARY KEY (`master_id`),
  UNIQUE KEY `ms_expertise` (`master_id`),
  UNIQUE KEY `master_id` (`master_id`,`reg_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `master_tb` */

insert  into `master_tb`(`master_id`,`reg_id`,`title_id`,`ms_image`,`ms_profile`,`ms_fname`,`ms_lname`,`ms_email`,`ms_contact`,`ms_address`,`ms_gender`,`ms_dob`,`ms_postal`,`ms_city`,`ms_discription`,`aprove`,`ms_rollno`,`ms_mname`,`ms_social`,`ms_expertise`,`ms_department`,`ms_program`,`Specialization`) values 

(1,2,NULL,'15416855011534676525download.jpg',NULL,'ali','ali','ali@gmail.com','03003232381','Johar Square Apartment, Johar Mor, Karachi, Pakistan','male','2018-11-04',1334,' Karachi','want to get as much as grip on web development filed',0,9,'khan','https://www.facebook.com/Koraisaif','Web Development In Php Based Technology','IU DEPARTMENT','','none');

/*Table structure for table `program` */

DROP TABLE IF EXISTS `program`;

CREATE TABLE `program` (
  `program_id` int(10) NOT NULL AUTO_INCREMENT,
  `program_name` varchar(50) NOT NULL,
  `dept_id` varchar(100) NOT NULL,
  PRIMARY KEY (`program_id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;

/*Data for the table `program` */

insert  into `program`(`program_id`,`program_name`,`dept_id`) values 

(1,'Bachelor of Science in Computer Science BS (CS)','COMPUTER & TECHNOLOGY'),

(2,'Bachelor of Science in Computer Science BS (CS)','COMPUTER & TECHNOLOGY'),

(3,'Bachelor of Science in Computer Science BS (CS)','COMPUTER & TECHNOLOGY'),

(4,'Master of Science in Telecommunication and Network','COMPUTER & TECHNOLOGY'),

(5,'Doctor of Philosophy in Computer Sciences PhD (CS)','COMPUTER & TECHNOLOGY'),

(6,'Doctor of Philosophy in Telecommunication and Netw','COMPUTER & TECHNOLOGY'),

(7,'Bachelor of Fashion Design (BFD)','FASHION & DESIGN'),

(8,'Bachelor of Textile Design (BTD)','FASHION & DESIGN'),

(9,'BMS Advertising','MEDIA STUDIES'),

(10,'BMS Animation','MEDIA STUDIES'),

(11,'Film & TV ','MEDIA STUDIES'),

(12,'Bachelor of Business Administration (BBA)','BUSINESS ADMINISTRATION'),

(13,'Master of Business Administration (MBA)','BUSINESS ADMINISTRATION'),

(14,'Bachelor of Science in Development BS (DS)','SOCIAL SCIENCES'),

(15,'Bachelor of Science in Social Sciences BS (SS)','SOCIAL SCIENCES'),

(16,'Bachelor of Science in International Relations BS ','SOCIAL SCIENCES'),

(17,'Bachelor of Science in Economics','SOCIAL SCIENCES'),

(18,'Master of Science in Development Studies, MSc (DS)','SOCIAL SCIENCES'),

(19,'Master of Science in International Relations, MSc ','SOCIAL SCIENCES'),

(20,'MPhil IDS ','SOCIAL SCIENCES');

/*Table structure for table `ref_tb` */

DROP TABLE IF EXISTS `ref_tb`;

CREATE TABLE `ref_tb` (
  `ref_id` int(11) NOT NULL AUTO_INCREMENT,
  `master_id` int(11) DEFAULT NULL,
  `ref_name` varchar(200) DEFAULT NULL,
  `ref_phone` varchar(200) DEFAULT NULL,
  `ref_email` varchar(200) DEFAULT NULL,
  `ref_desg` varchar(500) DEFAULT NULL,
  `ref_instit` varchar(200) DEFAULT NULL,
  `ref_address` varchar(100) NOT NULL,
  PRIMARY KEY (`ref_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `ref_tb` */

/*Table structure for table `registeration` */

DROP TABLE IF EXISTS `registeration`;

CREATE TABLE `registeration` (
  `reg_id` int(11) NOT NULL AUTO_INCREMENT,
  `role_id` int(11) DEFAULT NULL,
  `reg_username` varchar(200) DEFAULT NULL,
  `reg_contact` varchar(200) DEFAULT NULL,
  `reg_email` varchar(200) DEFAULT NULL,
  `reg_password` varchar(200) DEFAULT NULL,
  `reg_image` blob,
  PRIMARY KEY (`reg_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `registeration` */

insert  into `registeration`(`reg_id`,`role_id`,`reg_username`,`reg_contact`,`reg_email`,`reg_password`,`reg_image`) values 

(1,1,'admin','03003232381','admin@myuni.com','admin123','1541685141153431334502th-egg-person (1).jpg'),

(2,NULL,'ali','03003232381','ali@gmail.com','123','15416853061534402487download.jpg');

/*Table structure for table `role` */

DROP TABLE IF EXISTS `role`;

CREATE TABLE `role` (
  `role_id` int(11) NOT NULL AUTO_INCREMENT,
  `role_name` varchar(200) DEFAULT NULL,
  `role_status` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `role` */

/*Table structure for table `skills_tb` */

DROP TABLE IF EXISTS `skills_tb`;

CREATE TABLE `skills_tb` (
  `skill_id` int(11) NOT NULL AUTO_INCREMENT,
  `master_id` int(11) DEFAULT NULL,
  `skill_name` varchar(200) DEFAULT NULL,
  `skill_percentage` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`skill_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `skills_tb` */

/*Table structure for table `subtitle_table` */

DROP TABLE IF EXISTS `subtitle_table`;

CREATE TABLE `subtitle_table` (
  `subtitle_id` int(11) NOT NULL AUTO_INCREMENT,
  `title_id` int(11) DEFAULT NULL,
  `subtitle_name` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`subtitle_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

/*Data for the table `subtitle_table` */

insert  into `subtitle_table`(`subtitle_id`,`title_id`,`subtitle_name`) values 

(1,1,'PHP'),

(2,1,'Database'),

(3,2,'Software'),

(4,1,'Java'),

(5,2,'SEO Enginner');

/*Table structure for table `title_table` */

DROP TABLE IF EXISTS `title_table`;

CREATE TABLE `title_table` (
  `title_id` int(11) NOT NULL AUTO_INCREMENT,
  `title_name` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`title_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `title_table` */

insert  into `title_table`(`title_id`,`title_name`) values 

(0,'Mid Level'),

(1,'Developer'),

(2,'PHP Expert Developer');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
